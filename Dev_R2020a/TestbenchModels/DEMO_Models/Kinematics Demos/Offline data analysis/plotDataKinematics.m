close all
resultsFolder = 'C:\Experiments\Luis';

trials = dir(fullfile(resultsFolder,'*.mat'));
if size(trials,1) >= 1
    
    trials2cell = struct2cell(trials); %Cell array from Structure
    
    [oldFile, tf] = listdlg('PromptString','Experiment to plot:','ListString',...
        trials2cell(1,:),'ListSize',[300,150], 'SelectionMode','single');
    if tf == 1 % A file has been selected
        fileName = trials2cell{1,oldFile}; % Selected File will be loaded to plot
        
        load([resultsFolder '\' fileName]);
        
        answer = questdlg('Do you want to see the simulation?', ...
            'Simulation', ...
            'Yes','No','No');
        
        switch answer
            case 'No'
                simulation = false;
            case 'Yes'
                simulation = true;
        end
    end
    
end


FileInfo = dir([resultsFolder '\' fileName])

fig = figure;
yTick = [-180, -135, -90, -45, 0, 45, 90, 135, 180];
set(fig,'Position', [50 50 1400 900],'Name', 'Experimental Data');
grid on
hold on

    plotCenter = zeros(size(simoutAnglesCenter.signals.values,3),3);
    plotL1 = zeros(size(simoutAnglesCenter.signals.values,3),3);
    plotL2 = zeros(size(simoutAnglesCenter.signals.values,3),3);
    plotL3 = zeros(size(simoutAnglesCenter.signals.values,3),3);
    plotR1 = zeros(size(simoutAnglesCenter.signals.values,3),3);
    plotR2 = zeros(size(simoutAnglesCenter.signals.values,3),3);
    plotR3 = zeros(size(simoutAnglesCenter.signals.values,3),3);

for i = 1:size(simoutAnglesCenter.signals.values,3)
    plotCenter(i,1:3) = rad2deg(simoutAnglesCenter.signals.values(1,:,i));
    plotL1(i,1:3) = rad2deg(simoutAnglesLeft.signals.values(1,:,i));
    plotL2(i,1:3) = rad2deg(simoutAnglesLeft.signals.values(2,:,i));
    plotL3(i,1:3) = rad2deg(simoutAnglesLeft.signals.values(3,:,i));
    plotR1(i,1:3) = rad2deg(simoutAnglesRight.signals.values(1,:,i));
    plotR2(i,1:3) = rad2deg(simoutAnglesRight.signals.values(2,:,i));
    plotR3(i,1:3) = rad2deg(simoutAnglesRight.signals.values(3,:,i));
end

plotR1(:,1:2) = -plotR1(:,1:2);
plotR2(:,1:2) = -plotR2(:,1:2);
plotR3(:,1:2) = -plotR3(:,1:2);

handle = subplot(3,3,1);
plot1 = plot(simoutAnglesCenter.time,plotCenter);
title('Angles Lower Back')
grid minor
% yticks(yTick)
ylim([-150, 150])
xlabel('time (s)')
ylabel('angle (degrees)')

subplot(3,3,4)
plot2 = plot(simoutAnglesLeft.time, plotL1);
title('Angles Upper Back Left')
grid minor
% yticks(yTick)
ylim([-150, 150])
xlabel('time (s)')
ylabel('angle (degrees)')


handle2 = subplot(3,3,5);
plot3 = plot(simoutAnglesLeft.time, plotL2);
title('Angles Shoulder Left')
grid minor
% yticks(yTick)
ylim([-150, 150])
xlabel('time (s)')
ylabel('angle (degrees)')


handle3 = subplot(3,3,6);
plot4 = plot(simoutAnglesLeft.time, plotL3);
title('Angles Elbow Left')
grid minor
% yticks(yTick)
ylim([-150, 150])
xlabel('time (s)')
ylabel('angle (degrees)')


subplot(3,3,7)
plot5 = plot(simoutAnglesRight.time, plotR1);
title('Angles Upper Back Right')
grid minor
% yticks(yTick)
ylim([-150, 150])
xlabel('time (s)')
ylabel('angle (degrees)')


subplot(3,3,8)
plot6 = plot(simoutAnglesRight.time, plotR2);
title('Angles Shoulder Right')
grid minor
% yticks(yTick)
ylim([-150, 150])
xlabel('time (s)')
ylabel('angle (degrees)')


subplot(3,3,9)
plot7 = plot(simoutAnglesRight.time, plotR3);
title('Angles Elbow Right')
grid minor
% yticks(yTick)
ylim([-150, 150])
xlabel('time (s)')
ylabel('angle (degrees)')


legend1 = sprintf('Rotation around Z-Axis\nArm: Abduction/ Adduction');
legend2 = sprintf('Rotation around Y-Axis\nArm: Pronation/ Supination');
legend3 = sprintf('Rotation around X-Axis\nArm: Flexion/ Extension');

legendFull = legend(plot1,{legend1,legend2,legend3});
% Programatically move the Legend
positionPlot1 = get(handle,'Position');
positionPlot3 = get(handle2,'Position');
positionPlot4 = get(handle3,'Position');

positionLegend = [positionPlot3(1) positionPlot1(2) positionPlot3(3) positionPlot3(4)] ;

newUnits = 'normalized';
set(legendFull,'Position', positionLegend,'Units', newUnits);

positionBox = [positionPlot4(1) positionPlot1(2) positionPlot3(3) positionPlot3(4)] ;
% handle3 = subplot(3,3,3);
% pos = get(handle3,'Position');
str = {['File: ' FileInfo.name],' ', ' ', ['Date: ' FileInfo.date]};
annotation('textbox', positionBox, 'String', str,'FitBoxToText','on')

if simulation
    SensorIDs.time = simoutSensIDs.time;
    SensorIDs.signals.dimensions = simoutSensIDs.signals.dimensions;
    SensorIDs.signals.values = simoutSensIDs.signals.values;
    for i = 1:size(simoutSensIDs.signals.values,3)
        SensorIDs.signals.values(:,:,i) = [11809208, 11809208, 11810499, 11808932, 11809190, 11808948, 11809073];
    end
    sim('KinematicsSim')
    set_param('KinematicsSim', 'SimulationCommand', 'start')
end


