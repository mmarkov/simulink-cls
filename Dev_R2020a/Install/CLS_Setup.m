function CLS_Setup()
warning off backtrace
warning off verbose
fSep = filesep;
%% Change the active to the function dir
mFilePath = mfilename('fullpath');
idx = strfind(mFilePath, fSep);
mFilePath = mFilePath(1:idx(end));
cd (mFilePath);
%% Remove the CLS from the matlab search path
hW = waitbar(0, 'INSTALLING CLS: Removing previos versions ...');
warning('Be sure to remove any previously installed CLS toolboxes!')
CLS_Remove();
waitbar(0.1, hW, 'INSTALLING CLS: Checking requirements ...', 'windowstyle', 'modal');
frames = java.awt.Frame.getFrames();
frames(end).setAlwaysOnTop(1);
%% Find the root level dir
addpath('VersionControl');
addpath('BugFix');
currentDir = mfilename('fullpath');
rootDirIdx = strfind(currentDir,'Install');
rootDir = currentDir(1:rootDirIdx(end)-2);

%% Determine the current win OS/Matlab version and check the compatibility
[osVerN, osVerStr] = getOSver();
[currentSimVerN, currentSimVerStr] = getSimVersion();
if osVerN < 7.0 || ~contains(osVerStr,{'Windows', 'MacOS'})
    error('The operating system needs to be Windows 7 or higher or MacOS. Linux is not supported!');
end
% Windows
if strcmp(osVerStr, 'Windows')
    if osVerN >= 8.0 && currentSimVerN < 8.9
        error('The CLS reuires MATLAB 2017a or higher for this version of Windows!');
    end
end
% MacOS
if strcmp(osVerStr, 'MacOS')
    warning('The MacOS support is experimental. Most of the I/O devices will not work!');
end
% Opengl rendering fix
openglData = opengl('data');
if ~openglData.Software 
    warning('The Simulink 3D Animation might have problems with hardware openGL rendering.');
    prompt = 'Do you want to switch to software rendering instead (do this only if you exp. problems)? y/n [n]: ';
    str = input(prompt,'s');
    if isempty(str)
        str = 'n';
    end
    if strcmp(str,'y')
        opengl software
        opengl('save','software');
    end
end

%% Check for Simulink-specific compatibility issues
switch true
    case currentSimVerN <= 8.6
        error('The minimum required MATLAB version is: R2016a!')
end
%% Determine the presence of compatible MEX compailer and install SLDRT kernel
waitbar(0.2, hW, 'INSTALLING CLS: SLDRT ...');
try
    mex -setup
catch 
    waitbar(0, hW, 'INSTALLING CLS: Failed!');
    pause(2);
    delete(hW);
    disp('The CLS requires compatible C/C++ language compiler to be installed!');
    disp('Please visit the https://de.mathworks.com/support/requirements/supported-compilers.html');
    disp('for a list of compatible compilers (VS Community is recommended choice)');
    error('No compatible C/C++ language compiler found!');
end

% Install SLDRT kernel
try
    sldrtkernel -install
catch ME
    waitbar(0, hW, 'INSTALLING CLS: Failed!');
    pause(2);
    delete(hW);
    rethrow(ME);
end

%% Offer to upgrade if neccessary
testMdl = [rootDir '/TestbenchModels/DEMO_Models/' 'DEMO_1.slx'];
[currentCLSverN, currentCLSverStr] = getSimFileVersion(testMdl);
if currentCLSverN > currentSimVerN
    error([ 'You are trying to install CLS Toolbox compiled with newer Simulink (' ...
        currentCLSverStr ') on a older version of Simulink (' currentSimVerStr ')']);
elseif currentCLSverN < currentSimVerN
    warning(['You are trying to install CLS Toolbox compiled with older Simulink (' ...
        currentCLSverStr ') on a newer version of Simulink (' currentSimVerStr ')']);
    disp('It is strongly recomended that you upgrade!');
    prompt = 'Do you want to upgrade the CLS? y/n [y]: ';
    str = input(prompt,'s');
    if isempty(str)
        str = 'y';
    end
    if strcmp(str,'y')
        try
            convertCLS();
            waitbar(1, hW, 'INSTALLING CLS: Upgrade succesfull!');
            disp('Go to the newly created folder and run');
            disp('"CLS_Setup()"');
            disp('from the command prompt.');
        catch ME
            waitbar(0, hW, 'INSTALLING CLS: Upgrade failed!');
            pause(2);
            delete(hW);
            rethrow(ME);
        end
        pause(2);
        delete(hW);
        return
    end
end
%% Add each of the dirs to the matlab search path
waitbar(0.4, hW, 'INSTALLING CLS: Adding directory trees ...');
currentPath = pathdef();
dirs  = {'Install', 'Components', 'TestbenchModels/Common'};
installPath = [];
for i = 1:length(dirs)
    folderS = genpath([rootDir, '/', dirs{i}]);
    installPath = [installPath folderS];
    path(folderS,currentPath);
    currentPath = path();
end
path(rootDir,currentPath);
currentPath = path();

%% Save the current search path
waitbar(0.5, hW, 'INSTALLING CLS: Saving search path ...');
if savepath
    delete(hW);
    error('You need administrator privilegies in order to perform this operation!');
else
    save([rootDir, '/Install/', 'installPath.mat'], 'installPath')
    waitbar(0.6, hW, 'INSTALLING CLS: Rehashing toolbox cache ...');
    % Repairing the repositories
    fixCorruptedRepository('SystemLib');
    fixCorruptedRepository('SignalProcessingLib');
    waitbar(0.65, hW, 'INSTALLING CLS: Rehashing toolbox cache ...');
    fixCorruptedRepository('ModelConfigurationLib');
    fixCorruptedRepository('MappingLib');
    fixCorruptedRepository('SpatialRotLib');
    waitbar(0.7, hW, 'INSTALLING CLS: Rehashing toolbox cache ...');
    fixCorruptedRepository('InputLib');
    fixCorruptedRepository('FeedbackLib');
    fixCorruptedRepository('ArdLib');
    waitbar(0.75, hW, 'INSTALLING CLS: Rehashing toolbox cache ...');
    fixCorruptedRepository('ExperimentalTaskLib');
    fixCorruptedRepository('ControlLib');
    fixCorruptedRepository('CLS_Toolbox');
    waitbar(0.8, hW, 'INSTALLING CLS: Rehashing toolbox cache ...');
    % Refresh toolbox cache
    rehash toolboxcache
    lb = LibraryBrowser.LibraryBrowser2;
    lb.refresh;
    waitbar(1, hW, 'INSTALLING CLS: Done!');
    disp('The CLS has been successfully installed!');
    disp('Check the Simulink library browser in order to access the toolbox.');
    disp('Toolbox manual (CLS Manual.pdf) can be found in the doc folder.');
    pause(1.5);
    delete(hW);
end
end