function blkStruct = slblocks
  % Specify that the product should appear in the library browser
  % and be cached in its repository
  Browser.Library = 'CLS_Toolbox';
  Browser.Name    = 'CLS Toolbox';
  blkStruct.Browser = Browser;