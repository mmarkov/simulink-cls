function [modelN, modelStr] = getSimFileVersion(modelName)
%getSimFileVersion Summary of this function goes here
%   The function returns the Simulink-Model version

% get model version
 info = Simulink.MDLInfo(modelName); % works only in releases after R2009b
 modelN = str2double(info.SimulinkVersion);
modelStr = info.ReleaseName;

end

