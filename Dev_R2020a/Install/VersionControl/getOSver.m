function [osVerN, osVerStr] = getOSver()
%GETSIMVersion Summary of this function goes here
%   The function returns the current windows version
if ispc
    % get Windows version
    [~, tempVar] = system('ver');
    sIdx = strfind(tempVar ,'Version');
    osVerN = str2double(tempVar(sIdx + length('Version '): sIdx + length('Version XXX')));
    osVerStr = [tempVar(2:sIdx-2) num2str(osVerN)];
elseif ismac
    osVerN = 10;
    osVerStr = 'MacOS';
else
    osVerN = 0;
    osVerStr = 'NOT PC';
end

