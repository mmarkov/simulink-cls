function [simVersionN, simVersionStr] = getSimVersion()
%GETSIMVersion Summary of this function goes here
%   The function returns the current matlab version

% get Simulink version
tempVar = ver('Simulink');
simVersionN = str2double(tempVar.Version);
simVersionStr = tempVar.Release;

simVersionStr = simVersionStr(2:end-1);

end

