function matlabVer = getMatlabVersion()
%GETMATLABVERSION Summary of this function goes here
%   The function returns the current matlab version

% get MATLAB version
tempVar = ver('MATLAB');
matlabVer = str2double(tempVar.Version);


end

