function convOK = convertCLS(varargin)
convOK = 0; % No conversion necessary; convOK = 1 - succesfull conversion; convOK =  -1 - error
warning('off');
f = filesep; 
% Check if the conversion can be performed
[currentSimVerN, currentSimVer] = getSimVersion();
if nargin == 0
    desiredVerN = currentSimVerN; % we try to upgrade to the newest version
    desiredVer = currentSimVer;
else
    desiredVer = varargin{1};
    switch desiredVer
        case 'R2016a'
            desiredVerN = 8.7;
        case 'R2016b'
            desiredVerN = 8.8;
        case 'R2017a'
            desiredVerN = 8.9;
        case 'R2017b'
            desiredVerN = 9.0;
        case 'R2018a'
            desiredVerN = 9.1;
        case 'R2018b'
            desiredVerN = 9.2;
        case 'R2019a'
            desiredVerN = 9.3;
        case 'R2019b'
            desiredVerN = 9.4;
        case 'R2020a'
            desiredVerN = 9.5;
        case 'R2020b'
            desiredVerN = 9.6;
        otherwise
            desiredVerN = 0;
    end
end
if  desiredVerN == 0
    convOK = -1;
    warning('on');
    errpr('Unknown Simulink version: supported Simulink versions are: R2016a to 2020b')
end
if desiredVerN > currentSimVerN
    convOK = -1;
    warning('on');
    error('You cannot upgrade to Simulink version higher than the current one')
end
% Retrieve root dir and check if the conversion is nessecary
currentDir = mfilename('fullpath');
rootDirIdx = strfind(currentDir,'Install');
rootDir = currentDir(1:rootDirIdx(end)-2);

testMdl = [rootDir '/TestbenchModels/DEMO_Models/' 'DEMO_1.slx'];
[currentCLSver, ~] = getSimFileVersion(testMdl);
if desiredVerN == currentCLSver
    convOK = 0;
    warning('on');
    error('The CLS is already compiled in the requested Simulink version')
end

% Create folder structure
targetDir = rootDir;
targetDirIdx = strfind(targetDir,f);
targetDir = [targetDir(1:targetDirIdx(end)) 'Dev_' desiredVer];
if exist(targetDir, 'dir') == 7
    try 
        rmdir( targetDir, 's');
    catch ME
        disp(['Unable to remove folder ' targetDir]);
        disp('Please close Matlab and delete it manually');
        warning('on');
        rethrow(ME)
    end
    rehash();
end
mkdir(targetDir);
% Copy files to new destination
convOK = 1;
try
    pBar = 0;
    hW = waitbar(pBar, 'Recompiling CLS: Copying folder structure', 'windowstyle', 'modal');
    frames = java.awt.Frame.getFrames();
    frames(end).setAlwaysOnTop(1);
    folderList = {'/Components', '/Doc', '/Install', '/TestbenchModels'};
    for folderID = 1 : numel(folderList)
        [status, msg] = copyfile([rootDir folderList{folderID}], [targetDir folderList{folderID}], 'f');
        pBar = pBar + 0.2*1/numel(folderList);
        waitbar(pBar, hW, 'Recompiling CLS: Copying folder structure');
        if status == 0
            warning('on');
            error(msg);
        end
    end
    % Remove old files
    SlXfileListOld = subdir([targetDir, '/*.slx']);
    for fileID = 1 : numel(SlXfileListOld)
        pBar = pBar + 0.1*1/numel(SlXfileListOld);
        waitbar(pBar, hW, 'Recompiling CLS: Removing old files');
        if ~SlXfileListOld(fileID).isdir
            delete(SlXfileListOld(fileID).name);
        end
    end
    % Convert the files
    SlXfileListNew = subdir([rootDir, '/*.slx']);
    for fileID = 1 : numel(SlXfileListNew)
        pBar = pBar + 0.7*1/numel(SlXfileListOld);
        if ~SlXfileListNew(fileID).isdir
            load_system(SlXfileListNew(fileID).name);
            model_Handle = bdroot;
            isModel = strcmp(get_param(model_Handle, 'LibraryType'), 'None') && ...
                strcmp(get_param(model_Handle, 'Type'), 'block_diagram');
            if isModel
                eval(['detachhdlcconfig ' model_Handle]);
                eval(['attachhdlcconfig ' model_Handle]);
            end
            if desiredVerN == currentSimVerN % upgrade
                save_system(bdroot,SlXfileListOld(fileID).name); % upgrade
                waitbar(pBar, hW, 'Recompiling CLS: Upgrading models');
            else
                Simulink.exportToVersion(model_Handle,SlXfileListOld(fileID).name,desiredVer);
                waitbar(pBar, hW, 'Recompiling CLS: Downgrading models');
            end
            close_system(model_Handle, 0)
        end
    end
catch ME
    close_system(model_Handle, 0)
    convOK = -1;
    warning('on');
    rethrow(ME)
end
close (hW);
delete (hW);
warning('on');
end
