function fixCorruptedRepository(slxFile)
%% Fix corrupted repository
load_system(slxFile);
set_param(slxFile,'Lock','off');
set_param(slxFile,'EnableLBRepository','on');
save_system(slxFile);
end