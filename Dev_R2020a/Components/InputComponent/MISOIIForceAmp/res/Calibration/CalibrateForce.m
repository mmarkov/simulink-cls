function varargout = CalibrateForce(varargin)
% CALIBRATEFORCE MATLAB code for CalibrateForce.fig
%      CALIBRATEFORCE, by itself, creates a new CALIBRATEFORCE or raises the existing
%      singleton*.
%
%      H = CALIBRATEFORCE returns the handle to a new CALIBRATEFORCE or the handle to
%      the existing singleton*.
%
%      CALIBRATEFORCE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CALIBRATEFORCE.M with the given input arguments.
%
%      CALIBRATEFORCE('Property','Value',...) creates a new CALIBRATEFORCE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CalibrateForce_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CalibrateForce_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CalibrateForce

% Last Modified by GUIDE v2.5 04-Feb-2014 15:12:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CalibrateForce_OpeningFcn, ...
                   'gui_OutputFcn',  @CalibrateForce_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CalibrateForce is made visible.
function CalibrateForce_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CalibrateForce (see VARARGIN)
global  YLIM

% Choose default command line output for CalibrateForce
handles.output = hObject;
handles.devInfo = InitDAQInfo( handles );
YLIM = [ 0 10 ];
set( handles.txtCurrForce,  'String', 'X' );
if ~isempty( varargin ), 
    handles.gcb = varargin{ 1 }{ 1 }; 
    set( handles.chSelection,'Enable', 'off' );
    SimBlock2GUI( handles );
else
    handles.gcb = [];
    calibP.maxForce = 5;
    calibP.cboChannels = 1;
    set(handles.chSelection,'Enable', 'on');
    set(handles.fig, 'UserData', calibP);
end
handles.acqTimer = [];
handles.daqSession = [];

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CalibrateForce wait for user response (see UIRESUME)
% uiwait(handles.fig);

function devInfo = InitDAQInfo( handles )

devInfo = daq.getDevices; 
devNames = cell( 1, length( devInfo ) );
for i = 1:length( devInfo ), devNames{ i } = devInfo( i ).Model; end
set( handles.cboDevices, 'String', devNames );

function SimBlock2GUI( handles )
calibP = get(handles.fig,'UserData');
calibP.maxForce = str2double( get_param( handles.gcb, 'maxF' ) );
calibP.cboChannels =  str2double( get_param( handles.gcb, 'inCh' ) );
set( handles.chSelection,'Value', calibP.cboChannels );
set( handles.txtMaxForce, 'String', calibP.maxForce );
set( handles.fig,'UserData', calibP );

% --- Outputs from this function are returned to the command line.
function varargout = CalibrateForce_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btnOK.
function btnOK_Callback(hObject, eventdata, handles)
% hObject    handle to btnOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close( handles.fig );


function txtCurrForce_Callback(hObject, eventdata, handles)
% hObject    handle to txtCurrForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtCurrForce as text
%        str2double(get(hObject,'String')) returns contents of txtCurrForce as a double


% --- Executes during object creation, after setting all properties.
function txtCurrForce_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtCurrForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close fig.
function fig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to fig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    if ~isempty( handles.acqTimer )
        stop( handles.acqTimer );
        delete( handles.acqTimer );
    end
    if ~isempty( handles.daqSession ), delete( handles.daqSession ); end
    if ~isempty( handles.gcb )
        GUI2SimBlock( handles );
    end
catch ME
    disp( ME.message )
end
% Hint: delete(hObject) closes the figure
delete(hObject);

function GUI2SimBlock( handles )
set_param( gcb, 'maxF', get( handles.txtMaxForce, 'String' ) );

% --- Executes on button press in btnConnect.
function btnConnect_Callback(hObject, eventdata, handles)
% hObject    handle to btnConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
calibP = get(handles.fig,'UserData');
channelNames = {'ai0', 'ai1', 'ai2', 'ai3', 'ai4' , 'ai5', 'ai6', 'ai7', 'ai8'};
channelIndex = calibP.cboChannels;
set(hObject, 'Enable', 'off');
try
    devIndex = get( handles.cboDevices, 'Value' );
    devID = handles.devInfo( devIndex ).ID;
    handles.daqSession = InitDAQ( devID, channelNames{ channelIndex } );
%     addlistener( handles.daqSession, 'DataAvailable', @( source, event ) ForceDataAvailable( source, event, handles ) );
%     handles.daqSession.startBackground;
    handles.acqTimer = timer( 'ExecutionMode', 'fixedRate', 'TimerFcn', @( source, event ) ForceDataAvailable( source, event, handles ), 'Period', 0.1 );
    guidata( handles.fig, handles);
    start( handles.acqTimer );
catch ME
    disp( ME.message );
    set(hObject, 'Enable', 'on');
end
drawnow

function daqSession = InitDAQ( dev, channel )
daqSession = daq.createSession( 'ni' );
daqSession.addAnalogInputChannel( dev, channel, 'Voltage' );
daqSession.Rate = 10;
daqSession.NotifyWhenDataAvailableExceeds = 1; 
daqSession.IsContinuous = true;
daqSession.Channels.TerminalConfig = 'SingleEnded';

function ForceDataAvailable( ~, ~, handles )
global YLIM
% currForce = abs( event.Data );
calibP = get(handles.fig,'UserData');
maxForce = calibP.maxForce;
currForce = abs( handles.daqSession.inputSingleScan );
if isempty( maxForce ), maxForce = currForce; end
if maxForce < currForce, maxForce = currForce; end
cla( handles.axForce );
line( [ 0 2 ], [ maxForce maxForce ], 'LineStyle', '--', 'Color', 'r', 'LineWidth', 2, 'Parent', handles.axForce );
hold( handles.axForce, 'on' );
bar( 1, currForce, 0.5, 'FaceColor', 'b', 'Parent', handles.axForce );
hold( handles.axForce, 'off' );
ylim( handles.axForce, YLIM );
xlim( handles.axForce, [ 0 2 ] );
set( handles.txtCurrForce, 'String', num2str( currForce, '%.2f' ) );
calibP.maxForce = maxForce;
set( handles.fig, 'UserData', calibP );
set( handles.txtMaxForce, 'String', num2str( maxForce, '%.2f' ) );
drawnow

% --- Executes on selection change in cboDevices.
function cboDevices_Callback(hObject, eventdata, handles)
% hObject    handle to cboDevices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns cboDevices contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cboDevices


% --- Executes during object creation, after setting all properties.
function cboDevices_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cboDevices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ppmScale.
function ppmScale_Callback(hObject, eventdata, handles)
% hObject    handle to ppmScale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ppmScale contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ppmScale
global YLIM 

switch get(hObject,'Value')
    case 1
        YLIM = [ 0 10 ];
    case 2
        YLIM = [ 0 5 ];
    case 3
        YLIM = [ 0 1 ];
    otherwise
        msgbox( 'Unknown scale option!', 'ERROR1', 'error' );
end
set( handles.axForce, 'YLim', YLIM );

% --- Executes during object creation, after setting all properties.
function ppmScale_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ppmScale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in chSelection.
function chSelection_Callback(hObject, eventdata, handles)
% hObject    handle to chSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns chSelection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from chSelection
calibP = get(handles.fig, 'UserData');
calibP.cboChannels = get(hObject,'Value');
set(handles.fig, 'UserData', calibP);

% --- Executes during object creation, after setting all properties.
function chSelection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnResetMaxForce.
function btnResetMaxForce_Callback(hObject, eventdata, handles)
% hObject    handle to btnResetMaxForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
calibP = get( handles.fig, 'UserData' );
calibP.maxForce = str2double( get( handles.txtCurrForce, 'String' ) );
set( handles.txtMaxForce, 'String', num2str( calibP.maxForce, '%.2f' ) );
set( handles.fig, 'UserData', calibP );


function txtMaxForce_Callback(hObject, eventdata, handles)
% hObject    handle to txtMaxForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtMaxForce as text
%        str2double(get(hObject,'String')) returns contents of txtMaxForce as a double


% --- Executes during object creation, after setting all properties.
function txtMaxForce_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtMaxForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
