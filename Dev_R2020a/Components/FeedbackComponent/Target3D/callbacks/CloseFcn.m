autoopen = get_param(gcb,'autoopen');                     
if autoopen                                                  
 sysPath = [gcb '/VR Sink'];                 
 close_system(sysPath);                                                                              
end
clear sysPath autoopen;     