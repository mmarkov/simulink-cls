fullscreen = get_param(gcb, 'fullscreen');                                                                                 
autoopen = get_param(gcb, 'autoopen');     
target3D_H = get_param(gcb, 'UserData');
if strcmp(autoopen,'on')                                                                                                     
    sysPath = [gcb '/VR Sink'];                                                                                              
    if  isempty(target3D_H) || ~isvalid(target3D_H)                                                                          
        open_system(sysPath,'OpenFcn');                                                                                      
        target3D_H = vrgcf;                                                                                                  
    end                                                                                                                      
    set(target3D_H, 'Fullscreen', fullscreen);                                                                             
    set(target3D_H, 'NavPanel', 'minimized', 'Tooltips', 'off', 'ToolBar', 'off', 'StatusBar', 'off', 'Antialiasing', 'off');
end                                            
set_param(gcb, 'UserData', target3D_H);
warning('off', 'Simulink:Engine:BlockOutputInfNanDetectedError');
clear sysPath  fullscreen  autoopen  target3D_H;