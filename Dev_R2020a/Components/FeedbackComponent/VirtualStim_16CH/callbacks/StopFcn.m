virtStim_H = get_param(gcb, 'UserData');
if ~isempty(virtStim_H) && isvalid(virtStim_H)
	set(virtStim_H, 'Fullscreen', 'off');       
end     
warning('on', 'Simulink:Engine:BlockOutputInfNanDetectedError');
clear virtStim_H