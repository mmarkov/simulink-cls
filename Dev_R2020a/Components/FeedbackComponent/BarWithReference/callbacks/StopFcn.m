monoBar_H = get_param(gcb, 'UserData');
if ~isempty(monoBar_H) && isvalid(monoBar_H)
	set(monoBar_H, 'Fullscreen', 'off');      
end
warning('on', 'Simulink:Engine:BlockOutputInfNanDetectedError');
clear monoBar_H