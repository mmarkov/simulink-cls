virtualHand_H = get_param(gcb, 'UserData');
if ~isempty(virtualHand_H) && isvalid(virtualHand_H)
	set( virtualHand_H, 'Fullscreen', 'off' );
end
warning('on', 'Simulink:Engine:BlockOutputInfNanDetectedError');
clear virtualHand_H