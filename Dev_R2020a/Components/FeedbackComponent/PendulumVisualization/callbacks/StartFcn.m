fullscreen = get_param(gcb, 'fullscreen');                                                                                 
autoopen = get_param(gcb, 'autoopen');     
pendulum3D_H = get_param(gcb, 'UserData');
if strcmp(autoopen,'on')                                                                                                     
    sysPath = [gcb '/VR Sink'];                                                                                              
    if  isempty(pendulum3D_H) || ~isvalid(pendulum3D_H)                                                                          
        open_system(sysPath,'OpenFcn');                                                                                      
        pendulum3D_H = vrgcf;                                                                                                  
    end                                                                                                                      
    set(pendulum3D_H, 'Fullscreen', fullscreen);                                                                             
    set(pendulum3D_H, 'NavPanel', 'minimized', 'Tooltips', 'off', 'ToolBar', 'off', 'StatusBar', 'off', 'Antialiasing', 'off');
end                                            
set_param(gcb, 'UserData', pendulum3D_H);
clear sysPath  fullscreen  autoopen  pendulum3D_H;