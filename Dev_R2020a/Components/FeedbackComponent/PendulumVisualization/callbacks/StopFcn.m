pendulum3D_H = get_param(gcb, 'UserData');
if ~isempty(pendulum3D_H) && isvalid(pendulum3D_H)
	set(pendulum3D_H, 'Fullscreen', 'off');       
end     
warning('on', 'Simulink:Engine:BlockOutputInfNanDetectedError');
clear pendulum3D_H