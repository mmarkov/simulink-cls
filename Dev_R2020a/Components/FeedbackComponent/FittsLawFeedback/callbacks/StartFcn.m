fullscreen = get_param(gcb, 'fullscreen');
autoopen = get_param(gcb, 'autoopen');     
fittsLaw_H = get_param(gcb, 'UserData');
if strcmp(autoopen,'on')                                                                                                     
    sysPath = [gcb '/VR Sink'];                                                                                              
    if  isempty(fittsLaw_H) || ~isvalid(fittsLaw_H)                                                                          
        open_system(sysPath,'OpenFcn');                                                                                      
        fittsLaw_H = vrgcf;                                                                                                  
    end
    set(fittsLaw_H, 'Fullscreen', fullscreen);
    set(fittsLaw_H, 'NavPanel', 'minimized', 'Tooltips', 'off', 'ToolBar', 'off', 'StatusBar', 'off', 'Antialiasing', 'off');
end                                            
set_param(gcb, 'UserData', fittsLaw_H);
clear sysPath  autoopen  target3D_H  fullscreen;