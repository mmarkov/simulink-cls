function checksum = x_or( packet )
% returns the bitwise x-or connection of all bytes and 234. As a requirement 
% by EAI, the checksum has to be included as the last element of the packet
packet = uint8( packet );
checksum = packet( 1 );
for i = 1:length( packet )-1
    checksum_tmp = bitxor( checksum, packet( i+1 ) );
    checksum = checksum_tmp;
end
checksum = bitxor( checksum, 234 );



