blockH = gcb;
FigHandle=get_param(blockH,'UserData');
if isempty(FigHandle),
    FigHandle=-1;
end

if ishandle(FigHandle),
    delete(FigHandle);
end

clear  FigHandle blockH