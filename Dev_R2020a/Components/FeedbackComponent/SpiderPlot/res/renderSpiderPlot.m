function renderSpiderPlot(block)
%MSFUNTMPL_BASIC A Template for a Level-2 MATLAB S-Function
%   The MATLAB S-function is written as a MATLAB function with the
%   same name as the S-function. Replace 'msfuntmpl_basic' with the
%   name of your S-function.
%
%   It should be noted that the MATLAB S-function is very similar
%   to Level-2 C-Mex S-functions. You should be able to get more
%   information for each of the block methods by referring to the
%   documentation for C-Mex S-functions.
%
%   Copyright 2003-2010 The MathWorks, Inc.

%%
%% The setup method is used to set up the basic attributes of the
%% S-function such as ports, parameters, etc. Do not add any other
%% calls to the main body of the function.
%%
setup(block);

%endfunction

%% Function: setup ===================================================
%% Abstract:
%%   Set up the basic characteristics of the S-function block such as:
%%   - Input ports
%%   - Output ports
%%   - Dialog parameters
%%   - Options
%%
%%   Required         : Yes
%%   C-Mex counterpart: mdlInitializeSizes
%%
function setup(block)

% Register number of ports
block.NumInputPorts  = 1;
block.NumOutputPorts = 0;

% Setup port properties to be inherited or dynamic
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

% Override input port properties
% block.InputPort(1).Dimensions        = 1;
% block.InputPort(1).DatatypeID  = 8;  % double
% block.InputPort(1).Complexity  = 'Real';
% block.InputPort(1).DirectFeedthrough = true;

% % Override output port properties
% block.OutputPort(1).Dimensions       = 0;
% block.OutputPort(1).DatatypeID  = 0; % double
% block.OutputPort(1).Complexity  = 'Real';

% Register parameters
block.NumDialogPrms     = 0;

% Register sample times
%  [0 offset]            : Continuous sample time
%  [positive_num offset] : Discrete sample time
%
%  [-1, 0]               : Inherited sample time
%  [-2, 0]               : Variable sample time
block.SampleTimes = [-1 0];

% Specify the block simStateCompliance. The allowed values are:
%    'UnknownSimState', < The default setting; warn and assume DefaultSimState
%    'DefaultSimState', < Same sim state as a built-in block
%    'HasNoSimState',   < No sim state
%    'CustomSimState',  < Has GetSimState and SetSimState methods
%    'DisallowSimState' < Error out when saving or restoring the model sim state
block.SimStateCompliance = 'DefaultSimState';

%% -----------------------------------------------------------------
%% The MATLAB S-function uses an internal registry for all
%% block methods. You should register all relevant methods
%% (optional and required) as illustrated below. You may choose
%% any suitable name for the methods and implement these methods
%% as local functions within the same file. See comments
%% provided for each function for more information.
%% -----------------------------------------------------------------

block.RegBlockMethod('PostPropagationSetup',    @DoPostPropSetup);
block.RegBlockMethod('InitializeConditions', @InitializeConditions);
block.RegBlockMethod('Start', @Start);
block.RegBlockMethod('Outputs', @Outputs);     % Required
block.RegBlockMethod('Update', @Update);
block.RegBlockMethod('Derivatives', @Derivatives);
block.RegBlockMethod('Terminate', @Terminate); % Required
block.SetSimViewingDevice(true);

%end setup

%%
%% PostPropagationSetup:
%%   Functionality    : Setup work areas and state variables. Can
%%                      also register run-time methods here
%%   Required         : No
%%   C-Mex counterpart: mdlSetWorkWidths
%%
function DoPostPropSetup(block)
block.NumDworks = 1;

block.Dwork(1).Name            = 'x1';
block.Dwork(1).Dimensions      = 1;
block.Dwork(1).DatatypeID      = 0;      % double
block.Dwork(1).Complexity      = 'Real'; % real
block.Dwork(1).UsedAsDiscState = true;


%%
%% InitializeConditions:
%%   Functionality    : Called at the start of simulation and if it is
%%                      present in an enabled subsystem configured to reset
%%                      states, it will be called when the enabled subsystem
%%                      restarts execution to reset the states.
%%   Required         : No
%%   C-MEX counterpart: mdlInitializeConditions
%%
function InitializeConditions(block)
%

%end InitializeConditions


%%
%% Start:
%%   Functionality    : Called once at start of model execution. If you
%%                      have states that should be initialized once, this
%%                      is the place to do it.
%%   Required         : No
%%   C-MEX counterpart: mdlStart
%%
function Start(block)

%
block.Dwork(1).Data = 0;
set(block.BlockHandle, 'UserData', block.InputPort(1).Dimensions);
% get the figure associated with this block, create a figure if it doesn't
% exist
%
FigHandle = CreateSfunXYFigure(block.BlockHandle);
%end Start

%%
%% Outputs:
%%   Functionality    : Called to generate block outputs in
%%                      simulation step
%%   Required         : Yes
%%   C-MEX counterpart: mdlOutputs
%%
function Outputs(block)

FigHandle=GetSfunXYFigure(block.BlockHandle);
if ~ishandle(FigHandle)
    return
end

%
% Get UserData of the figure.
%
ud = FigHandle.UserData;
inSize = get(block.BlockHandle,'UserData');
if inSize(1) == 1 || inSize(2) == 1
    inSigDim = max(inSize);
    nomOfIn = 1;
else
    inSigDim = inSize(2);
    nomOfIn = inSize(1);
end
% Plot
thetaA = linspace(0, 2*pi - ((2*pi)/inSigDim), inSigDim);
for sigID = 1: nomOfIn
    xx = block.InputPort(1).Data(sigID,:).*cos(thetaA);
    yy = block.InputPort(1).Data(sigID,:).*sin(thetaA);
    set(ud.PlotH(sigID),'XData', [xx xx(1)]);
    set(ud.PlotH(sigID), 'YData', [yy yy(1)]);
end
%end Outputs

%%
%% Update:
%%   Functionality    : Called to update discrete states
%%                      during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlUpdate
%%
function Update(block)

% block.Dwork(1).Data = block.InputPort(1).Data;

%end Update

%%
%% Derivatives:
%%   Functionality    : Called to update derivatives of
%%                      continuous states during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlDerivatives
%%
function Derivatives(block)

%end Derivatives

%%
%% Terminate:
%%   Functionality    : Called at the end of simulation for cleanup
%%   Required         : Yes
%%   C-MEX counterpart: mdlTerminate
%%
function Terminate(block)


%end Terminate

%
%=============================================================================
% GetSfunXYFigure
% Retrieves the figure window associated with this S-function XY Graph block
% from the block's parent subsystem's UserData.
%=============================================================================
%
function FigHandle = GetSfunXYFigure(block)
if strcmp(get_param(block,'BlockType'),'M-S-Function')
    block=get_param(block,'Parent');
end

FigHandle=get_param(block,'UserData');
if isempty(FigHandle)
    FigHandle=-1;
end

% end GetSfunXYFigure


%=============================================================================
% SetSfunXYFigure
% Stores the figure window associated with this S-function XY Graph block
% in the block's parent subsystem's UserData.
%=============================================================================
%
function SetSfunXYFigure(block,FigHandle)

if strcmp(get_param(bdroot,'BlockDiagramType'),'model')
    if strcmp(get_param(block,'BlockType'),'M-S-Function')
        block=get_param(block,'Parent');
    end
    
    set_param(block,'UserData',FigHandle);
end

% end SetSfunXYFigure



%=============================================================================
% CreateSfunXYFigure
% Creates the figure window associated with this S-function XY Graph block.
%=============================================================================
%
function FigHandle = CreateSfunXYFigure(block)

inSize = get(block,'UserData');
if inSize(1) == 1 || inSize(2) == 1
    inSigDim = max(inSize);
    nomOfIn = 1;
else
    inSigDim = inSize(2);
    nomOfIn = inSize(1);
end

rho = 1;

FigHandle = GetSfunXYFigure(block);

if ishandle(FigHandle)
    ud = FigHandle.UserData;
    if ~isempty(ud) 
        cla(ud.XYAxes,'reset');
        cax = newplot(ud.XYAxes);
    end
else
    FigHandle = figure('Color', [1 1 1], 'Units', 'normalized', 'Position', ...
        [0.775 0.55 0.2 0.35], 'Toolbar', 'none', 'MenuBar', 'none', ...
        'Name', 'SpiderPlot', 'NumberTitle', 'off');
    cax = newplot();
end

next = lower(get(cax, 'NextPlot'));


% get x-axis text color so grid is in same color
% get the axis gridColor
axColor = get(cax, 'Color');
gridAlpha = get(cax, 'GridAlpha');
axGridColor = get(cax,'GridColor').*gridAlpha + axColor.*(1-gridAlpha);
tc = axGridColor;
ls = get(cax, 'GridLineStyle');

% Hold on to current Text defaults, reset them to the
% Axes' font attributes so tick marks use them.
fAngle = get(cax, 'DefaultTextFontAngle');
fName = get(cax, 'DefaultTextFontName');
fSize = get(cax, 'DefaultTextFontSize');
fWeight = get(cax, 'DefaultTextFontWeight');
fUnits = get(cax, 'DefaultTextUnits');
set(cax, ...
    'DefaultTextFontAngle', get(cax, 'FontAngle'), ...
    'DefaultTextFontName', get(cax, 'FontName'), ...
    'DefaultTextFontSize', get(cax, 'FontSize'), ...
    'DefaultTextFontWeight', get(cax, 'FontWeight'), ...
    'DefaultTextUnits', 'data');

% only do grids if hold is off
hold(cax, 'off');
hold_state = ishold(cax);

if ~hold_state
    
    % make a radial grid
    hold(cax, 'on');
    % ensure that Inf values don't enter into the limit calculation.
    arho = abs(rho(:));
    maxrho = max(arho(arho ~= Inf));
    hhh = line([-maxrho, -maxrho, maxrho, maxrho], [-maxrho, maxrho, maxrho, -maxrho], 'Parent', cax);
    set(cax, 'DataAspectRatio', [1, 1, 1], 'PlotBoxAspectRatioMode', 'auto');
    v = [get(cax, 'XLim') get(cax, 'YLim')];
    ticks = sum(get(cax, 'YTick') >= 0);
    delete(hhh);
    % check radial limits and ticks
    rmin = 0;
    rmax = v(4);
    rticks = 4;
    
    % define a circle
    th = 0 : pi / 50 : 2 * pi;
    xunit = cos(th);
    yunit = sin(th);
    % now really force points on x/y axes to lie on them exactly
    inds = 1 : (length(th) - 1) / 4 : length(th);
    xunit(inds(2 : 2 : 4)) = zeros(2, 1);
    yunit(inds(1 : 2 : 5)) = zeros(3, 1);
    % plot background if necessary
    if ~ischar(get(cax, 'Color'))
        patch('XData', xunit * rmax, 'YData', yunit * rmax, ...
            'EdgeColor', tc, 'FaceColor', get(cax, 'Color'), ...
            'HandleVisibility', 'off', 'Parent', cax);
    end
    
    % draw radial circles
    c82 = cos(82 * pi / 180);
    s82 = sin(82 * pi / 180);
    rinc = (rmax - rmin) / rticks;
    for sigID = (rmin + rinc) : rinc : rmax
        hhh = line(xunit * sigID, yunit * sigID, 'LineStyle', ls, 'Color', tc, 'LineWidth', 1, ...
            'HandleVisibility', 'off', 'Parent', cax);
    end
    set(hhh, 'LineStyle', '-'); % Make outer circle solid
    
    % plot spokes
    th = (1 : inSigDim/2) * 2 * pi / (inSigDim);
    cst = cos(th);
    snt = sin(th);
    cs = [-cst; cst];
    sn = [-snt; snt];
    line(rmax * cs, rmax * sn, 'LineStyle', ls, 'Color', tc, 'LineWidth', 1, ...
        'HandleVisibility', 'off', 'Parent', cax);
    
    % annotate spokes in degrees
    rt = 1.1 * rmax;
    for sigID = 1 : length(th)
        
        loc = int2str(sigID + 1);
        
        text(rt * cst(sigID), rt * snt(sigID), loc,...
            'HorizontalAlignment', 'center', ...
            'HandleVisibility', 'off', 'Parent', cax);
        if sigID == length(th)
            loc  = '1';
        else
            loc = int2str(inSigDim/2  + sigID + 1);
        end
        text(-rt * cst(sigID), -rt * snt(sigID), loc, ...
            'HorizontalAlignment', 'center', ...
            'HandleVisibility', 'off', 'Parent', cax);
    end
    
    % set view to 2-D
    view(cax, 2);
    % set axis limits
    axis(cax, rmax * [-1, 1, -1.15, 1.15]);
    % Turn off axes
    set(cax, 'DataAspectRatio', [1, 1, 1]), axis(cax, 'off');
    % Reset defaults.
    set(cax, ...
        'DefaultTextFontAngle', fAngle , ...
        'DefaultTextFontName', fName , ...
        'DefaultTextFontSize', fSize, ...
        'DefaultTextFontWeight', fWeight, ...
        'DefaultTextUnits', fUnits );
end

pH = zeros(1,nomOfIn);
pColors = lines(nomOfIn)*0.75;
legendNames = cell(1,nomOfIn);
for sigID = 1: nomOfIn
    r = 0;
    % plot data on top of grid
    thetaA = linspace(0, 2*pi - ((2*pi)/inSigDim), inSigDim);
    xx = r.*cos(thetaA);
    yy = r.*sin(thetaA);
    pH(sigID) = plot([xx xx(1)], [yy yy(1)], 'LineWidth', 1.5, 'Parent', cax, 'color', pColors(sigID,:));
    legendNames{sigID} = ['Ch' num2str(sigID)];
end
legend(cax, legendNames);
%
% Associate the figure with the block, and set the figure's UserData.
%
ud.XYAxes   = cax;
ud.PlotH = pH;
ud.Block = block;
SetSfunXYFigure(block,FigHandle);
set(FigHandle,'HandleVisibility','callback','UserData',ud);

% end CreateSfunXYFigure


%
%=============================================================================
% LocalFigureDeleteFcn
% This is the XY Graph figure window's DeleteFcn.  The figure window is
% being deleted, update the XY Graph block's UserData to reflect the change.
%=============================================================================
%
function LocalFigureDeleteFcn(src, ~)

%
% Get the block associated with this figure and set it's figure to -1
%
ud=get(src,'UserData');
SetSfunXYFigure(ud.Block, -1)

% end LocalFigureDeleteFcn

