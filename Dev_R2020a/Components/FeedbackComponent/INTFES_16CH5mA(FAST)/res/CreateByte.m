function byte = CreateByte( wH, wL )
byte = bitor( bitshift( uint8(wH) , 4 ), uint8( wL ) );
end