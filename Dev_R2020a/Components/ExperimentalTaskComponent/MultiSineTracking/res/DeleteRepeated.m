function y = DeleteRepeated( x )
%#codegen

idx = nan( size( x ) );
k = 1;
for i = 1:length( x )
    if i > 1 && ( x( i ) == x( i - 1 ) )
        idx( k ) = i; 
        k = k + 1;
    end
end
idx( isnan( idx ) ) = [];
x( idx ) = [];
y = x;

end

