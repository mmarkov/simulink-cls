function GenerateReferenceSignal(currentBlock, refSampleTime, refDuration, refType, amp, freq, phase, offset, M, band, numSines, numTrials, gridSkip, fmax, levels, L)

referenceTime = 0:refSampleTime:refDuration;
zeroData = zeros( size( referenceTime ) );

if refType == 1
    % amp, freq, phase, offset
    referenceValues = amp * sin( 2 * pi * referenceTime * freq + phase ) + offset;
elseif refType == 2
        signalTs = refSampleTime; 
        Fs = 1 / signalTs; 
        Fn = Fs / 2;
        Tp = refDuration / M; 
        %Td = Tp * M; 
        P = Tp / signalTs;
        signalBand = band / Fn;
        sinedata = [numSines, numTrials, gridSkip];
        wmax = 2 * pi * fmax;
        [ data, ~ ] = GenMultiSine( [ P M ], signalBand, levels, sinedata, Fs, wmax, L );
        referenceValues = data';
        referenceTime = ( 0:( length( data ) - 1 ) ) * signalTs;
else
    referenceValues = zeroData;
end

set_param([currentBlock '/inTime'], 'Value', ['[' num2str(referenceTime) ']']);
set_param([currentBlock '/inSignal'], 'Value', ['[' num2str(referenceValues) ']']);