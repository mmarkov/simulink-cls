function [u,freqs] = GenMultiSine(N, band, levels, nosine, fs, wmax, L )
    %#codegen

    P = round(N(1)); 
    M = N(2);
    gridSkip = nosine(3);
    possfreq = 2 * pi * (1:gridSkip:fix(P/2)) / P;
    possfreq = possfreq(possfreq > band(1) * pi);
    possfreq = possfreq(possfreq < band(2) * pi);
    numtrial = nosine(2);
    fn = fs / 2;
    n = logspace(log10(1), log10(length(possfreq)), nosine(1));
    n = DeleteRepeated(round(n));
    freqs = possfreq(n);
    w = 2 * pi * (freqs * fn / pi); % sdosen
    u1 = zeros(P, 1);
    bestamp = 0;

    for kn = 1:numtrial
        ut = zeros(P,1);
        for ks = 1:length(freqs);
            if w(ks) <= wmax % sdosen
                ut = ut + cos((0:P-1)' * freqs(ks) + rand(1,1) * 2 * pi);
            else
                ut = ut + L * cos((0:P-1)' * freqs(ks) + rand(1,1) * 2 * pi);
            end
        end
        mm = max(ut);
        mn = min(ut);
        if kn == 1
            u1 = ut; 
            bestamp = mm - mn; 
        end
        if mm - mn < bestamp
           u1 = ut;
           bestamp = mm - mn; 
        end
    end

    %u1 = u1 - ( sum(u1(u1>0)) - abs(sum(u1(u1<0))) )*0.5; % Center around zero (marko).

    mn = min(u1);
    mm = max(u1);
    mm = max(mm, -mn);
    mn = -mm;
    u = (levels(2) - levels(1)) * (u1-mn) / (mm-mn) + levels(1);
    if M > 1
        uu = u;
        for km = 2:M
            u = [uu; u]; %#ok<*AGROW>
        end
    end
    
end 