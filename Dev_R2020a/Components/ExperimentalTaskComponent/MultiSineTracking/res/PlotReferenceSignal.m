function PlotReferenceSignal(refSampleTime, referenceTime, referenceValues, band)

referenceTime = str2num(referenceTime);
referenceValues = str2num(referenceValues);
band = str2num(band);

Fs = 1/refSampleTime;

FR = fft( referenceValues );
df = Fs / length( referenceValues ); 
freq = 0:df:( Fs - df );
subplot( 211 ), plot( referenceTime, referenceValues); title( 'REFERENCE WAVEFORM' ); xlabel( 'time (s)' );
subplot( 212 ), stem( freq, abs( FR ) ); xlim( band ); title ( 'REFERNCE WAVEFORM SPECTRA' ); xlabel( 'Frequency (Hz)' );