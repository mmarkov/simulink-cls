fullscreen = get_param(gcb, 'fullscreen');
autoopen = get_param(gcb, 'autoopen');     
UpperBodyKin_H = get_param(gcb, 'UserData');
if strcmp(autoopen,'on')                                                                                                     
    sysPath = [gcb '/VR Sink'];                                                                                              
    if  isempty(UpperBodyKin_H) || ~isvalid(UpperBodyKin_H)                                                                          
        open_system(sysPath,'OpenFcn');                                                                                      
        UpperBodyKin_H = vrgcf;                                                                                                  
    end
    set(UpperBodyKin_H, 'Fullscreen', fullscreen);
    set(UpperBodyKin_H, 'NavPanel', 'minimized', 'Tooltips', 'off', 'ToolBar', 'off', 'StatusBar', 'off', 'Antialiasing', 'off');
end                                            
set_param(gcb, 'UserData', UpperBodyKin_H);
clear sysPath  autoopen  target3D_H  fullscreen;