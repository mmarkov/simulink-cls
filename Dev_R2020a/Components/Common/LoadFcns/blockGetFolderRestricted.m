function [resultPath, modelPath, relPath] = blockGetFolderRestricted(varargin)

resultPath = '';
modelPath = '';
relPath = '';
fSep = filesep;

if (isempty(varargin{1}))
    modelPath = get_param(bdroot, 'filename');
    idx = strfind(modelPath, fSep);
    modelPath = modelPath(1:idx(end));
    folderPath = uigetdir(modelPath, 'Select folder');
    if all(folderPath) ~= 0
%         if (isempty(strfind(folderPath, modelPath)))
%             modelPath = '';
%             warndlg('Please select a file from the model root directory or one of its subdirectories.');
%         else
            resultPath = folderPath;
            relPath = relativepath(resultPath, modelPath);
%         end
    else
        modelPath = '';
    end
    
elseif ~isempty(varargin{1})
    modelPath = get_param(bdroot, 'filename');
    idx = strfind(modelPath, fSep);
    modelPath = modelPath(1:idx(end));
    oldFolderPath = [modelPath varargin{1}];
    idx = strfind(oldFolderPath, fSep);
    if ~isempty(idx) 
        oldFolderPath = oldFolderPath(1:idx(end));
    end
    folderPath = uigetdir(oldFolderPath, 'Select folder');
    if all(folderPath) ~= 0
        if (isempty(strfind(folderPath, oldFolderPath)) && isempty(strfind(folderPath, modelPath)))
            modelPath = '';
            warndlg('Please select a file from the root directory of the already selected file or one of its subdirectories.');
        else
            resultPath = folderPath;
            relPath = relativepath(resultPath, modelPath);
        end
    else
        modelPath = '';
    end
    
elseif length(varargin) > 2    
    warndlg('The function "blockgetFolderRestricted" takes one input argument at max.');

end

end