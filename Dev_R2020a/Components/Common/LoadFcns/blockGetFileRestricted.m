function [resultPath, resultFile, modelPath, relPath] = blockGetFileRestricted(varargin)

resultPath = '';
resultFile = '';
modelPath = '';
relPath = '';
fSep = filesep;

if (isempty(varargin{1}) && (length(varargin) == 1 || length(varargin) == 2) || isempty(varargin))
    if length(varargin) == 2
        fileFormat = varargin{2};
    else
        fileFormat = '';
    end
    modelPath = get_param(bdroot, 'filename');
    idx = strfind(modelPath, fSep);
    modelPath = modelPath(1:idx(end));
    [filename, filepath] = uigetfile(fileFormat, 'Select file', modelPath);
    if all(filename) ~= 0
        if (isempty(strfind(filepath, modelPath)))
            modelPath = '';
            warndlg('Please select a file from the model root directory or one of its subdirectories.');
        else
            resultPath = [filepath filename];
            resultFile = filename;
            relPath = relativepath(resultPath, modelPath);
        end
    else
        modelPath = '';
    end
    
elseif ~isempty(varargin(1)) && (length(varargin) == 1 || length(varargin) == 2)
    if length(varargin) == 2
        fileFormat = varargin{2};
    else
        fileFormat = '';
    end
    modelPath = get_param(bdroot, 'filename');
    idx = strfind(modelPath, fSep);
    modelPath = modelPath(1:idx(end));
    oldFilePath = [modelPath varargin{1}];
    idx = strfind(oldFilePath, fSep);
    if ~isempty(idx)
        oldFilePath = oldFilePath(1:idx(end));
    end
    [filename, filepath] = uigetfile(fileFormat, 'Select file', oldFilePath);
    if all(filename) ~= 0
        if (isempty(strfind(filepath, oldFilePath)) && isempty(strfind(filepath, modelPath)))
            modelPath = '';
            warndlg('Please select a file from the model''s root directory or one of its subdirectories.');
        else
            resultPath = [filepath filename];
            resultFile = filename;
            relPath = relativepath(resultPath, modelPath);
        end
    else
        modelPath = '';
    end
    
elseif length(varargin) > 2    
    warndlg('The function "blockgetFileRestricted" takes only one input argument at max.');

end

end