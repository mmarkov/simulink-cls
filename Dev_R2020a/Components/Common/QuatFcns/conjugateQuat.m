function qOut = conjugateQuat( q ) 

% quatConjugate = Conjugate of quaternion q
% Conjugation does not affect scalar numbers, but reverses the sign of 
% imaginary numbers. For unit quaternions, the conjugate equals the inverse.

qOut = [ q(:,1)  -q(:,2:4) ];
