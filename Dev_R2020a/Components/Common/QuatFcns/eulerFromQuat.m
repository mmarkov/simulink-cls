function eulerAngles = eulerFromQuat( q )

% quatToEuler = Euler Angles in the form ZYX.
%
% Euclidean vectors can be represented in quaternion form.
% The quaternion [q0, q1, q2, q3] (where q0 represents the scalar, and q1-3
% the vecor part) can be therefore rewritten as:
%
%       q0 = qw = cos(alpha/2)
%       q1 = qx = sin(alpha/2) * cos(beta(x))
%       q2 = qy = sin(alpha/2) * cos(beta(y))
%       q3 = qz = sin(alpha/2) * cos(beta(z))
%
% where alpha is a rotation angle and cos(beta(x,y,z)) the direction
% cosines, representing the axis of rotation.
% Additional information about the conversion can be obtained from
% "A tutorial on SE(3) transformation parameterizations" by Jose-Luis
% Blanco (2010).
%
% In addition:
% The inverse sine (sin^-1 or arcsin) is defined as x = sin(y) and
% operates in the domain of -1 <= x <= 1 for a real result, or in other
% words, in the range of -pi/2 <= y <= pi/2 principal values.
% The existence of unclear positions in the yaw-pitch-roll
% representation outside this values, represent a problem during the
% convertion of a quaternion to euler angles.
% This forces us to delimit the descriminant, defined as:
%
%       d = -(qx.*qz - qw.*qy)
%
% To the named ranges of arcsin.

% Quaternion Norm:

q = bsxfun(@rdivide,q,sqrt(sum(q.^2,2)));
q(~isfinite(q)) = 1;

% Quaternion definition:

qw = q(:,1);
qx = q(:,2);
qy = q(:,3);
qz = q(:,4);

% Output prealocation:

eulerAngles = zeros(size(q,1), 3, 'like', q);


% Definition of arcsin:

arcSin = -2*(qx.*qz - qw.*qy);
arcSin(arcSin > 1) = 1;
arcSin(arcSin < -1) = -1;

% Quaternion to Euler convertion as defined in Blanco (2010):

eulerAngles = [ atan2( 2*(qx.*qy+qw.*qz), qw.^2 + qx.^2 - qy.^2 - qz.^2 ), ...
    asin( arcSin ), ...
    atan2( 2*(qy.*qz+qw.*qx), qw.^2 - qx.^2 - qy.^2 + qz.^2 )];

end

