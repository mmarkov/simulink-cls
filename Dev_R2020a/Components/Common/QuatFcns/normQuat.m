function qOut = normQuat( q )

% quatNormalize = Norm value of the input quaternion.

qnorm = zeros(size(q,1),1, 'like', q);
for i = size(q,1):-1:1
    qnorm(i,:) = norm(q(i,:),2);
end

qOut = qnorm.*qnorm;
