function dcm = DCMFromQuat( q )

% quatToDCM returns the Direction Cosine Matrix to a given input
% Quaternion.
%
%   Input:          q           quaternion array (N x 4) of the form 
%                               [q0, q1, q2, q3] (where q0 represents the 
%                               scalar, and q1-3 the vecor part)
%
%   Output:         dcm         direction cosine matrix (3 x 3 x N)
%
% Direction Cosine Matrices (DCM) are used to transform coordinate reference
% frames  into eachother, and represent three unit vectors describing a
% coordinate reference frame.
%
% The conversion from a quaternion to a DCM is described e.g. in 
% "Introduction into quaternions for spacecraft attitude
% representation" by Grossekatthöfer et al. (2012).

% Quaternion normalization:

qNormalized = normalizationQuat( q );

% Output prealocation:

dcm = zeros(3,3,size(qNormalized,1),'like',qNormalized);

% Quaternion to DCM convertion as defined in Grossekatthöfer et al. (2012):

dcm(1,1,:) = qNormalized(:,1).^2 + qNormalized(:,2).^2 - qNormalized(:,3).^2 - qNormalized(:,4).^2;

dcm(1,2,:) = 2.*(qNormalized(:,2).*qNormalized(:,3) + qNormalized(:,4).*qNormalized(:,1));

dcm(1,3,:) = 2.*(qNormalized(:,2).*qNormalized(:,4) - qNormalized(:,3).*qNormalized(:,1));

dcm(2,1,:) = 2.*(qNormalized(:,2).*qNormalized(:,3) - qNormalized(:,4).*qNormalized(:,1));

dcm(2,2,:) = qNormalized(:,1).^2 - qNormalized(:,2).^2 + qNormalized(:,3).^2 - qNormalized(:,4).^2;

dcm(2,3,:) = 2.*(qNormalized(:,3).*qNormalized(:,4) + qNormalized(:,2).*qNormalized(:,1));

dcm(3,1,:) = 2.*(qNormalized(:,2).*qNormalized(:,4) + qNormalized(:,3).*qNormalized(:,1));

dcm(3,2,:) = 2.*(qNormalized(:,3).*qNormalized(:,4) - qNormalized(:,2).*qNormalized(:,1));

dcm(3,3,:) = qNormalized(:,1).^2 - qNormalized(:,2).^2 - qNormalized(:,3).^2 + qNormalized(:,4).^2;