function qOut = inverseQuat( q )

% quatInverse = Inverse of quaternion q
% For unit quaternions, the inverse equals the conjugate.

qOut  = conjugateQuat( q )./(normQuat( q )*ones(1,4));