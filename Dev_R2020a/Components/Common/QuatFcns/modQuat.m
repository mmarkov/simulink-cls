function modulusQuat = modQuat( q )


modulusQuat = zeros(size(q,1),1, 'like', q);
for i = size(q,1):-1:1
    modulusQuat(i,:) = norm(q(i,:),2);
end
