function blockAutoFormat(varargin)

switch nargin 
    case 1
        rescaleFactor = varargin{1};
    case 2
        bWidth = varargin{1};
        bHeight = varargin{2};
end

model_Handle = bdroot;
isModel = strcmp(get_param(model_Handle, 'LibraryType'), 'None') && ...
    strcmp(get_param(model_Handle, 'Type'), 'block_diagram');

if (~isModel && strcmp(get_param(model_Handle, 'Lock'),'off')) || ...            % the operation can only be executed on unlocked-library blocks or not-running sim-models
        (isModel && strcmp(get_param(model_Handle, 'SimulationStatus'), 'stopped'))
    
    if exist('bWidth', 'var') && exist('bHeight', 'var')
        % Automatically resize the block to the fixed dimensions
        blockWidth = bWidth;
        blockHeight = bHeight;
        currentPos = get_param( gcb, 'Position');
        currentWidth = currentPos( 3 ) - currentPos( 1 );
        currentHeight = currentPos( 4 ) - currentPos( 2 );
        if (currentWidth ~= blockWidth) || (currentHeight ~= blockHeight)
            set_param(gcb, 'Position', [currentPos( 1 ) currentPos( 2 ) ...
                currentPos( 1 ) + blockWidth currentPos( 2 ) + blockHeight]);
        end
        
        fontWeight = 'bold';
        fontSize = 13;
        set_param( gcb, 'Fontweight', fontWeight)
        set_param( gcb, 'FontSize', fontSize);
    else
        %% SETUP THE BLOCK APPEREANCE
        if ~exist('rescaleFactor', 'var')
            rescaleFactor = 1;
        end
        
        %Setup the Text Font properties
        fontWeight = 'bold';
        fontSize = round(13 - rescaleFactor^2);
        
        if ~strcmp( get_param(gcb , 'Fontweight'), fontWeight) || (get_param(gcb , 'FontSize') ~= fontSize)
            set_param( gcb, 'Fontweight', fontWeight)
            set_param( gcb, 'FontSize', fontSize);
        end
        
        % Automatically resize the block to the fixed dimensions
        blockWidth = round(200/rescaleFactor);
        blockHeight = round(160/rescaleFactor);
        currentPos = get_param( gcb, 'Position');
        currentWidth = currentPos( 3 ) - currentPos( 1 );
        currentHeight = currentPos( 4 ) - currentPos( 2 );
        if (currentWidth ~= blockWidth) || (currentHeight ~= blockHeight)
            set_param(gcb, 'Position', [currentPos( 1 ) currentPos( 2 ) ...
                currentPos( 1 ) + blockWidth currentPos( 2 ) + blockHeight]);
        end
    end
end
end