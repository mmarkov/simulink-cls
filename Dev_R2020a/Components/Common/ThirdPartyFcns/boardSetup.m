% partly copied from rbtlkgui.m in rtwin library

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  INSTALLNEWBOARD
%  callback for the install new board button
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function boardSetup(install)
if (strcmp(get_param(gcs,'SimulationStatus'), 'external') | strcmp(get_param(gcs,'SimulationStatus'), 'running')) == 0
    warning('off','Simulink:Masking:ClosingBlockDialog');
    
    % get all supported daq cards
    if  getMatlabVersion() <= 8.4
        drvdir = fullfile(matlabroot,'toolbox','rtw','targets','rtwin', 'drv', rtwintargetarch); %~absolut path?
        mfrlist = dir(drvdir);
        mfrlist = mfrlist([mfrlist.isdir]);
    else
        drvdir = SLDRT.getDriverDir;
        mfrlist = dir(drvdir);
        mfrlist = mfrlist([mfrlist.isdir]);
    end
    
    clear alldrv
    alldrv = {};
    
    for mfr = sort({mfrlist.name})
        % get driver list per directory, rule out parent and empty directories
        if mfr{1}(1)=='.'; continue; end
        drvlist = dir(fullfile(drvdir, mfr{1}, '*.rwd'));
        if isempty(drvlist); continue; end
        alldrv = [alldrv sort({drvlist.name})];
    end
    
    % get all already installed boards
    boards = getpref('RealTimeWindowsTarget', 'InstalledBoards', struct('DrvName',{},'DrvAddress',{}));
    InstalledBoards = {boards.DrvName};
    
    %find all connected daq cards
    daqreset;
    ai = daq.getDevices;
    
    % get mask of Block
    maskObj = Simulink.Mask.get(gcb);
    
    if (isempty(ai))
        % set dropdown menu items
        indOfParam = find(strcmp({maskObj.Parameters.Name},'daq'));
        maskObj.Parameters(indOfParam).TypeOptions = {'none'};
        set_param(gcb,'displayDAQStatus','This PC has no available DAQ cards!');
        return;
    end
    
    allDev = {};
    menu = {};
    
    DevAlreadyInstalled = ones(length(ai),1);
    % display connected devices
    for i = 1:length(ai)
        vendor = ai(i).Vendor.FullName;
        model = ai(i).Model;
        DevStr = ['National_Instruments/' model];
        DisplayDev = DevStr;
        if ~ismember(DevStr, InstalledBoards)
            DisplayDev = [DevStr ' (not installed)'];
            DevAlreadyInstalled(i) = 0;
        end
        if ~ismember([model '.rwd'], alldrv)
            DisplayDev = [DevStr ' (not supported)'];
            DevAlreadyInstalled(i) = -1;
        end
        
        allDev(i) = {DevStr};
        menu(i) = {DisplayDev};
    end
    
    
    
    % set dropdown menu items
    indOfParam = find(strcmp({maskObj.Parameters.Name},'daq'));
    maskObj.Parameters(indOfParam).TypeOptions = menu;
    
    choice = get_param(gcb,'daq');
    
    ind = find(strcmp(choice, menu));
    
    blk = [gcb '/Analog Input'];
    
    % if board already installed change to it
    if (DevAlreadyInstalled(ind) == 1)
        set_param(gcb,'displayDAQStatus','Board available');
        ind2 = find(strcmp({boards.DrvName}, choice));
        set_param(blk, 'DrvName', choice, 'DrvAddress', int2str(boards(ind2).DrvAddress)); % DrvOptions
        if (install) set_param(gcb,'displayDAQStatus','Board already installed.'); end
        % if board is not supported do nothing
    elseif (DevAlreadyInstalled(ind) == -1)
        set_param(gcb,'displayDAQStatus','Board is not compatible.');
        % if board not installed and supported install it when checkbox is pressed
    else
        if (install)
            set_param(gcb,'displayDAQStatus','Installing board ....');
            NewBoardInstalled(choice(1:end-16));
        else
            set_param(gcb,'displayDAQStatus','Please install board.');
        end
    end
    warning('on','Simulink:Masking:ClosingBlockDialog');
    
else
    set_param(gcb,'displayDAQStatus','Simulation in progress...');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  NEWBOARDINSTALLED
%  callback when new board installed
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function NewBoardInstalled(choice)

blk = [gcb '/Analog Input'];
drvname = choice;

% call the driver GUI
while true
    opt = rtdrvgui('Open', drvname);
    if isempty(opt)
        return;
    end
    addr = opt(1);
    opt(1) = [];
    if ~IsBoardConflict(drvname, addr)
        break;
    end
    if ~HasBoardGUI(drvname)
        return;
    end
end

% set the board parameters
set_param(blk, 'DrvName', drvname, 'DrvAddress', int2str(addr), 'DrvOptions', mat2str(opt));

% store board to preferences if not yet there
AddInstalledBoard(drvname,addr);
set_param(gcb,'displayDAQStatus','Board installed.');
boardSetup(false);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  GETINSTALLEDBOARDS
%  get all installed boards from preferences
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [boards, brdpopup] = GetInstalledBoards

% retrieve all the installed boards from preferences
boards = getpref('RealTimeWindowsTarget', 'InstalledBoards', struct('DrvName',{},'DrvAddress',{}));

% build board popup string if requested
if nargout>1
    if isempty(boards)
        brdpopup = '< no board installed >';
    else
        brdpopup = '';
        for brd=boards
            brdpopup = [brdpopup '|' PrintableDrvName(brd.DrvName, brd.DrvAddress)];  %#ok<AGROW>
        end
        brdpopup = ['< no board selected >' brdpopup];
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  ADDINSTALLEDBOARD
%  add a new board to the list of installed boards
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function AddInstalledBoard(drvname, drvaddress)

% build the board structure
brd.DrvName = drvname;
brd.DrvAddress = drvaddress;

% add the board to the boards array
boards = [GetInstalledBoards brd];
setpref('RealTimeWindowsTarget','InstalledBoards', SortBoards(boards));



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  SETINSTALLEDBOARD
%  set new value for a specific installed board
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function SetInstalledBoard(drvname, oldaddress, newaddress)

% find the board in the boards array
boards = GetInstalledBoards;
brdidx = FindBoardIndex(boards, drvname, oldaddress);
if (brdidx==0)
    return;
end

% change the old address to the new address
boards(brdidx).DrvAddress = newaddress;
setpref('RealTimeWindowsTarget','InstalledBoards', SortBoards(boards));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  ISBOARDCONFLICT
%  test if a board/address pair already exists
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function conflict = IsBoardConflict(drvname, drvaddress)

allboards = GetInstalledBoards;
for b = allboards
    if strcmpi(drvname,b.DrvName) && drvaddress==b.DrvAddress
        uiwait(errordlg({'You have selected an address for this board that is already'
            'in use by another installed board. If this is really the correct'
            'setting, please select the appropriate board from'
            'the board popup menu instead of setting the conflicting address here.'}, ...
            'Board address setting conflict','modal'));
        conflict = 1;
        return;
    end
end
conflict = 0;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  SORTBOARDS
%  sort the boards array - board name, then board address
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function boards = SortBoards(boards)

if ~isempty(boards)
    [~, srt] = sort([boards.DrvAddress]);
    boards = boards(srt);
    [~, srt] = sort({boards.DrvName});
    boards = boards(srt);
end
