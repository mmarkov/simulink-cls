function deleteBuildFiles(model_Handle)
if strcmp(get_param(model_Handle, 'SimulationStatus'), 'stopped')
    disp('CLEAR BUILD - In progress...');
    fileExt = {'.eep', '.elf', '.hex', '_sfun.mexw64', '.rxw64', '.slxc'};
    dirExt = {'slprj', '_sldrt_win64', '_ert_rtw'};
    systempath = which(model_Handle);
    completepath = systempath(1:length(systempath) - length(model_Handle) - 4);
    dirstr = dir(completepath); 
    for i = 1:length(dirstr)
        if dirstr(i).isdir == 1
            dirname = dirstr(i).name;
            for j = 1:length(dirExt)
                if ~isempty(strfind(dirname, dirExt{j}))
                    try
                        rmdir([completepath dirname], 's');
                    catch
                        
                    end
                end
            end
        else
            filename = dirstr(i).name;
            for j = 1:length(fileExt)
                if ~isempty(strfind(filename, fileExt{j}))
                    try
                        delete([completepath filename]);
                    catch
                        
                    end
                end
            end
        end
    end
    set_param(model_Handle,'MetaData', []);
    disp('CLEAR BUILD - Done. All build files have been deleted.');
else
    disp('CLEAR BUILD - Cannot delete build files, simulation is still in progress.');
end

end