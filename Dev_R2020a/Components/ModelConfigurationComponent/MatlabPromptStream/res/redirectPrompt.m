function redirectPrompt(simBlock)
%setPrompt Sets the Command Window prompt
%
% Syntax:
%    setPrompt(newPrompt)
%
% Description:
%    setPrompt(newPrompt) sets the Command Window prompt to the specified
%    NEWPROMPT. NEWPROMPT can be one of the following:
%
%      - a static string:        setPrompt('>> ')
%             => this is the default prompt string ('>> ')
%
%      - an evaluable string:    setPrompt('datestr(now)')
%             => the new prompt will look like: '25-Jan-2010 01:00:51'
%             Note: the evaluable string is expected to return a string
%
%      - an evaluable function:  setPrompt(@()(['[',datestr(now),'] ']))
%             => the new prompt will look like: '[25-Jan-2010 01:00:51] '
%             Note: the evaluable function is expected to return a string
%
%      - the static string 'timestamp' will continuously update the last
%        (current) prompt with the current time: '[25-Jan-2010 01:00:51] '
%        This has the effect of displaying desktop command execution times.
%        The 'timestamp' string can be used with other static text to
%        customize its appearance. For example: setPrompt('<timestamp!> ').
%
%      - an empty value or no input argument restores the default prompt
%
% Examples:
%    setPrompt('[-]')                   % Replaces '>> ' prompt with '[-]'
%    setPrompt('%')                     % => '%  ' (space-padded)
%    setPrompt('sprintf(''<%f>'',now)') % => '<734163.056262>'
%    setPrompt('datestr(now)')          % => '25-Jan-2010 01:00:51' (unchanging)
%    setPrompt('[''['',datestr(now),''] '']') % => '[25-Jan-2010 01:00:51] '
%    setPrompt(@()(['[',datestr(now),'] ']))  % => '[25-Jan-2010 01:00:51] '
%        (note that these are the same: the first uses an evaluable string,
%         while the second uses an evaluable function)
%    setPrompt('timestamp')             % => '[25-Jan-2010 01:00:51] ' (continuously-updated)
%    setPrompt('<timestamp> ')          % => '<25-Jan-2010 01:00:51> ' (continuously-updated)
%    setPrompt('>> ')                   % restores the default prompt
%    setPrompt('')                      % restores the default prompt
%    setPrompt                          % restores the default prompt
%
% Known issues/limitations:
%    - Prompts shorter than the default prompt are space-padded
%    - When selecting desktop text and pasting to the Editor, the prompt
%      is not stripped as it would be with the default prompt ('>> ')
%    - Continuously-updated prompts sometimes interfere with tab popups
%      and text selection
%    - Problems with Macs (Desktop becomes unresponsive)
%
% Warning:
%    This code heavily relies on undocumented and unsupported Matlab functionality.
%    It works on Matlab 7+, but use at your own risk!
%
% Technical explanation:
%    A technical explanation of the code in this utility can be found on
%    <a href="http://undocumentedmatlab.com/blog/setprompt-setting-matlab-desktop-prompt/">http://undocumentedmatlab.com/blog/setprompt-setting-matlab-desktop-prompt/</a>
%
% Bugs and suggestions:
%    Please send to Yair Altman (altmany at gmail dot com)
%
% Change log:
%    2014-10-10: Fixed for R2014a
%    2010-02-01: Added R14SP3 compatibility fix suggested by J.G. Dalissier
%    2010-01-29: Fixed a few edge-cases (some reported by J.G. Dalissier) with '>> ' terminated prompts; fixed a few problems with continuous timestamps; enabled customizing continuous timestamp prompts; added Mac warning
%    2010-01-26: Fixed a few edge cases (some inspired by J. Raymond); added continuously-updated timestamp option
%    2010-01-25: First version posted on the <a href="http://www.mathworks.com/matlabcentral/fileexchange/authors/27420">MathWorks File Exchange</a>
%
% See also:
%    fprintf, cprintf (on the File Exchange)

% License to use and modify this code is granted freely to all interested, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.

% Programmed and Copyright by Yair M. Altman: altmany(at)gmail.com
% $Revision: 1.04 $  $Date: 2014/10/10 08:31:29 $

% TODO: Mac problems: try PostSet listener; check pre-existing CaretUpdateCallback

% Generate a warning to Mac users
try
    if ismac
        warningStr = sprintf('The MATLAB command window display block is disabled in MacOS');
        warning('YMA:setPrompt:Mac',warningStr);  %#ok sprintf format (compatibility)
        return;
    end
catch
    % never mind - R14SP3 compatibility
end

% Get the reference handle to the Command Window text area
jDesktop = com.mathworks.mde.desk.MLDesktop.getInstance;
try
    cmdWin = jDesktop.getClient('Command Window');
    jTextArea = cmdWin.getComponent(0).getViewport.getComponent(0);
catch
    commandwindow;
    jTextArea = jDesktop.getMainFrame.getFocusOwner;
end
jTextArea = handle(jTextArea, 'CallbackProperties');

% Instrument the text area's callback
if simBlock ~= 0
    set(jTextArea,'CaretUpdateCallback',{@setPromptFcn,simBlock});
else
    set(jTextArea,'CaretUpdateCallback','');
end
end  % setPrompt


function setPromptFcn(jTextArea,eventData,simBlock)
% preserve the last-modified prompt string
persistent inProgress refreshCLK
if isempty(inProgress)
    inProgress = 0;  %#ok unused
    refreshCLK = tic;
end

if inProgress
    return;
end

if toc(refreshCLK) >= 0.03 % Refresh every 30 ms
    refreshCLK = tic;
    inProgress = 1;
    try
        % Ensure we have the relevant Java reference handle
        if isnumeric(jTextArea) || isempty(jTextArea)
            jTextArea = get(eventData,'Source');
        end
        try jTextArea = jTextArea.java;  catch,  end  %#ok
        % Get text
        cwText = get(jTextArea,'Text');
        maxLength = 5000;
        if length(cwText) >= maxLength
            cwText = cwText(1,end-maxLength+1:end);
        end
        % Ensure that the block exists
        try
            set_param(simBlock, 'UserData', cwText);
        catch
            set(jTextArea,'CaretUpdateCallback',''); % Turn off if the error occurs
            inProgress = 0;
            return;
        end
        
        % Force refresh of the simBlock
        val = str2double(get_param(simBlock, 'forceRedraw'));
        if val>=1000
            val = 0;
        end
        set_param(simBlock, 'forceRedraw' , num2str(val+1));
    catch
        % Never mind - ignore...
    end
    inProgress = 0;
end
end  % setPromptFcn