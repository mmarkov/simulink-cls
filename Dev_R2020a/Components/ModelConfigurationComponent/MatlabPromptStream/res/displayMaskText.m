color('red');
text(0.6, 0.95, 'Matlab-Console Stream; ACTIVE');
color('black');
try
    cmdOut = get_param(gcb,'UserData');
    if ~isempty(cmdOut) && ischar(cmdOut)
        stringLines = strsplit(cmdOut, '\n');
        % Exclusion criterias
        okStringLines = {};
        for i = 1:length(stringLines)
            if ~isempty(stringLines{i}) && ...                                         % not a empty line
                    numel(strfind(stringLines{i},' ')) ~= numel(stringLines{i}) && ... % not a space line
                    isempty(strfind(stringLines{i}, 'K>>')) &&  ...                    % not a debug (brekapoint) line
                    isempty(strfind(stringLines{i}, '>>'))                             % not a user-input line
                okStringLines = [ okStringLines, stringLines(i) ];
            end
        end
        % Display only the last N lines
        maxNumLines = str2double(get_param(gcb,'maxNumlines'));
        if numel(okStringLines)>maxNumLines
            okStringLines(1:end-maxNumLines) = [];
        end
        if ~isempty(okStringLines)
            % Insert a new line after the indicated number of characters
            maxNumChar = get_param(gcb,'maxNumChar');
            c = 1;
            formatStringLines = {};
            if ~(strcmp(maxNumChar,'Unlimited'))
                maxNumChar = str2double(maxNumChar);
                for i = 1 : length(okStringLines)
                    numOfSubStrings = ceil(length(okStringLines{i})/maxNumChar);
                    for j = 1: numOfSubStrings
                        try
                            formatStringLines{c} = okStringLines{i}((j-1)*maxNumChar + 1:j*maxNumChar);
                        catch
                            formatStringLines{c} = okStringLines{i}((j-1)*maxNumChar + 1:end);
                        end
                        c = c+1;
                    end
                end
            else
                formatStringLines = okStringLines;
            end
            % Display only the last N lines
            if numel(formatStringLines)>maxNumLines
                formatStringLines(1:end-maxNumLines) = [];
            end
            if ~isempty (formatStringLines)
                cmdOut = strjoin(formatStringLines, '\n');
                text(0.05, 0.5, cmdOut);
            end
        end
    end
catch
    % Something went wrong. Disable the verbose mode
    redirectPrompt(0);
    color('red');
    text(0.65, 0.95, 'Matlab-Console; ACTIVE');
    text(0.05, 0.5, 'SOMETHING WENT WRONG ... Streaming stopped!');
    
end
