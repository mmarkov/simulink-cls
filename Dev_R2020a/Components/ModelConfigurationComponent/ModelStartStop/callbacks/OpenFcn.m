modelName1 = bdroot;
blockName = gcb;
if strcmp(get_param(modelName1, 'LibraryType'), 'None') && strcmp(get_param(modelName1, 'Type'), 'block_diagram') && ~isempty(get_param(modelName1,'filename'))
    % When running the model, first switch the current directory to the model's
    % directory.
    % Reason: In order to execute the real time windows target model, the
    % current directory must be the folder where the model executable was
    % built (see setting of build folder below).
    fullFilePath = get_param(modelName1,'filename');
    idx = strfind(fullFilePath,filesep);
    cd(fullFilePath(1:idx(end)-1));
    
    % Also, set the build and cache folders to the current directory.
    % Reason: This way, build files will be built in the model's directory,
    % instead of whereever you happen to be at the moment.
    cacheFolderPath = fullFilePath(1:idx(end)-1);
    codeGenFolderPath = fullFilePath(1:idx(end)-1);
    
    if ~(strcmp(Simulink.fileGenControl('get', 'CacheFolder'), cacheFolderPath) && strcmp(Simulink.fileGenControl('get', 'CodeGenFolder'), codeGenFolderPath))
        Simulink.fileGenControl('set', 'CacheFolder', cacheFolderPath,'CodeGenFolder', codeGenFolderPath, 'keepPreviousPath', true, 'createDir', true);
    end
    clear idx fullFilePath cacheFolderPath codeGenFolderPath
    
    % Prepare the callback for the first time use
    userData = get_param(blockName, 'UserData');
    if isempty(userData) || ~isfield(userData,'sysReady') || ~isfield(userData,'Text')
        userData.sysReady = 1;
        userData.Text = 'START';
    end
    if strcmp(userData.Text,'START') && userData.sysReady && strcmp(get_param(modelName1,'SimulationStatus'),'stopped')
        userData.Text = 'STOP';
        userData.sysReady = 0;
        set_param(blockName, 'UserData', userData);
        % Force refresh of the block
        val = str2double(get_param(blockName, 'forceRedraw'));
        if val>=1000
            val = 0;
        end
        set_param(blockName, 'forceRedraw' , num2str(val+1));
        % Start 
        set_param( modelName1, 'SimulationMode', 'external' );
        set_param( modelName1, 'SimulationCommand', 'Start' );
        if strcmp(get_param(modelName1,'SimulationStatus'),'stopped') % an error occured
            userData.Text = 'START';
            userData.sysReady = 1;
            set_param(blockName, 'UserData', userData);
            % Force refresh of the block
            val = str2double(get_param(blockName, 'forceRedraw'));
            if val>=1000
                val = 0;
            end
            set_param(blockName, 'forceRedraw' , num2str(val+1));
        end
        % Cleanup
        clear modelName1 blockName val systempath userData
    elseif (strcmp(userData.Text,'STOP') && userData.sysReady)
        set_param( modelName1, 'SimulationCommand', 'Stop' );
        clear modelName1 blockName val systempath userData
    end
end
