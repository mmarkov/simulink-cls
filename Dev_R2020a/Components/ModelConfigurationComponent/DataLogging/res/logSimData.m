function logSimData(model_Handle, block_Handle)
fSep = filesep;
if strcmp(get_param(block_Handle,'logdata'),'on')
   
    disp('Saving data .........................');
    if isempty(evalin('base','who(''simout*'')'))
        disp('Data is corrupted (skipped saving)............. ');
    else
        dirName = get_param(block_Handle,'fDir1');
        if ~exist(dirName,'dir')
            disp('Selected directory for data logging does not exist .............'); 
            modelDir = which(model_Handle);
            idx  =  strfind(modelDir, fSep);
            modelDir = modelDir(1:idx(end));
            dirName =  [modelDir 'Recorded_Data'];
            status = mkdir(dirName);
            if status
                disp('Logging directory created in the working directory ...............');
            end
        end
        
        % File name
        fName =  get_param(block_Handle,'fName');
        
        %File path
        fullPath = fullfile(dirName,fName);
        
        %Append number
        c = 1;
        while exist([fullPath '_' num2str(c) '.mat'],'file')
            c = c+1;
        end
        newFile = [fullPath '_' num2str(c) '.mat'];
        
        % Count From blocks
        froms = find_system(block_Handle, 'FollowLinks', 'on', 'LookUnderMasks', 'all', 'BlockType', 'From');
        numberFroms = size(froms,1);
        
        % Save list
        c = 1;
        saveflag = 0;
        varName = ['simout' get_param(froms{c}, 'GotoTag')];
        while (evalin('base',['exist(''', varName, ''',''var'')'] ) && c <= numberFroms)
            varTemp = evalin('base', ['eval(''' varName '.time'')']);
            if isempty(varTemp)
                % dont save
            else
                if saveflag == 0
                    evalin('base', ['save(''', newFile, ''',''' varName ''')']);
                    saveflag = 1;
                else
                    evalin('base', ['save(''', newFile, ''',''' varName ''',''-append'')']);
                end
            end
            c = c + 1;
            if c <= numberFroms
                varName = [ 'simout' get_param(froms{c}, 'GotoTag')];
            end
        end
        fName = strsplit(newFile,fSep);
        fName = fName{end};
        
        % Screenshot
        if saveflag == 1
            printParam = ['-s' model_Handle];
            print(printParam, '-dbitmap', 'tmp.bmp');
            Screenshot = imread('tmp.bmp');
            delete('tmp.bmp');
            
            % Block parameters
            %% find all libraries in CLS_Toolbox
            % load CLS toolbox
            CLS = load_system('CLS_Toolbox.slx');
            
            % get all blocks from toolbox...
            libTemp = find_system(CLS,'Type','block');
            % ... and determine the top level libraries
            topLibraries = get_param(libTemp,'OpenFcn');
            
            % Save all libraries:
            completeLibList = {};
            completeLibList = [topLibraries', completeLibList];
            
            for i = 1:size(topLibraries)
                % Load each Toplibrary
                load_system(topLibraries{i});
                % Add all libraries in there to completeLibList
                tempBlocks = [find_system(topLibraries{i},'Type','block')];
                LibInfoCurrentBlock = libinfo(tempBlocks,'SearchDepth',0);
                completeLibList = [LibInfoCurrentBlock.Library, completeLibList];
            end
            
            %% Save mask parameters of all blocks in model which belong to CLS
            % path of calling system
            currentSystem = which(gcs);
            
            %% find all libraries in CLS_Toolbox
            % load CLS toolbox
            CLS = load_system('CLS_Toolbox.slx');
            
            % get all blocks from toolbox...
            libTemp = find_system(CLS,'Type','block');
            % ... and determine the top level libraries
            topLibraries = get_param(libTemp,'OpenFcn');
            
            % Save all libraries:
            completeLibList = {};
            completeLibList = [topLibraries', completeLibList];
            
            for i = 1:size(topLibraries)
                % Load each Toplibrary
                load_system(topLibraries{i});
                % Add all libraries in there to completeLibList
                tempBlocks = [find_system(topLibraries{i},'Type','block')];
                LibInfoCurrentBlock = libinfo(tempBlocks,'SearchDepth',0);
                completeLibList = [LibInfoCurrentBlock.Library, completeLibList];
            end
            
            Parameters = struct();
            
            % load current model
            load_system(currentSystem);
            
            % get all blocks and Subsystems in model
            blocks = find_system(model_Handle, 'SearchDepth',1,'Type','block');
            
            % determine if block belongs to CLS and save them
            for i=1:size(blocks)
                currentBlock = blocks{i};
                
                % get blocks library info
                try
                    blktype = get_param(currentBlock,'BlockChoice');
                catch
                    blktype = [];
                end
                
                if ~isempty(blktype)
                    LibInfoCurrentBlock = libinfo([currentBlock '/' blktype], 'SearchDepth', 0);
                else
                    LibInfoCurrentBlock = libinfo(currentBlock,'SearchDepth', 0);
                end
                
                if (~isempty(LibInfoCurrentBlock))
                    % determine Library of currentBlock
                    LibCurrentBlock = LibInfoCurrentBlock.Library;
                    
                    % if block belongs to CLS
                    if (ismember(LibCurrentBlock, completeLibList))
                        
                        % if block is configurable subsystem
                        if (~isempty(get_param(currentBlock,'BlockChoice')))
                            BlockName = get_param(currentBlock,'BlockChoice');
                            curr_choice = [currentBlock '/' BlockName];
                            maskObj = Simulink.Mask.get(curr_choice);
                            
                            % normal block
                        else
                            BlockName = get_param(currentBlock,'Name');
                            maskObj = Simulink.Mask.get(currentBlock);
                        end
                        
                        maskParams = maskObj.Parameters;
                        param = struct();
                        % format block name
                        BlockName = ['block' BlockName];
                        BlockName = regexprep(BlockName,'[^a-zA-Z0-9]','_');
                        % write each parameter of mask in param
                        for j = 1:size(maskParams,2)
                            param(j).name = maskObj.Parameters(j).Prompt; %Name
                            
                            if (exist(maskObj.Parameters(j).Value,'var'))
                                tempValue = eval(maskObj.Parameters(j).Value);
                            else
                                tempValue = maskObj.Parameters(j).Value;
                            end
                            param(j).value = tempValue;
                        end
                        Parameters.(BlockName) = param;
                    end
                end
            end 
        end
        
        if saveflag == 1
            assignin('base', 'Parameters', Parameters);
            assignin('base', 'Screenshot', Screenshot);
            evalin('base', ['save(''' newFile ''', ''Parameters'' , ', '''-append'')']);
            evalin('base', ['save(''' newFile ''', ''Screenshot'' , ', '''-append'')']);
            disp(['Simulation data saved in file: ' fName ' ............. ']);
        else
            disp('Simulation data was not saved: All variables were empty');
        end
        
    end
end

