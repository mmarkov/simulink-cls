function configureForCLSDev(model_Handle, csName, forceRefresh)

if strcmp(get_param(model_Handle, 'LibraryType'), 'None') && strcmp(get_param(model_Handle, 'Type'), 'block_diagram')
    
    cs = getActiveConfigSet(model_Handle);
    if (forceRefresh || isempty(strfind(cs.get_param('Name'), csName))) && ~strcmp(cs.get_param('Name'),'External Mode Configuration')
        disp('CONFIGURING THE MODEL ... Please wait ...');
        switch csName
            case 'Desktop RT'
                cs = configDesktopRT();
            case 'Arduino ERT'
                cs = configArduinoRT();
            case 'RaspberryPi ERT'
                cs = configRaspberryRT();
            otherwise
                error('Unknow configuration set!')
        end
        % Finally, configure the model
        try
            setActiveConfigSet(model_Handle, 'Configuration');
            while true
                detachConfigSet ( model_Handle, cs.Name );
            end
        end
        attachConfigSet ( model_Handle, cs );
        setActiveConfigSet( model_Handle, cs.Name);
        % Configure callbacks
        set_param( model_Handle, 'PreLoadFcn', 'modelPreLoad;');
        set_param( model_Handle, 'InitFcn', 'modelInit;');
        set_param( model_Handle, 'CloseFcn', 'modelClose;');
        set_param( model_Handle, 'SimulationMode','External');
        disp('CONFIGURING THE MODEL... Done!');
    end
end

end




