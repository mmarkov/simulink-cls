function updateMdlTs(sysH, blockH)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if strcmp(get_param(sysH, 'LibraryType'), 'None') ...
        && strcmp(get_param(sysH, 'Type'), 'block_diagram') ...
        && ((strcmp( get_param(sysH,'SimulationStatus'), 'stopped')) || (strcmp( get_param(sysH,'SimulationStatus'), 'initializing')))
    
    % Retrive the values
    simTs = str2num(get_param(blockH,'simTs'));
    if isempty(simTs)
        simTs=evalin('base', get_param(blockH,'simTs'));
    end
    inTs = str2num(get_param(blockH,'inTs'));
    if isempty(inTs)
        inTs=evalin('base',get_param(blockH,'inTs'));
    end
    ctrlTs = str2num(get_param(blockH,'ctrlTs'));
    if isempty(ctrlTs)
        ctrlTs=evalin('base', get_param(blockH,'ctrlTs'));
    end
    sysTs = str2num(get_param(blockH,'sysTs'));
    if isempty(sysTs)
        sysTs=evalin('base', get_param(blockH,'sysTs'));
    end
    feedTs = str2num(get_param(blockH,'stimTs'));
    if isempty(feedTs)
        feedTs=evalin('base', get_param(blockH,'stimTs'));
    end
    mappTs = str2num(get_param(blockH,'feedTs'));
    if isempty(mappTs)
        mappTs=evalin('base',get_param(blockH,'feedTs'));
    end
    expTs = str2num(get_param(blockH,'expTs'));
    if isempty(sysTs)
        sysTs=evalin('base',get_param(blockH,'sysTs'));
    end
    
    simDur = str2num(get_param(blockH,'simDur'));
    if isempty(simDur)
        simDur=evalin('base',get_param(blockH,'simDur'));
    end
    
    updateAllowed = true;
    if ~evalin('base', 'exist(''simTs'',''var'')')
        updateAllowed = false; % this prevents simulink from crashing when using the ModelSettings Block for the first time
    end
    
    % Update only if simulation times have been updated
    if ~evalin('base', 'exist(''simTs'',''var'')') || (simTs ~= evalin('base','simTs') || inTs ~= evalin('base','inTs') || ...
            ctrlTs ~= evalin('base','ctrlTs') || sysTs ~= evalin('base','sysTs') || ...
            feedTs ~= evalin('base','feedTs') || mappTs ~= evalin('base','mappTs') || ...
            expTs ~= evalin('base','expTs') || simDur ~= evalin('base','simDur'))
        % Export the variables to the workspace
        assignin('base', 'simDur', simDur);
        assignin('base', 'simTs', simTs);
        assignin('base', 'inTs', inTs);
        assignin('base', 'ctrlTs', ctrlTs);
        assignin('base', 'sysTs', sysTs);
        assignin('base', 'feedTs', feedTs);
        assignin('base', 'mappTs', mappTs);
        assignin('base', 'expTs', expTs);
        %         if strcmp(get_param(sysH,'Dirty'),'on')
        %             save_system;
        %         end
        if updateAllowed
            set_param(sysH,'SimulationCommand', 'update');
        end
        
    end
end

end

