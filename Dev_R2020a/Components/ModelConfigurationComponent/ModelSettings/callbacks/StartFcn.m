% Play start audio cue
countDur = 3;  
audioCue = get_param(gcb, 'audioCue' ); 
RES = [evalin('base', 'PATH.get(gcb)') '/res/'];
[soundCue, soundFs] = audioread([RES 'simStartSound.wav']);
p = linspace(1.8, 2.5, countDur);
if countDur > 1 && strcmp(audioCue, 'on')
    j = 1;
    for i = countDur:-1:1
        pause( 0.75 );
        disp ( ['SIMULATION: Starting in : ' num2str(i) ' seconds...'])
        sound(soundCue*2, p(j)*soundFs);
        j = j + 1;
    end
end
disp ( 'SIMULATION: STARTED! ')
clear countDur RES audioCue p i j soundCue soundFs