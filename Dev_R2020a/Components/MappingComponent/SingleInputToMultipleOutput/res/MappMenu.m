function varargout = MappMenu(varargin)
% MAPPMENU MATLAB code for MappMenu.fig
%      MAPPMENU, by itself, creates a new MAPPMENU or raises the existing
%      singleton*.
%
%      H = MAPPMENU returns the handle to a new MAPPMENU or the handle to
%      the existing singleton*.
%
%      MAPPMENU('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAPPMENU.M with the given input arguments.
%
%      MAPPMENU('Property','Value',...) creates a new MAPPMENU or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MappMenu_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MappMenu_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MappMenu

% Last Modified by GUIDE v2.5 15-Dec-2015 14:35:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MappMenu_OpeningFcn, ...
                   'gui_OutputFcn',  @MappMenu_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MappMenu is made visible.
function MappMenu_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MappMenu (see VARARGIN)

% Choose default command line output for MappMenu
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes MappMenu wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Load input argument if not empty
if ~isempty(varargin)
    tmp = varargin{1};
    loaddata = load(tmp{1});
    if ~isempty(loaddata)
        set(handles.tblMapping, 'Data', loaddata.mappData);
    end
end


% --- Outputs from this function are returned to the command line.
function varargout = MappMenu_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in pmFromChan.
function pmFromChan_Callback(hObject, eventdata, handles)
% hObject    handle to pmFromChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pmFromChan contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pmFromChan


% --- Executes during object creation, after setting all properties.
function pmFromChan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pmFromChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pmToChan.
function pmToChan_Callback(hObject, eventdata, handles)
% hObject    handle to pmToChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pmToChan contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pmToChan


% --- Executes during object creation, after setting all properties.
function pmToChan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pmToChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnCopyChan.
function btnCopyChan_Callback(hObject, eventdata, handles)
% hObject    handle to btnCopyChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btnOpenSB.
function btnOpenSB_Callback(hObject, eventdata, handles)
% hObject    handle to btnOpenSB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

hMappingMenu = gcf;
run('SignalBuilder.m');
hSignalBuilder = gcf;
set(hMappingMenu, 'UserData', hSignalBuilder);

% --- Executes on button press in btnSaveMapp.
function btnSaveMapp_Callback(hObject, eventdata, handles)
% hObject    handle to btnSaveMapp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

flag = 0;
mappData = get(handles.tblMapping, 'Data');
for i = 1:length(mappData)
    checkBp = str2num(mappData{i,1}); %#ok<*ST2NM>
    checkInt = str2num(mappData{i,2});
    checkFreq = str2num(mappData{i,3});
    if checkInt == 1
        checkInt = ones(1, length(checkBp));
        mappData{i,2} = ['[' regexprep(num2str(checkInt), ' *', ' ') ']'];
    end
    if checkFreq == 1
        checkFreq = ones(1, length(checkBp));
        mappData{i,3} = ['[' regexprep(num2str(checkFreq), ' *', ' ') ']'];
    end
    if ~isempty(checkBp)
       if ~(((isempty(checkInt)) || (length(checkInt) == length(checkBp))) && ((isempty(checkFreq)) || (length(checkFreq) == length(checkBp))))
           flag = 1;
       end
    end
end

if flag == 0
    %currentDir = which('MappingBlock.slx');
    %currentDir = [currentDir(1:end-length('MappingBlock.slx')) 'DefaultMappings'];
    %currentDir = fullfile(currentDir, '.mat');
    [filename, path] = uiputfile('*.mat', 'Save mapping');
    if filename ~= 0
        save( fullfile( path, filename ), 'mappData' );
    end
else
    warning('The number of elements in breakpoints, intensity and frequency must be the same!');
end

% --- Executes on selection change in listInputs.
function listInputs_Callback(hObject, eventdata, handles)
% hObject    handle to listInputs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listInputs contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listInputs


% --- Executes during object creation, after setting all properties.
function listInputs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listInputs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listMappings.
function listMappings_Callback(hObject, eventdata, handles)
% hObject    handle to listMappings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listMappings contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listMappings


% --- Executes during object creation, after setting all properties.
function listMappings_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listMappings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnConnect.
function btnConnect_Callback(hObject, eventdata, handles)
% hObject    handle to btnConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listConnections.
function listConnections_Callback(hObject, eventdata, handles)
% hObject    handle to listConnections (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listConnections contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listConnections


% --- Executes during object creation, after setting all properties.
function listConnections_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listConnections (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnDisconnect.
function btnDisconnect_Callback(hObject, eventdata, handles)
% hObject    handle to btnDisconnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btnDefault.
function btnDefault_Callback(hObject, eventdata, handles)
% hObject    handle to btnDefault (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

table = cell(16,3);
for chan = 1:16
    table(chan, 1) = cellstr('');
    table(chan, 2) = cellstr('');
    table(chan, 3) = cellstr('');
end
set(handles.tblMapping, 'Data', table);

% --- Executes on button press in btnApply.
function btnApply_Callback(hObject, eventdata, handles)
% hObject    handle to btnApply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btnHelp.
function btnHelp_Callback(hObject, eventdata, handles)
% hObject    handle to btnHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
open('MappMenu_help.html');

% --- Executes on button press in btnPlotMapp.
function btnPlotMapp_Callback(hObject, eventdata, handles)
% hObject    handle to btnPlotMapp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

tbl = get(handles.tblMapping, 'Data');
plotChMapp(tbl);


% --- Executes on selection change in pmSignalTo.
function pmSignalTo_Callback(hObject, eventdata, handles)
% hObject    handle to pmSignalTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pmSignalTo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pmSignalTo


% --- Executes during object creation, after setting all properties.
function pmSignalTo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pmSignalTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function handles = getLookupTable()

hSig = get(gcf, 'UserData');
if (isempty(hSig))
    warning('No signal builder found');
else
    hPanels = get(hSig, 'Children');
    for i = 1:length(hPanels)
        if (strcmp(get(hPanels(i), 'Type'), 'uipanel'))
            if (strcmp(get(hPanels(i), 'Title'), ' Lookup Table '))
                hLookup = get(hPanels(i), 'Children');
            end
%             if (strcmp(get(hPanels(i), 'Title'), ' Options '))
%                 hOptions = get(hPanels(i), 'Children');
%             end
        end
    end
    
    if isempty(hLookup)
        warning('No Lookup Table found');
%     elseif isempty(hOptions)
%         warning('No Options found');
    else
        for i = 1:length(hLookup)
            if (strcmp(get(hLookup(i), 'TooltipString'), 'Breakpoints'))
                hBreakpoints = hLookup(i);
            end
            
            if (strcmp(get(hLookup(i), 'TooltipString'), 'Values'))
                hValues = hLookup(i);
            end
        end
        
        if isempty(hBreakpoints || hValues)
            warning('No Breakpoints or Values found');
        else
            handles = [hBreakpoints hValues];
        end
    end
end


% --- Executes on button press in btnSetSkip.
function btnSetSkip_Callback(hObject, eventdata, handles)
% hObject    handle to btnSetSkip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

chan = get(handles.pmSignalTo, 'Value');
set(handles.pmSignalTo, 'Value', mod(chan,16)+1);

% --- Executes on button press in btnSetInten.
function btnSetInten_Callback(hObject, eventdata, handles)
% hObject    handle to btnSetInten (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

hLookup = getLookupTable();
bp = get(hLookup(1), 'String');
val = get(hLookup(2), 'String');
table = get(handles.tblMapping, 'Data');
chan = get(handles.pmSignalTo, 'Value');
testval = str2num(val);

if (sum(testval < 0) > 0) || (sum(testval > 1) > 0)
    warning('MAPPING MENU - Intensity must be between 0 and 1.');
else
    table(chan, 1) = cellstr(bp);
    table(chan, 2) = cellstr(val);
    if get(handles.chSetOne, 'Value') == 1
        table(chan, 3) = cellstr('1');
    end
    set(handles.tblMapping, 'Data', table);
    set(handles.pmSignalTo, 'Value', mod(chan,16)+1);
end

% --- Executes on button press in btnSetFreq.
function btnSetFreq_Callback(hObject, eventdata, handles)
% hObject    handle to btnSetFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

hLookup = getLookupTable();
bp = get(hLookup(1), 'String');
val = get(hLookup(2), 'String');
table = get(handles.tblMapping, 'Data');
chan = get(handles.pmSignalTo, 'Value');
testval = str2num(val);

if (sum(testval < 0) > 0) || (sum(testval > 1) > 0)
    warning('MAPPING MENU - Frequency must be between 0 and 1.');
else
    table(chan, 1) = cellstr(bp);
    table(chan, 3) = cellstr(val);
    if get(handles.chSetOne, 'Value') == 1
        table(chan, 2) = cellstr('1');
    end
    set(handles.tblMapping, 'Data', table);
    set(handles.pmSignalTo, 'Value', mod(chan,16)+1);
end

% --- Executes on button press in btnSetBoth.
function btnSetBoth_Callback(hObject, eventdata, handles)
% hObject    handle to btnSetBoth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

hLookup = getLookupTable();
bp = get(hLookup(1), 'String');
val = get(hLookup(2), 'String');
table = get(handles.tblMapping, 'Data');
chan = get(handles.pmSignalTo, 'Value');
testval = str2num(val);

if (sum(testval < 0) > 0) || (sum(testval > 1) > 0)
    warning('MAPPING MENU - Intensity and frequency must be between 0 and 1.');
else
    table(chan, 1) = cellstr(bp);
    table(chan, 2) = cellstr(val);
    table(chan, 3) = cellstr(val);
    set(handles.tblMapping, 'Data', table);
    set(handles.pmSignalTo, 'Value', mod(chan,16)+1);
end


% --- Executes on button press in btnSetClear.
function btnSetClear_Callback(hObject, eventdata, handles)
% hObject    handle to btnSetClear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

table = get(handles.tblMapping, 'Data');
chan = get(handles.pmSignalTo, 'Value');

table(chan, 1) = cellstr('');
table(chan, 2) = cellstr('');
table(chan, 3) = cellstr('');
set(handles.tblMapping, 'Data', table);


% --- Executes on selection change in pmSignalLoad.
function pmSignalLoad_Callback(hObject, eventdata, handles)
% hObject    handle to pmSignalLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pmSignalLoad contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pmSignalLoad


% --- Executes during object creation, after setting all properties.
function pmSignalLoad_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pmSignalLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnLoadSignal.
function btnLoadSignal_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoadSignal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

table = get(handles.tblMapping, 'Data');
chan = get(handles.pmSignalLoad, 'Value');
  
hSig = get(gcf, 'UserData');

updateBp = getappdata(hSig, 'changebp');
updateVal = getappdata(hSig, 'changeval');

bp = str2num(cell2mat(table(chan, 1)));
if isempty(table{chan, 2});
    val = str2num(cell2mat(table(chan, 3)));
else
    val = str2num(cell2mat(table(chan, 2)));
end

if (isempty(hSig))
    warning('No signal builder found');
elseif (isempty(bp))
    warning('Cannot load empty signal (no breakpoints).');
elseif (isempty(val))
    warning('Cannot load empty signal (no values).');
else
    hPanels = get(hSig, 'Children');
    for i = 1:length(hPanels)
        if (strcmp(get(hPanels(i), 'Type'), 'uipanel'))
            if (strcmp(get(hPanels(i), 'Title'), ' Lookup Table '))
                hLookup = get(hPanels(i), 'Children');
            end
            if (strcmp(get(hPanels(i), 'Title'), ' Options '))
                hOptions = get(hPanels(i), 'Children');
            end
        end
%         if (strcmp(get(hPanels(i), 'Type'), 'axes'))
%             hAxes = get(hPanels(i), 'Children');
%         end
    end
    
    if isempty(hLookup)
        warning('No Lookup Table found');
    elseif isempty(hOptions)
        warning('No Options found');
    else
        for i = 1:length(hLookup)
            if (strcmp(get(hLookup(i), 'TooltipString'), 'Breakpoints'))
                set(hLookup(i), 'String', ['[' regexprep(num2str(bp), ' *', ' ') ']']);
                updateBp();
            end
            
            if (strcmp(get(hLookup(i), 'TooltipString'), 'Values'))
                set(hLookup(i), 'String', ['[' regexprep(num2str(val), ' *', ' ') ']']);
                updateVal();
            end
        end
        
%         for i = 1:length(hOptions)
%             
%             if (strcmp(get(hOptions(i), 'TooltipString'), 'Change the horizontal limits of the signal.'))
%                 xLimits = str2num(get(hOptions(i), 'String'));
%             end
%             
%             if (strcmp(get(hOptions(i), 'TooltipString'), 'Sampling period (s)'))
%                 sampTime = str2double(get(hOptions(i), 'String'));
%             end
%         end
    end
    
    tmpChild = get(gcf, 'Children');
    for i = 1:length(tmpChild)
        if (strcmp(get(tmpChild(i), 'Type'), 'axes'))
            set(tmpChild(i), 'Visible', 'off');
        end
    end
     
end


% --- Executes on button press in btnLoadMap.
function btnLoadMap_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoadMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
    [filename, path] = uigetfile('*.mat', 'Select mapping');
    loaddata = load(fullfile(path, filename));
    set(handles.tblMapping, 'Data', loaddata.mappData);
catch
    
end


% --- Executes on button press in btnConvertMap.
function btnConvertMap_Callback(hObject, eventdata, handles)
% hObject    handle to btnConvertMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
    [filename, path] = uigetfile('*.mat', 'Select old mapping');
    convertOldMappingFile(filename, path);
catch
    
end

% --- Executes on button press in chSetOne.
function chSetOne_Callback(hObject, eventdata, handles)
% hObject    handle to chSetOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chSetOne