function plotChMapp(tbl)

figure('units','normalized','outerposition',[0 0 1 1]);
legendflag = 0;
for i = 1:16
    if (~isempty(tbl{i, 1}))
        x = str2num(tbl{i, 1}); %#ok<*ST2NM>
        xi = linspace(x(1), x(end));

        y1 = str2num(tbl{i, 2});
        if isempty(y1)
            y1 = zeros(1, length(x));
        end
        y1 = interp1(x, y1, xi, 'linear');

        y2 = str2num(tbl{i, 3});
        if isempty(y2)
            y2 = zeros(1, length(x));
        end
        y2 = interp1(x, y2, xi, 'linear');

        subplot(4,4,i);
        xGridInc = 0.1;
        yGridInc = 0.1;
        xGrid = -1:xGridInc:1;
        yGrid = -1:yGridInc:1;
        [XGrid, YGrid] = meshgrid(xGrid, yGrid);
        hold on
        plot(xi, y1, xi, y2, 'r');
        if legendflag == 0
            legend('Intensity', 'Frequency', 'Location', 'best');
            legendflag = 1;
        end
        plot(XGrid, YGrid, ...
            'Color', [.6 .6 .6], ...
            'LineStyle', 'none', ...
            'Marker', '.', ...
            'MarkerSize', 1, ...
            'HitTest', 'off');
        hold off
        xlim([-1 1]);
        ylim([0 1]);
        if floor((i-1)/4) == 3
            xlabel('In');
        end
        
        if mod(i, 4) == 1
            ylabel('Out');
        end
        
        
            
        title(['Channel ' num2str(i)]);
    end
end

set(gcf, 'Color', [1 1 1]);