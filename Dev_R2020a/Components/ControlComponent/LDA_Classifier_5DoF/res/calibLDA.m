function calibLDA(timingUsed, usedFeatures, thrZC)

%% Load data and select classes
algoType = 'LDA';
[folderPathSave, trainingFileStr, rawDataMatrixTrain, ~, classesOfInterest, trainClasses, rawDataFromTo, ~, waitH] = loadData(algoType);
if ~(ischar(folderPathSave) && exist(folderPathSave,'dir'))
    return
end
windowLength = timingUsed(1);
interOffset = timingUsed(2);
nSamplesPerTrial = length(rawDataFromTo);
totNumOfClasses = length(classesOfInterest);

%% Calculate Feature matrices of the signals
featArray = {'RMS', 'ZC', 'WL', 'SSC'};
feats = featArray(usedFeatures);
sscThresh = thrZC;
[TDFMatrixTrain,~, nChannels, nFeatures] = getFeatureMatrix(rawDataMatrixTrain, feats, thrZC, sscThresh, windowLength, interOffset, nSamplesPerTrial);
wrongOrder  = repmat(1:nFeatures,[1 nChannels]);
[~, rightOrder] = sort(wrongOrder, 'ascend');
TDFMatrixTrain = TDFMatrixTrain(rightOrder,:);

%% Train LDA
[Wg, Cg] = trainLDA(TDFMatrixTrain, totNumOfClasses);
waitVal = 0.6;
waitbar(waitVal, waitH, [algoType ': Plotting ...']);
%% PCA Feature-space
featsName='';
for i = 1:length(feats)
    featsName=[featsName feats{i}];
end
motionLabels = cell (1, totNumOfClasses);
j = 1;
for i = classesOfInterest
    motionLabels{j} = ['Class ' num2str(i-1)];
    j = j+1;
end
optionsPlot.MotionLabels = motionLabels;
optionsPlot.Colors = mat2cell(lines(totNumOfClasses), ones(1, totNumOfClasses), 3);
optionsPlot.FigTitle = ['Feature space for: ' trainingFileStr];
displayFeatureSpace(TDFMatrixTrain, reshape(repmat(trainClasses(~isnan(trainClasses)),size(TDFMatrixTrain,2)/size(trainClasses(~isnan(trainClasses)),2),1),size(TDFMatrixTrain,2),[]), optionsPlot);
waitVal = waitVal + 0.2;
waitbar(waitVal, waitH, [algoType ': Finishing ...']);
%% SAVE THE DATA
set_param(gcb,'W_LDA_constant',mat2str(Wg));
set_param(gcb,'C_LDA_constant',mat2str(Cg));
set_param(gcb,'trainingFile',trainingFileStr);
set_param(gcb,'classesOfInterest',mat2str(classesOfInterest));
if ischar(folderPathSave) && exist(folderPathSave,'dir')
    set_param(gcb,'trainingFolder',folderPathSave);
end
% save ([folderPathSave 'calibData_LDA_',featsName,'.mat'], 'W_LDA','C_LDA','artefactRemovalChannel','overlapLDA','windowSizeLDA','goodCh','usedFeatures');
waitVal = waitVal + 0.2;
waitbar(waitVal, waitH, [algoType ': Done!']);
pause(0.5);
close(waitH)

end


