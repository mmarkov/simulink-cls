function turnOptionsOnOff(blockHandle, param_Name)

switch param_Name
    case 'whichTrig1'
        invName = 'whichDOFs1';
    case 'whichTrig2'
        invName = 'whichDOFs2';
    case 'whichTrig3'
        invName = 'whichDOFs3';
    case 'whichTrig4'
        invName = 'whichDOFs4';
    case 'whichTrig5'
        invName = 'whichDOFs5';
end

maskE = get_param(blockHandle,'MaskEnables');
maskN = get_param(blockHandle, 'MaskNames');
switch strcmp( get_param (blockHandle, param_Name), 'Not used' )
    case 1
        maskE{find(strcmp(maskN,invName))} = 'off';
    case 0
        maskE{find(strcmp(maskN,invName))} = 'on';
end
set_param(blockHandle,'MaskEnables',maskE);


end