function outStates = formatStates(blockHandle, param_Name)
outStates = zeros(1, 8) - 1;
valP = get_param(blockHandle, param_Name);
if ~isempty(strfind(valP, 'x')) 
    valP(strfind(valP, 'x')) = '0';
end
valP = str2num(valP);


outStates(1:length(valP)) = valP;

outStates = num2str(outStates);

end