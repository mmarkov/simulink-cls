function [DOFActivations,DOFTargets] = applyCSPRPosProcessGen(RawActs, Targets, DOFs, maxOnly, classesOfInterest)
%same for the targets
c = size(Targets,2); %to init after NoMove ends
if c>0
    DOFTargets = zeros(size(RawActs,1),5);
else
    DOFTargets=[];
end
DOFActivations=zeros(size(RawActs,1),5);
%make sure only one DOF is <>0 at a time
for iSample = 1:size(RawActs,1)
    if maxOnly
        mxIdx = abs(RawActs(iSample,:))==max(abs(RawActs(iSample,:)));
        RawActs(iSample,:) = RawActs(iSample,:) .* mxIdx;
    end
end
% Step 2: Arrange activations per DOF. 1:sup/pro,  3:KG/HO, 4:FP
for i=1:size(DOFs,1)
    switch sum(DOFs(i,:)~=0)
        case 2
            DOFActivations(:,i) = RawActs(:,DOFs(i,1)) - RawActs(:,DOFs(i,2));
        case 1
            if DOFs(i,1)
                DOFActivations(:,i) = RawActs(:,DOFs(i,1));
            else
                DOFActivations(:,i) = -RawActs(:,DOFs(i,2));
            end
    end
end

clear DOFTargets
c = size(Targets,2); %to init after NoMove ends
if c>0
    for classi = classesOfInterest 
        for i=1:size(DOFs,1)
            if classi == DOFs(i,1)
                DOFTargets(c:c+size(Targets,2)-1,i) =  Targets(DOFs(i,1),:);
                c = c+size(Targets,2);
            end
            if classi == DOFs(i,2)
                DOFTargets(c:c+size(Targets,2)-1,i) =  -Targets(DOFs(i,2),:); 
                c = c+size(Targets,2);
            end
        end  
    end
    DOFTargets=[DOFTargets;zeros(1,size(DOFTargets,2))];
else
    DOFTargets=[];
end
