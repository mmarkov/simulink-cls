function W = cspTrainLinearRegression(DOFActivationsTrain, DOFTargetsTrain, selectedDOFs )
%TRAINLINEARREGRESSION

Wsmall = (DOFActivationsTrain(:,selectedDOFs)'*DOFActivationsTrain(:,selectedDOFs))\(DOFActivationsTrain(:,selectedDOFs)'*DOFTargetsTrain(:,selectedDOFs));

W=zeros(5,5);
dofActACtr=0;
for dofA=1:5
    if selectedDOFs(dofA)
        dofActACtr=dofActACtr+1;
    end
    dofActBCtr=0;
    for dofB=1:5
        if selectedDOFs(dofB)
            dofActBCtr=dofActBCtr+1;
        end
        if selectedDOFs(dofB) && selectedDOFs(dofA)
            W(dofA,dofB)=Wsmall(dofActACtr,dofActBCtr);
        end
    end
end

end

