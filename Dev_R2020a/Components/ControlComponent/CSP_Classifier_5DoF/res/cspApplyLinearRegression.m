function estimates = applyLinearRegression(LR_W, DOFActivationsTest )
%APPLYLINEARREGRESSION 

estimates = LR_W'*DOFActivationsTest';

end

