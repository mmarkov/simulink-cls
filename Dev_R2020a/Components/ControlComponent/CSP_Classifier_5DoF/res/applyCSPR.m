function varargout = applyCSPR(W, inputFeatures, classPairs)
% applyCSPR(W, inputFeatures, classPairs, maxOnly) apply a previously
% trained CSP regression to feature vectors for estimation
%
% Inputs:
% - W and classPairs are the outputs from trainCSPR
% - inputFeatures: a [features x time] matrix of features to be estimated
% - maxOnly: set to "true" to get only the output of the winning motion
% (proportional classifier - one class at a time active).
% set to false to get proportional output for every class
% (co-activations/simultaneous control signals possible)
%
% Outputs:
% - activationsPerClass: proportional activation signal per class
% - winningClasses: index of the winning class for each feature vector =
% classification result. Unaffected by maxOnly

%1) Apply the CSP weight matrix that compares are classes with each other
%to the test samples. The corresponding class for each column in W is coded
%in classPairs. W(:,1), W(:,3),... W(:,end-1) are the classes that should
%"win" the CSP competitions against (W(:,2), W(:,4),... W(:,end)

% 2) for each sample, for each class get the lowest activation. The
% reasoning is the following: if, e.g. "flexion" is currently performed,
% then all classes schould lose at least once their CSP competetion -
% against flexion - so their lowest activation will be really small. Only
% for flexion, which should win all of its competitions, the lowest
% activation will actually still be quite large. => activationsPerClass

% 3) We can also apply a second assumption: if the index of our current
% class is 1,3,...end-1, then it should win against its neighbouring class.
% we can calculate the margin by how much it won as the ratio of
% output(thisClass) / output(nextClass).
% However, if its index is 2,4,...end, then it should have lost. We can
% calculte the margin by how much it lost as output(thisClass) / output(previousClass)
% this gives us a "likelihood" ratio of the probability that this class
% should actually win/lose. => likelihoodsPerClass

% Multiplying the likelihood with the activation gives the best results for
% the estimations

nClasses = max(classPairs);
activationsRaw = W'*inputFeatures;

for iSample = 1:length(activationsRaw)
    if iSample == 12
        iSample = iSample;
    end
    currActRaw = activationsRaw(:,iSample);
    for iClass = 1:nClasses
        actsThisClass = currActRaw(classPairs == iClass);
        [~, idx] = sort (abs(actsThisClass));
        activationsPerClass(iSample, iClass) = actsThisClass(idx(1)); %get the first element = lowest absolute activation
        
        indicesOfThisClass = find(classPairs == iClass);
        for iIdx = 1:length(indicesOfThisClass)
            if mod(indicesOfThisClass(iIdx),2) == 1 %different to c# because indices are 1...n not 0...n
                likelihoodsPerClass(iSample, iIdx) = abs(currActRaw(indicesOfThisClass(iIdx)) /currActRaw(indicesOfThisClass(iIdx)+1));
            else
                likelihoodsPerClass(iSample, iIdx) = abs(currActRaw(indicesOfThisClass(iIdx)) /currActRaw(indicesOfThisClass(iIdx)-1));
            end
        end
        classLikelihoods(iSample, iClass) =  min(likelihoodsPerClass(iSample, :));
    end
    actsThisSample = abs(activationsPerClass(iSample,:));
    
    likesThisSample(iSample,:) = classLikelihoods(iSample,:)/sum(classLikelihoods(iSample,:));
    for i = 1: length(likesThisSample(iSample,:))
        likesThisSample(iSample,i) = 1.0 / (1 + exp(-20 * (likesThisSample(iSample,i) - 0.5))); %sigmoidal transformation => make small even smaller, large even larger. 0.5 stays 0.5
    end
    
    
    activationsPerClass(iSample,:) = actsThisSample.*likesThisSample(iSample,:);
    % ---- Specific to 7+1 classes => now has to be done outside this
    % function by its caller!
    %     DOFactivations(iSample,1) = activationsPerClass(iSample,2) - activationsPerClass(iSample,3);
    %     DOFactivations(iSample,2) = activationsPerClass(iSample,4) - activationsPerClass(iSample,5);
    %     DOFactivations(iSample,3) = activationsPerClass(iSample,7) - activationsPerClass(iSample,8);
    %     DOFactivations(iSample,4) = activationsPerClass(iSample,6);
    %     if maxOnly
    %         mxIdx = abs(DOFactivations(iSample,:))==max(abs(DOFactivations(iSample,:)));
    %         DOFactivations(iSample,:) = DOFactivations(iSample,:) .* mxIdx;
    %     end
    winningClasses(iSample) = find (activationsPerClass(iSample,:)==max(activationsPerClass(iSample,:)));
end

varargout{1} = activationsPerClass;
if nargout == 2
    varargout{2} = winningClasses;
end
if nargout == 3
    varargout{3} = likesThisSample;
end

end
