function checkDoFSelectionValidity(blockHandle, param_Name)
selOK = true;
valP = get_param(blockHandle, param_Name);

val1 = get_param(blockHandle, 'DOF1_idx');
val2 = get_param(blockHandle, 'DOF2_idx');
val3 = get_param(blockHandle, 'DOF3_idx');
val4 = get_param(blockHandle, 'DOF4_idx');

switch param_Name
    case 'DOF1_idx'
        selOK = ~ismember({valP}, {val2, val3, val4}) || strcmp(valP,'None');
        invName = 'DOF1_inv';
    case 'DOF2_idx'
        selOK = ~ismember({valP}, {val1, val3, val4}) || strcmp(valP,'None');
        invName = 'DOF2_inv';
    case 'DOF3_idx'
        selOK = ~ismember({valP}, {val1, val2, val4}) || strcmp(valP,'None');
        invName = 'DOF3_inv';
    case 'DOF4_idx'
        selOK = ~ismember({valP}, {val1, val2, val3}) || strcmp(valP,'None');
        invName = 'DOF4_inv';
end

if ~selOK
    set_param(blockHandle, param_Name, 'None');
end


maskE = get_param(blockHandle,'MaskEnables');
maskN = get_param(blockHandle, 'MaskNames');
switch strcmp( get_param (blockHandle, param_Name), 'None' )
    case 1
        maskE{find(strcmp(maskN,invName))} = 'off';
    case 0
        maskE{find(strcmp(maskN,invName))} = 'on';
end
set_param(blockHandle,'MaskEnables',maskE);
    

end