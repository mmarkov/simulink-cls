function CLS_Remove()
%% Find the root level dir
currentDir = mfilename('fullpath');
rootDirIdx = strfind(currentDir,'Install');
rootDir = currentDir(1:rootDirIdx(end)-2);
%% Remove the install path from the search directory
if exist([rootDir, '/Install/', 'installPath.mat'],'file') == 2
    % Remove the install path
    disp('REMOVING CLS: Removing the obsolete paths ...')
    load ([rootDir, '/Install/', 'installPath.mat'],'installPath');
    rmpath(installPath);
    currentPath = path();
    % Remove the obsolete paths
    if ispc
        pathStrings = strsplit(currentPath,';');
    else
        pathStrings = strsplit(currentPath,':');
    end
    doesPathExist = cellfun(@(x) exist(x,'dir'), pathStrings);
    pathNotOK = find(doesPathExist == 0);
    for i = pathNotOK
        rmpath(pathStrings{i});
    end
    currentPath = path();
    path(currentPath);
    disp('REMOVING CLS: Removing the search path ...')
    if savepath
        error('You need administrator privilegies in order to perform this operation!');
    else
        delete([rootDir, '/Install/', 'installPath.mat']);
        disp('REMOVING CLS: Rehashing toolboxes ...')
        % Refresh toolbox cache
        rehash toolboxcache
        lb = LibraryBrowser.LibraryBrowser2;
        lb.refresh
        disp('REMOVING CLS: Finished!')
    end
else
    disp('REMOVING CLS: There are no previosly installed versions!')
end
end