function num = dot2num(dot) 
 d = strsplit(dot,'.');
 num =  ((((((+str2double(d{1}))*256)+(+str2double(d{2})))*256)+(+str2double(d{3})))*256)+(+str2double(d{4}));
end