fullscreen = get_param(gcb, 'fullscreen');
chMode = get_param(gcb, 'channelNr');
virtStim16_H = get_param(gcb, 'UserData');
if strcmp(get_param(gcb,'winA'),'on')
    if  isempty(virtStim16_H) || ~isvalid(virtStim16_H)
        sysPath = [gcb '/VR Sink'];
        open_system(sysPath,'OpenFcn');
        virtStim16_H = vrgcf;
        Pix_SS = get(0,'screensize');
        switch chMode
            case '4 (1x4)'
                posNormalized = [0.2 0.1 0.6 0.4];
            case '4 (2x2)'
                posNormalized = [0.3 0.1 0.4 0.5];
            case '4 (diamond, clockwise)'
                posNormalized = [0.25 0.1 0.5 0.6];
            case '4 (diamond, counterclockwise)'
                posNormalized = [0.25 0.1 0.5 0.6];
            case '8 (1x8)'
                posNormalized = [0.1 0.1 0.8 0.38];
            case '8 (2x4, row enumeration)'
                posNormalized = [0.2 0.1 0.6 0.45];
            case '8 (2x4, column enumeration)'
                posNormalized = [0.2 0.1 0.6 0.45];
            case '16 (1x16)'
                posNormalized = [0.005 0.1 0.99 0.5];
            case '16 (2x8, row enumeration)'
                posNormalized = [0.05 0.1 0.9 0.4];
            case '16 (2x8, column enumeration)'
                posNormalized = [0.05 0.1 0.9 0.4];
            otherwise
                posNormalized = [0.25 0.1 0.5 0.7];
        end
        
        posNormalized([1 3]) = posNormalized([1 3])*Pix_SS(3);
        posNormalized([2 4]) = posNormalized([2 4])*Pix_SS(4);
        set(virtStim16_H, 'Position', posNormalized);
        set(virtStim16_H, 'Fullscreen', fullscreen); 
        set(virtStim16_H, 'NavPanel', 'minimized', 'Tooltips', 'off', 'ToolBar', 'off', 'StatusBar', 'off', 'Antialiasing', 'off');
    end
    
end
set_param(gcb, 'UserData', virtStim16_H);
clear sysPath Pix_SS posNormalized tmp virtStim16_H chMode fullscreen;