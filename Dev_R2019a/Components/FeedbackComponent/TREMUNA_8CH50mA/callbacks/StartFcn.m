COMP = get_param(gcb,'comP');      
TREMUNA = Stimulator( COMP );
TREMUNA.Connect;

% Setup stimulation parameters
defstimparams = zeros(8,5);
defstimparams(:,1) = TREMUNA.MIN_FREQ;
defstimparams(:,2) = TREMUNA.MIN_AMP;
defstimparams(:,3) = TREMUNA.MIN_PW;
for i = 1:Stimulator.NUM_CHANNELS
    TREMUNA.SetChannelParamsFromVector( i,  defstimparams( i, : ) ); 
end

% Turn on
TREMUNA.TurnON;
states = '00000000';
channels = 1:8;
for i = 1:length( channels )
    states( channels( i ) ) = '1';
end
TREMUNA.SetStates( states( end:-1:1 ) );
% Free COM port
TREMUNA.Disconnect;
clear COMP              