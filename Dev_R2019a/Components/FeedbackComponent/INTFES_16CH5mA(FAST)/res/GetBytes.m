function [ high, low ] = GetBytes( w )
low = bitand( uint16( w ), hex2dec( 'FF' ) );
high = bitshift( bitand( uint16( w ), hex2dec( 'FF00' ) ), -8 );
end