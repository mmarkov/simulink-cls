fittsLaw_H = get_param(gcb, 'UserData');
if ~isempty(fittsLaw_H) && isvalid(fittsLaw_H)
	set(fittsLaw_H, 'Fullscreen', 'off');       
end     
clear fittsLaw_H