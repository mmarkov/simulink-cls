sysPath = [gcb '/VR Sink'];
fullscreen = get_param(gcb, 'fullscreen');                                                                            
autoopen = get_param(gcb, 'autoopen');       
monoBar_H = get_param(gcb, 'UserData');
if strcmp(autoopen,'on')                                   
	if  isempty(monoBar_H) || ~isvalid(monoBar_H)                
		open_system(sysPath,'OpenFcn');                
		monoBar_H =  vrgcf;                                      
	end;          
	set(monoBar_H, 'Fullscreen', fullscreen);                                                                           
	set(monoBar_H, 'NavPanel', 'minimized', 'Tooltips', 'off', 'ToolBar', 'off', 'StatusBar', 'off', 'Antialiasing', 'off');
end                                                                                                                     
set_param(gcb, 'UserData', monoBar_H);
clear sysPath  fullscreen  autoopen  monoBar_H;