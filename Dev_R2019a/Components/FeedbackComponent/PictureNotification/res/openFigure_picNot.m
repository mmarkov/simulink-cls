function openFigure_picNot( blockH )

FigHandle=get_param(blockH,'UserData');
if isempty(FigHandle) || ~ishandle(FigHandle)
    %
    % the figure doesn't exist, create one
    %
    dispBlockData = get_param([blockH '/dispPic'],'UserData');
    if ~isempty(dispBlockData)
        picData = dispBlockData.picData;
        % Recreate if closed, at the begining
        FigHandle = figure('Color', [1 1 1], 'Units', 'normalized', 'Position', ...
            [0.775 0.55 0.2 0.35], 'Toolbar', 'none', 'MenuBar', 'none', ...
            'Name', 'Picture Message', 'NumberTitle', 'off', ...
            'DeleteFcn', @LocalFigureDeleteFcn);
        
        % Always recreate the axes at the begining
        aH = axes('Parent', FigHandle);
        set(aH, 'Units', 'normalized', 'Position', [0 0 1 1], ...
            'DataAspectRatioMode', 'manual', 'MinorGridLineStyle', 'none', ...
            'XTick', [], 'Ytick', [], 'ZTick', []);
        
        if numel(picData) == 0
            %     tH = text(0.5,0.5, 'No pictures loaded!', 'HorizontalAlignment', 'center', ...
            %         'Parent', aH, 'FontSize', 14, 'Units', 'normalized');
            iH = -1;
        else
            iH = image(ones(size(picData{1})), 'Parent', aH);
            %     tH = text(0.5,0.05, '' , 'HorizontalAlignment', 'center', ...
            %             'Parent', aH, 'FontSize', 14, 'Units', 'normalized');
            axis(aH,'off');
            axis(aH,'image');
        end
        
        
        %
        % Associate the figure with the blockH, and set the figure's UserData.
        %
        ud.XYAxes   = aH;
        ud.ImgH = iH;
        % ud.TextH = tH;
        ud.Block = blockH;
        set_param(blockH,'UserData',FigHandle);
        set(FigHandle,'HandleVisibility','callback','UserData',ud);
    end
end

end

function LocalFigureDeleteFcn(src, ~)

%
% Get the block associated with this figure and set it's figure to -1
%
ud=get(src,'UserData');
set_param(ud.Block,'UserData',-1);

end