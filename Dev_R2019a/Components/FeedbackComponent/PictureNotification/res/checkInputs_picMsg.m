function checkInputs_picMsg(paramNames)
blockHandle = gcb;
sysH = bdroot;
if strcmp(get_param(sysH, 'LibraryType'), 'None') && ...
        strcmp(get_param(sysH, 'Type'), 'block_diagram')
    imgList = {};
    fullFilePath = get_param(sysH, 'filename');
    idx = strfind(fullFilePath, '\');
    if ~isempty(idx)
        picFolder = get_param(blockHandle, 'picFolder');
        relPath = [fullFilePath(1:idx(end)) picFolder];
        if exist(relPath, 'dir')
            listFjpg = dir([relPath , '/*.jpg']);
            listFpng = dir([relPath , '/*.png']);
            if (numel(listFjpg) > 0) || (numel(listFpng) > 0)
                try
                    for i = 1:numel(listFjpg)
                        imgList = [imgList  {listFjpg(i).name}];
                    end
                    
                    for i = 1:numel(listFpng)
                        imgList = [imgList  {listFpng(i).name}];
                    end
                catch
                    warndlg('Folder unchanged due to the file-read error', 'Load error: Invalid File!');
                end
            end
            for i= 1:length(paramNames)
                if ~isempty(get_param(blockHandle,paramNames{i})) && ~any(strcmp(imgList, get_param(blockHandle,paramNames{i})))
                    warndlg(['The picture ' get_param(blockHandle,paramNames{i}) ' does not exist'],'PictureNotification::MsgID', 'replace');
                end
            end
        end
    end
end
end