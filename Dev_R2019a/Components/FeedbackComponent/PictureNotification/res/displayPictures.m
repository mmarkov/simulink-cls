function displayPictures(block)
%MSFUNTMPL_BASIC A Template for a Level-2 MATLAB S-Function
%   The MATLAB S-function is written as a MATLAB function with the
%   same name as the S-function. Replace 'msfuntmpl_basic' with the
%   name of your S-function.
%
%   It should be noted that the MATLAB S-function is very similar
%   to Level-2 C-Mex S-functions. You should be able to get more
%   information for each of the block methods by referring to the
%   documentation for C-Mex S-functions.
%
%   Copyright 2003-2010 The MathWorks, Inc.

%%
%% The setup method is used to set up the basic attributes of the
%% S-function such as ports, parameters, etc. Do not add any other
%% calls to the main body of the function.
%%
setup(block);

%endfunction

%% Function: setup ===================================================
%% Abstract:
%%   Set up the basic characteristics of the S-function block such as:
%%   - Input ports
%%   - Output ports
%%   - Dialog parameters
%%   - Options
%%
%%   Required         : Yes
%%   C-Mex counterpart: mdlInitializeSizes
%%
function setup(block)

% Register number of ports
block.NumInputPorts  = 2;
block.NumOutputPorts = 0;

% Setup port properties to be inherited or dynamic
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

% Override input port properties
block.InputPort(1).Dimensions        = 1;
block.InputPort(1).DatatypeID  = 8;  % double
block.InputPort(1).Complexity  = 'Real';
block.InputPort(1).DirectFeedthrough = true;

% % Override output port properties
% block.OutputPort(1).Dimensions       = 0;
% block.OutputPort(1).DatatypeID  = 0; % double
% block.OutputPort(1).Complexity  = 'Real';

% Register parameters
block.NumDialogPrms     = 11;

% Register sample times
%  [0 offset]            : Continuous sample time
%  [positive_num offset] : Discrete sample time
%
%  [-1, 0]               : Inherited sample time
%  [-2, 0]               : Variable sample time
block.SampleTimes = [-1 0];

% Specify the block simStateCompliance. The allowed values are:
%    'UnknownSimState', < The default setting; warn and assume DefaultSimState
%    'DefaultSimState', < Same sim state as a built-in block
%    'HasNoSimState',   < No sim state
%    'CustomSimState',  < Has GetSimState and SetSimState methods
%    'DisallowSimState' < Error out when saving or restoring the model sim state
block.SimStateCompliance = 'DefaultSimState';

%% -----------------------------------------------------------------
%% The MATLAB S-function uses an internal registry for all
%% block methods. You should register all relevant methods
%% (optional and required) as illustrated below. You may choose
%% any suitable name for the methods and implement these methods
%% as local functions within the same file. See comments
%% provided for each function for more information.
%% -----------------------------------------------------------------

block.RegBlockMethod('PostPropagationSetup',    @DoPostPropSetup);
block.RegBlockMethod('InitializeConditions', @InitializeConditions);
block.RegBlockMethod('Start', @Start);
block.RegBlockMethod('Outputs', @Outputs);     % Required
block.RegBlockMethod('Update', @Update);
block.RegBlockMethod('Derivatives', @Derivatives);
block.RegBlockMethod('Terminate', @Terminate); % Required
block.SetSimViewingDevice(true);

%end setup

%%
%% PostPropagationSetup:
%%   Functionality    : Setup work areas and state variables. Can
%%                      also register run-time methods here
%%   Required         : No
%%   C-Mex counterpart: mdlSetWorkWidths
%%
function DoPostPropSetup(block)
block.NumDworks = 1;

block.Dwork(1).Name            = 'x1';
block.Dwork(1).Dimensions      = 1;
block.Dwork(1).DatatypeID      = 0;      % double
block.Dwork(1).Complexity      = 'Real'; % real
block.Dwork(1).UsedAsDiscState = true;


%%
%% InitializeConditions:
%%   Functionality    : Called at the start of simulation and if it is
%%                      present in an enabled subsystem configured to reset
%%                      states, it will be called when the enabled subsystem
%%                      restarts execution to reset the states.
%%   Required         : No
%%   C-MEX counterpart: mdlInitializeConditions
%%
function InitializeConditions(block)
%

%end InitializeConditions


%%
%% Start:
%%   Functionality    : Called once at start of model execution. If you
%%                      have states that should be initialized once, this
%%                      is the place to do it.
%%   Required         : No
%%   C-MEX counterpart: mdlStart
%%
function Start(block)

fullPath = block.DialogPrm(1).Data;
listFjpg = dir([fullPath , '/*.jpg']);
listFpng = dir([fullPath , '/*.png']);

picData = cell(1, numel(listFjpg) + numel(listFpng));
picNames = picData;
if numel(picData) > 0
    for i = 1:numel(listFjpg)
        picData{i} = imread([fullPath, '\' listFjpg(i).name]);
        picNames{i} =  listFjpg(i).name;
    end
    
    for j = 1:numel(listFpng)
        picData{j + numel(listFjpg)} = imread([fullPath, '\' listFpng(j).name]);
        picNames{j+ numel(listFjpg)} =  listFpng(j).name;
    end
end

blockData.picData = picData;
blockData.picNames = picNames;
set(block.BlockHandle,'UserData', blockData);

%
block.Dwork(1).Data = 0;

% get the figure associated with this block, create a figure if it doesn't
% exist
%
FigHandle = GetSfunXYFigure(block.BlockHandle);
if ~ishandle(FigHandle)
    FigHandle = CreateSfunXYFigure(block.BlockHandle);
else
    blockData = get(block.BlockHandle,'UserData');
    picData = blockData.picData;
    ud = get(FigHandle,'UserData');
    if ud.ImgH == -1
        aH = axes('Parent', FigHandle);
        set(aH, 'Units', 'normalized', 'Position', [0 0 1 1], ...
            'DataAspectRatioMode', 'manual', 'MinorGridLineStyle', 'none', ...
            'XTick', [], 'Ytick', [], 'ZTick', []);
        iH = image(ones(size(picData{1})), 'Parent', aH);
        ud.ImgH = iH;
        axis(aH,'off');
        axis(aH,'image');
        set(FigHandle,'UserData',ud);
    end
    set(ud.ImgH,'CData', ones(size(picData{1})));
end
%end Start

%%
%% Outputs:
%%   Functionality    : Called to generate block outputs in
%%                      simulation step
%%   Required         : Yes
%%   C-MEX counterpart: mdlOutputs
%%
function Outputs(block)

blockData = get(block.BlockHandle,'UserData');
picData = blockData.picData;
picNames = blockData.picNames;

FigHandle=GetSfunXYFigure(block.BlockHandle);
if ~ishandle(FigHandle),
    return
end

%
% Get UserData of the figure.
%
ud = FigHandle.UserData;

% Plot

if (numel(picData) > 0) && ((block.InputPort(1).Data) == 1) ...
        && (block.InputPort(2).Data >= 0)
    idx = find(strcmp(picNames, block.DialogPrm(2 + block.InputPort(2).Data).Data));
    if ~isempty(idx)
        set(ud.ImgH,'CData', picData{idx});
%         set(ud.TextH, 'String', picNames{idx})
    else
        set(ud.ImgH,'CData', ones(size(picData{1})));
%         set(ud.TextH, 'String', ['Message ID' num2str(block.InputPort(2).Data) ...
%             ' missing picture!']);
    end
end

%end Outputs

%%
%% Update:
%%   Functionality    : Called to update discrete states
%%                      during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlUpdate
%%
function Update(block)

% block.Dwork(1).Data = block.InputPort(1).Data;

%end Update

%%
%% Derivatives:
%%   Functionality    : Called to update derivatives of
%%                      continuous states during simulation step
%%   Required         : No
%%   C-MEX counterpart: mdlDerivatives
%%
function Derivatives(block)

%end Derivatives

%%
%% Terminate:
%%   Functionality    : Called at the end of simulation for cleanup
%%   Required         : Yes
%%   C-MEX counterpart: mdlTerminate
%%
function Terminate(block)


%end Terminate



%
%=============================================================================
% GetSfunXYFigure
% Retrieves the figure window associated with this S-function XY Graph block
% from the block's parent subsystem's UserData.
%=============================================================================
%
function FigHandle = GetSfunXYFigure(block)
if strcmp(get_param(block,'BlockType'),'M-S-Function')
    block=get_param(block,'Parent');
end

FigHandle=get_param(block,'UserData');
if isempty(FigHandle),
    FigHandle=-1;
end

% end GetSfunXYFigure


%=============================================================================
% SetSfunXYFigure
% Stores the figure window associated with this S-function XY Graph block
% in the block's parent subsystem's UserData.
%=============================================================================
%
function SetSfunXYFigure(block,FigHandle)

if strcmp(get_param(bdroot,'BlockDiagramType'),'model'),
    if strcmp(get_param(block,'BlockType'),'M-S-Function'),
        block=get_param(block,'Parent');
    end
    
    set_param(block,'UserData',FigHandle);
end

% end SetSfunXYFigure



%=============================================================================
% CreateSfunXYFigure
% Creates the figure window associated with this S-function XY Graph block.
%=============================================================================
%
function FigHandle = CreateSfunXYFigure(block)
blockData = get(block,'UserData');
%
% the figure doesn't exist, create one
%
picData = blockData.picData;
% Recreate if closed, at the begining
FigHandle = figure('Color', [1 1 1], 'Units', 'normalized', 'Position', ...
    [0.775 0.55 0.2 0.35], 'Toolbar', 'none', 'MenuBar', 'none', ...
    'Name', 'Picture Message', 'NumberTitle', 'off', ...
    'DeleteFcn', @LocalFigureDeleteFcn);

% Always recreate the axes at the begining
aH = axes('Parent', FigHandle);
set(aH, 'Units', 'normalized', 'Position', [0 0 1 1], ...
    'DataAspectRatioMode', 'manual', 'MinorGridLineStyle', 'none', ...
    'XTick', [], 'Ytick', [], 'ZTick', []);

if numel(picData) == 0
%     tH = text(0.5,0.5, 'No pictures loaded!', 'HorizontalAlignment', 'center', ...
%         'Parent', aH, 'FontSize', 14, 'Units', 'normalized');
    iH = -1;
else
    iH = image(ones(size(picData{1})), 'Parent', aH);
%     tH = text(0.5,0.05, '' , 'HorizontalAlignment', 'center', ...
%             'Parent', aH, 'FontSize', 14, 'Units', 'normalized');
    axis(aH,'off');
    axis(aH,'image');
end


%
% Associate the figure with the block, and set the figure's UserData.
%
ud.XYAxes   = aH;
ud.ImgH = iH;
% ud.TextH = tH;
ud.Block = block;
SetSfunXYFigure(block,FigHandle);
set(FigHandle,'HandleVisibility','callback','UserData',ud);
% end CreateSfunXYFigure


%
%=============================================================================
% LocalFigureDeleteFcn
% This is the XY Graph figure window's DeleteFcn.  The figure window is
% being deleted, update the XY Graph block's UserData to reflect the change.
%=============================================================================
%
function LocalFigureDeleteFcn(src, ~)

%
% Get the block associated with this figure and set it's figure to -1
%
ud=get(src,'UserData');
SetSfunXYFigure(ud.Block,-1)

% end LocalFigureDeleteFcn

