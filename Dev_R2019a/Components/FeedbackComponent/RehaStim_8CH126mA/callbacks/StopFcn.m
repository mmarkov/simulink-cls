COMP = get_param(gcb,'comP');      
REHASTIM = serial(COMP,'Baudrate',115200,'Parity','no', 'Stopbit',2,...
    'FlowControl', 'hardware');

try
    fopen(REHASTIM);
    cmdBits = '11000000';
    b1 = bin2dec(cmdBits(1:8));
    fwrite(REHASTIM,b1, 'uint8');
end

fclose(REHASTIM);
delete(REHASTIM);
clear COMP b1 REHASTIM;
