function checkInputs_soundMsg(paramNames)
blockHandle = gcb;
sysH = bdroot;
if strcmp(get_param(sysH, 'LibraryType'), 'None') && ...
        strcmp(get_param(sysH, 'Type'), 'block_diagram')
    matList = {};
    fullFilePath = get_param(sysH, 'filename');
    idx = strfind(fullFilePath, '\');
    if ~isempty(idx)
        soundFolder = get_param(blockHandle, 'soundFolder');
        relPath = [fullFilePath(1:idx(end)) soundFolder];
        if exist(relPath, 'dir')
            listFmat = dir([relPath , '/*.mat']);
            if (numel(listFmat) > 0)
                try
                    for i = 1:numel(listFmat)
                        matList = [matList  {listFmat(i).name}];
                    end
                catch
                    warndlg('Folder unchanged due to the file-read error', 'Load error: Invalid File!');
                end
            end
            for i= 1:length(paramNames)
                if ~isempty(get_param(blockHandle,paramNames{i})) && ~any(strcmp(matList, get_param(blockHandle,paramNames{i})))
                    warndlg(['The file ' get_param(blockHandle,paramNames{i}) ' does not exist'],'SoundNotification::MsgID', 'replace');
                end
            end
        end
    end
end
end