target3D_H = get_param(gcb, 'UserData');
if ~isempty(target3D_H) && isvalid(target3D_H)
	set(target3D_H, 'Fullscreen', 'off');       
end     
warning('on', 'Simulink:Engine:BlockOutputInfNanDetectedError');
clear target3D_H