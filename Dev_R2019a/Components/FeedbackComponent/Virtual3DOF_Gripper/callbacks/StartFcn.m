fullscreen = get_param( gcb, 'fullscreen' );                                                                                  
autoopen = get_param( gcb, 'autoopen' );      
virtualHand_H = get_param(gcb, 'UserData');
if strcmp(autoopen,'on')                                                                                                      
if  isempty(virtualHand_H) || ~isvalid(virtualHand_H)                                                                         
  sysPath = [gcb '/VR Sink'];                                                                                                 
  open_system(sysPath,'OpenFcn');                                                                                             
  virtualHand_H = vrgcf;                                                                                                      
end                                                                                                                           
  set( virtualHand_H, 'Fullscreen', fullscreen );                                                                             
  set(virtualHand_H, 'NavPanel', 'minimized', 'Tooltips', 'off', 'ToolBar', 'off', 'StatusBar', 'off', 'Antialiasing', 'off');
end                                  
set_param(gcb, 'UserData', virtualHand_H);
clear sysPath  fullscreen  autoopen  virtualHand_H                                                                                     