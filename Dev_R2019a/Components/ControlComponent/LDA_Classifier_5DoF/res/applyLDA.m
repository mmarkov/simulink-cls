function varargout = applyLDA (Wg, Cg, featureVectors, varargin)
%applyLDA applies a previously trained LDA (linear discriminant analysis)
%to a set of feature vectors for classification
%Sebastian Ams�ss Oct. 2012
%--------------------------------------------------------------------------
%input: 
% Wg              = weight matrix that transforms a new vector to the 
%                   LDA space, #classes x #features
%
% Cg              = offset compensation matrix, #classes x 1
%
% featureVectors  = set of feature vectors to be classified,
%                   #features x nVecs (nVecs = 1 - ...)
%
% optional        = vector containing the true classes of the featureVectors
%                   if provided, an output about the classification success
%                   is displayed. #nVecs x 1
%
% classification of each feature vector y in featureVectors:
% l_c = Wg*y + Cg
% l_c (likelihood per class) is a #classes x 1 vector. classify y to the class of the row that has
% the highest entry in l_c, see output description
%
%--------------------------------------------------------------------------
%output
%
% [classes]       = applyLDA (Wg, Cg, featureVectors)
%                   returns a 1 x nVecs vector where each entry is the 
%                   calculated class of the corresponding feature vector in featureVectors
%
% [classes, l_c]  = applyLDA (Wg, Cg, featureVectors)
%                   additionally delivers the likelihood vectors per class, 
%                   normalized to 1
%                   eg. l_c(:,3) = [0.8, 0.1, 0.05, 0.05] means that the third 
%                   feature vector had a likelihood of 80% being class1, 
%                   10% being class2 and each 5% for class 3 and 
%                   classes(3) will be 1 in that case.
%
%see also: trainLDA

%check arguments
nClasses = length(Cg);
[nFeatures, nObservations] = size(featureVectors);
if nFeatures ~= size(Wg,2) || size(Wg,1) ~= nClasses
    error('Miss-match in size of input arguments');
end

%perform classification
l_c = zeros(nClasses, nObservations);
calcClasses = zeros(1,nObservations);
for featVec = 1:nObservations
    l_c(:,featVec) = Wg*featureVectors(:,featVec) + Cg;
   % l_c(:,featVec) = l_c(:,featVec)/norm(l_c(:,featVec), 'fro'); %normalize sum to 1
    [~,calcClasses(1,featVec)] = max(l_c(:,featVec)); %get index of max likelihood
end
%Adopted from 
%Scheme E, Hudgins B, Englehart K: Confidence-Based Rejection for Improved
%Pattern Recognition Myoelectric Control, IEEE Transactions on Biomedical
%Engineering (60/6), 2013, pp 1563-70. See page 2

%bring l_c back to linear domain
l_c = exp(l_c); %carefull - values go up to 1e+260! realmax('double') = 1.7977e+308
                %if NaN or Inf occurs try l_c = l_c-repmat(min(l_c),8,1);
                %this should leave the ratio unchanged: e^x/e^y = e^(x-y)
%and normalize to sum up to 1
l_c = l_c./repmat(sum(l_c), nClasses,1);

%display result if user provided true classes
if nargin == 4
    trueClasses = varargin{1};
    verbose = true;
else
    verbose = false;
end

if verbose
    fprintf('\n---------------LDA Classification-----------------\n');
    nMissclass  = numel(trueClasses(trueClasses ~=calcClasses));
    nTotalClass = length(calcClasses);
    overallSuccessRate = 100 - nMissclass/nTotalClass*100;
    fprintf('SuccessRate RMS classification: %f%% (%i/%i)\n', overallSuccessRate, (length(calcClasses)-nMissclass), nTotalClass);
    figure; plot (calcClasses);
    xlabel ('Feature Vector'); ylabel('Calculated Class')
    fprintf('--------------------------------------------------\n');
end

% return results
varargout{1} = calcClasses;
if nargout > 1
    varargout{2} = l_c;
end

end