function [Wg, Cg, varargout] = trainLDA(featVecs, classes)
%TRAINLDA trains an LDA (linear discriminant analysis)
%-------------------------------------------------------------------------
%input: 
% #1)  feature vectors (rows = features, colums = observations)
% #2a) CHOOSE: classVector: row vector, where each column entry specifies
%      the class of the corresponding feature vector. 
% #2b) CHOOSE: number of classes. 
%      e.g. if there are 12 feature vectors and classes = 4
%          a classes vector of the form [1,1,1,2,2,2,3,3,3,4,4,4] will be
%          generated (assuming same number of samples for each class)
%      Hence, the second input argument can be a vector or a scalar
% ------------------------------------------------------------------------
%
%output:
% Wg = weight matrix that transforms a new vector to the LDA space,
% #classes x #features
% Cg = offset compensation matrix, #classes x 1
%
% optional: 
% #1: vector of true classes - 1 x length(featVecs)
% #2: pooled covariance matrix
% #3: matrix of mean vectors
% 
% 
% application of output for an unknown featureVector y:
% l_c = Wg*y + Cg
% l_c is a #classes x 1 vector. classify y to the class of the row that has the
% highest entry in l_c
% e.g. l_c = [12 4 90 2 32 4]' => classify to class 3
%
%[Wg Cg trueClasses] = trainLDA(featVecs, classes) additionally outputs the
%calculated classes vector (useful when only the number of classes (input 2a) was given)
%
%see also: applyLDA

%make sure that the dimensions seem legit
%if there are more Features than observations, the feature matrix is
%maybe transposed. 
%However, it might be plausible, so only issue a warning
[nFeatures, nObservations] = size(featVecs);
if nFeatures > nObservations
    display('trainLDA: Please make sure featureVector is in the correct orientation, rows = features, colums = observations');
end

%if the user only provided the number of classes, assume that there are
%equally many observations per class. Generate the classes vector
if isscalar(classes) 
    if rem(nObservations, classes) ~= 0
        error('The number of observations cannot be linearly split to classes. Make sure that feature matrix is of correct size or provide classes vector manually');
    end
    nPerClass = nObservations/classes;
    classes = 1:classes;
    classes = repmat(classes, nPerClass,1);
    classes = classes(:)';
else
    if length(classes) ~= nObservations
        error('Number of observations and number of class identifications is not equal');
    end
end
nClasses=max(classes);

%for each class, calculate the covariance matrix and pool all together
%also, calculate the mean feature vector for each class
pooledCovMat = zeros(nFeatures, nFeatures);
for currClass = 1:nClasses
    currCovMat = cov(featVecs(:,classes==currClass)');
    meanMat(:,currClass) = mean(featVecs(:,classes==currClass),2);
    pooledCovMat = pooledCovMat + currCovMat;
end
pooledCovMat = pooledCovMat/nClasses;

%calculate Cg
%(currently maybe not the most elegant way)
%invPooledCov = inv(pooledCovMat);
Cg = zeros(nClasses,1);
for i=1:nClasses
    Cg(i) =  meanMat(:,i)' / pooledCovMat *  meanMat(:,i);
end
Cg = log(1/nClasses) - 0.5*Cg';

%calculate Wg
Wg = meanMat' / pooledCovMat;

%return optional results
if nargout > 2
    varargout{1} = classes;
end
if nargout > 4
    varargout{2} = pooledCovMat;
    varargout{3} = meanMat;
end

