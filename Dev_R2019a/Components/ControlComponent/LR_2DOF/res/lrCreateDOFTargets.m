function DOFTargets = lrCreateDOFTargets(TDFMatrixTrain, Targets, DOFs, classesOfInterest)
%same for the targets
c = size(Targets,2); %to init after NoMove ends
if c>0
    DOFTargets = zeros(size(TDFMatrixTrain,2),2);
else
    DOFTargets=[];
end
c = 1;
if c>0
    for classi = classesOfInterest 
        for i=1:size(DOFs,1)
            if classi == DOFs(i,1)
                DOFTargets(c:c+size(Targets,2)-1,i) =  Targets(DOFs(i,1),:);
                c = c+size(Targets,2);
            end
            if classi == DOFs(i,2)
                DOFTargets(c:c+size(Targets,2)-1,i) = -Targets(DOFs(i,2),:); 
                c = c+size(Targets,2);
            end
        end  
    end
    %DOFTargets = [DOFTargets; zeros(1,size(DOFTargets,2))];
else
    DOFTargets=[];
end
end
