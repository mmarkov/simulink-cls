function calibLR(artefactRemovalChannel, timingUsed, usedFeatures, thrZC, DOFs)
if any(DOFs == 0)
    errordlg('All DOFs need to be allocated!');
end
%% Load data and select classes
algoType = 'LR';
[folderPathSave, trainingFileStr, rawDataMatrixTrain, rawDataTargetsTrain, classesOfInterest, trainClasses, rawDataFromTo, ~, nTrialsTrain, waitH] = loadData(algoType, artefactRemovalChannel);
if ~(ischar(folderPathSave) && exist(folderPathSave,'dir'))
    return
end
% Check the clasess selection
DOFs(DOFs>0) = DOFs(DOFs>0) + 1;
classesDOFs = sort(unique(DOFs))';
classesDOFs(classesDOFs == 0) = 1;
if ~isequal(classesOfInterest, classesDOFs)
    errordlg('The classes selected from Training Files and Mask-DOFs must be the same!');
    close(waitH);
    return;
end
windowLength = timingUsed(1);
interOffset = timingUsed(2);
nSamplesPerTrial = length(rawDataFromTo);
totNumOfClasses = length(classesOfInterest);

%% Calculate Feature matrices of the signals
featArray = {'RMS', 'ZC', 'WL', 'SSC'};
feats = featArray(usedFeatures);
sscThresh = thrZC;
[TDFMatrixTrain, nWindows, nChannels, nFeatures] = getFeatureMatrix(rawDataMatrixTrain, feats, thrZC, sscThresh, windowLength, interOffset, nSamplesPerTrial);
wrongOrder  = repmat(1:nFeatures,[1 nChannels]);
[~, rightOrder] = sort(wrongOrder, 'ascend');
TDFMatrixTrain = TDFMatrixTrain(rightOrder,:);

%% subsample the targets to have as many data points as the feature matrix
TargetsTrainPre = rawDataTargetsTrain(:,floor(linspace(1,length(rawDataTargetsTrain), nWindows*nTrialsTrain )));
TargetsTrainPre = TargetsTrainPre*100;

%% Train LR
classCtr=0;
TargetsTrain=NaN(10,size(TargetsTrainPre,2));
for classi=classesOfInterest
    classCtr=classCtr+1;
    TargetsTrain(classesOfInterest(classCtr),:)=TargetsTrainPre(classCtr,:);
end
DOFTargetsTrain = lrCreateDOFTargets(TDFMatrixTrain, TargetsTrain, DOFs, classesOfInterest);
W_LR = lrTrainLinearRegression(TDFMatrixTrain, DOFTargetsTrain);
waitVal = 0.6;
waitbar(waitVal, waitH, [algoType ': Plotting ...']);

%% PCA Feature-space
featsName='';
for i = 1:length(feats)
    featsName=[featsName feats{i}];
end
motionLabels = cell (1, totNumOfClasses);
j = 1;
for i = classesOfInterest
    motionLabels{j} = ['Class ' num2str(i-1)];
    j = j+1;
end
optionsPlot.MotionLabels = motionLabels;
optionsPlot.Colors = mat2cell(lines(totNumOfClasses), ones(1, totNumOfClasses), 3);
optionsPlot.FigTitle = ['Calib file(s): ' trainingFileStr];
displayFeatureSpace(TDFMatrixTrain, reshape(repmat(trainClasses(~isnan(trainClasses)),size(TDFMatrixTrain,2)/size(trainClasses(~isnan(trainClasses)),2),1),size(TDFMatrixTrain,2),[]), optionsPlot);

waitVal = waitVal + 0.2;
waitbar(waitVal, waitH, [algoType ': Finishing ...']);

%% SAVE THE DATA
goodCh=setdiff(1:nChannels,artefactRemovalChannel);
set_param(gcb,'W_LR',mat2str(W_LR));
set_param(gcb,'goodCh',mat2str(goodCh));
set_param(gcb,'trainingFile',trainingFileStr);
if ischar(folderPathSave) && exist(folderPathSave,'dir')
    set_param(gcb,'trainingFolder',folderPathSave);
end

waitVal = waitVal + 0.2;
waitbar(waitVal, waitH, [algoType ': Done!']);
pause(0.5);
close(waitH)

end
