function W = lrTrainLinearRegression(DOFActivationsTrain, DOFTargetsTrain )
%TRAINLINEARREGRESSION
W = (DOFActivationsTrain*DOFActivationsTrain')\(DOFActivationsTrain*DOFTargetsTrain);
