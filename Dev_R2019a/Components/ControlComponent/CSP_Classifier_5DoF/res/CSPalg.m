function [W_sorted, D1_normSorted, D2_normSorted] = CSPalg (Sigma1, Sigma2)
%CSP: Performs a common spatial pattern analysis of two signals with the
%covariance matrices Sigma1 and Sigma2, respectively.
%
%------
%input: 
%   Sigma1:     Covariance matrix of the first  signal
%   Sigma2:     Covariance matrix of the second signal
%
%-------
%output:
%   W_sorted:   matrix that transforms both input signals to the component
%               space, containing the filter coefficients in its rows. 
%               i.e., W(1,:)*x yields the first component, W(2,:)*x the
%               second etc. The variance of the first component is
%               maximized when x1 is presented,


%normalize matrices: saw that in a paper and works better
%Joint Diagonalization of Correlation Matrices by Using Newton Methods With
%Application to Blind Signal Separation, M. Joho & K. Rahbar SAM2002
%Sigma1 = Sigma1/norm(Sigma1,'fro');
%Sigma2 = Sigma2/norm(Sigma2,'fro');


% should be 1 now
% norm(Sigma1, 'fro')
% norm(Sigma2, 'fro')

%four joint diagonalisation methods. Eigenspectra are identical
%tic;[Ds, W] = LUJ1D([Sigma1, Sigma2]); toc; %Elapsed time is 0.329314 seconds.
%tic;[Ds, W] = LUJ2D([Sigma1, Sigma2]); toc; %Elapsed time is 1.311964 seconds.
%tic;[Ds, W] = QRJ1D([Sigma1, Sigma2]); toc; %Elapsed time is 0.245094 seconds.
%tic;[Ds, W] = QRJ2D([Sigma1, Sigma2]); toc; %Elapsed time is 0.527020 seconds.
% [Ds, W] = QRJ1D([Sigma1, Sigma2]);
% %Ds contains a concatination of the two diagonal matrices. Seperate them
% D1 = Ds(:,1:size(Sigma1,1));
% D2 = Ds(:,size(Sigma2,1)+1:end);

%generalized eigenvalue decomposition does the trick as well for only 2 Sigmas.
[W, evals] = eig(Sigma1,Sigma2);
W=W';
D1 = W*Sigma1*W';
D2 = W*Sigma2*W';

%sort the difference of the diagonal matrices and W with it 
Ds_sum = D1+D2; %get the sum of the matrices 
f = ones(size(Ds_sum,1),1)./diag(Ds_sum); %calculate factors that normalize the main diagonal to 1

W = W.*sqrt(repmat(f,1,size(W,1))); %multiply W by the sqrt of the factors (projection = quadratic function W'*S*W => therfore need square roots!)
D1_norm = W*Sigma1*W'; %perform the projection for the first cov. Matrix
D2_norm = W*Sigma2*W';   %perform the projection for the second cov. Matrix

W_sorted = sortrows([diag(D2_norm),W],1); %add the diagonal of the second class as first colum to W and sort rows according to that order
W_sorted = W_sorted(:,2:end); %remove the first column again

D1_normSorted = W_sorted*Sigma1*W_sorted';
D2_normSorted= W_sorted*Sigma2*W_sorted';
%the diagonal from D1_normSorted now goes from ~1 to ~0
%the diagonal from D2_normSorted  now goes from ~0 to ~1

% %plot eigenspectra
% figure;
% plot ([diag(D1_normSorted),diag(D2_normSorted)]);
% axis([1 length(diag(D1_normSorted)) 0 1]);

end

