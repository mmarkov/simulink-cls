function [W, classesInCSP] = trainCSP(inputFeatures, inputClasses)
%TRAINCSPR Trains a CSP regression
nDims = size(inputFeatures,1);
nSamples = size(inputFeatures,2);
if isscalar(inputClasses) 
    if rem(nSamples, inputClasses) ~= 0
        error('trainCSPR: The number of observations cannot be linearly split to classes. Make sure that feature matrix is of correct size or provide classes vector manually');
    end
    nPerClass = nSamples/inputClasses;
    inputClasses = 1:inputClasses;
    inputClasses = repmat(inputClasses, nPerClass,1);
    inputClasses = inputClasses(:)';
else
    if length(inputClasses) ~= nSamples
        error('trainCSPR: Number of observations and number of class identifications is not equal');
    end
end
nClasses = max(inputClasses);
if length(sort(unique(inputClasses))) ~= nClasses
    error ('trainCSPR: inputClasses must be contigeous');
end

CorrMats = zeros(nDims, nDims, nClasses);

% 1) calculate correlation matrices per class
for iClass = 1:nClasses 
    CorrMats(:,:,iClass) = inputFeatures(:,inputClasses==iClass) * inputFeatures(:,inputClasses==iClass)';
end

% 2) Calculate a CSP filter for each two-class combination
c = 1;
classesInCSP = [];
for iClass = 1:nClasses
    for jClass = iClass+1:nClasses
        [W_temp, d1, d2] = CSPalg(CorrMats(:,:,iClass), CorrMats(:,:,jClass));
        %the row vectors of W_temp are the spatial filters
        W(:,c) = W_temp(1,:)';
        W(:,c+1) = W_temp(end,:)';
        classesInCSP(c) = iClass;
        classesInCSP(c+1) = jClass;
        c=c+2;
    end
end

end

