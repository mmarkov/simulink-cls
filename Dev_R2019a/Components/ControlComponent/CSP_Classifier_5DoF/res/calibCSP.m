function calibCSP(artefactRemovalChannel, timingUsed, usedFeatures, thrZC, DOFs)
%% Load data and select classes
algoType = 'CSP';
[folderPathSave, trainingFileStr, rawDataMatrixTrain, rawDataTargetsTrain, classesOfInterest, trainClasses, rawDataFromTo, nChannelsPre, nTrialsTrain, waitH] = loadData(algoType, artefactRemovalChannel);
if ~(ischar(folderPathSave) && exist(folderPathSave,'dir'))
    return
end
% Check the clasess selection
DOFs(DOFs>0) = DOFs(DOFs>0) + 1;
classesDOFs = sort(unique(DOFs))';
classesDOFs(classesDOFs == 0) = 1;
if ~isequal(classesOfInterest, classesDOFs)
    errordlg('The classes selected from Training Files and Mask-DOFs must be the same!');
    close(waitH);
    return;
end
windowLength = timingUsed(1);
interOffset = timingUsed(2);
nSamplesPerTrial = length(rawDataFromTo);
totNumOfClasses = length(classesOfInterest);

%% Calculate Feature matrices of the signals
featArray = {'RMS', 'ZC', 'WL', 'SSC'};
feats = featArray(usedFeatures);
sscThresh = thrZC;
[TDFMatrixTrain, nWindows, nChannels, nFeatures] = getFeatureMatrix(rawDataMatrixTrain, feats, thrZC, sscThresh, windowLength, interOffset, nSamplesPerTrial);
wrongOrder  = repmat(1:nFeatures,[1 nChannels]);
[~, rightOrder] = sort(wrongOrder, 'ascend');
TDFMatrixTrain = TDFMatrixTrain(rightOrder,:);

%% subsample the targets to have as many data points as the feature matrix
TargetsTrainPre = rawDataTargetsTrain(:,floor(linspace(1,length(rawDataTargetsTrain), nWindows*nTrialsTrain )));
TargetsTrainPre = TargetsTrainPre*100;

%% Train CSP
[W_CSP, classes_CSP] = trainCSP(TDFMatrixTrain, totNumOfClasses);
% Apply Training data to calculate the postprocessing parameters (linear Reg)
activationsPerClassTrainPre = applyCSPR(W_CSP, TDFMatrixTrain, classes_CSP); %re-apply TRAIN data!
classCtr=0;
activationsPerClassTrain=zeros(size(activationsPerClassTrainPre,1),10);
TargetsTrain=NaN(10,size(TargetsTrainPre,2));
for classi = classesOfInterest
    classCtr=classCtr+1;
    activationsPerClassTrain(:,classesOfInterest(classCtr)) = activationsPerClassTrainPre(:,classCtr);
    TargetsTrain(classesOfInterest(classCtr),:)=TargetsTrainPre(classCtr,:);
end

[DOFActivationsTrain, DOFTargetsTrain] = applyCSPRPosProcessGen(activationsPerClassTrain, TargetsTrain, DOFs, true, classesOfInterest);
%train a linear regression to fit the (raw) estimated values to the targets
selectedDOFs = logical(sum(DOFs>0,2)');
LR_W_CSP = cspTrainLinearRegression(DOFActivationsTrain, DOFTargetsTrain, selectedDOFs);
waitVal = 0.6;
waitbar(waitVal, waitH, [algoType ': Plotting ...']);

%% PCA Feature-space
featsName='';
for i = 1:length(feats)
    featsName=[featsName feats{i}];
end
motionLabels = cell (1, totNumOfClasses);
j = 1;
for i = classesOfInterest
    motionLabels{j} = ['Class ' num2str(i-1)];
    j = j+1;
end
optionsPlot.MotionLabels = motionLabels;
optionsPlot.Colors = mat2cell(lines(totNumOfClasses), ones(1, totNumOfClasses), 3);
optionsPlot.FigTitle = ['Feature space for: ' trainingFileStr];
displayFeatureSpace(TDFMatrixTrain, reshape(repmat(trainClasses(~isnan(trainClasses)),size(TDFMatrixTrain,2)/size(trainClasses(~isnan(trainClasses)),2),1),size(TDFMatrixTrain,2),[]), optionsPlot);

waitVal = waitVal + 0.2;
waitbar(waitVal, waitH, [algoType ': Finishing ...']);

%% SAVE THE DATA
goodCh = setdiff(1:nChannelsPre,artefactRemovalChannel);
set_param(gcb,'W_CSP_constant',mat2str(W_CSP));
set_param(gcb,'LR_W_CSP_constant',mat2str(LR_W_CSP));
set_param(gcb,'classes_CSP_constant',mat2str(classes_CSP));
set_param(gcb,'goodCh',mat2str(goodCh));
set_param(gcb,'trainingFile',trainingFileStr);
set_param(gcb,'classesOfInterest',mat2str(classesOfInterest));

if ischar(folderPathSave) && exist(folderPathSave,'dir')
    set_param(gcb,'trainingFolder',folderPathSave);
end

waitVal = waitVal + 0.2;
waitbar(waitVal, waitH, [algoType ': Done!']);
pause(0.5);
close(waitH)

end


