modelBlock = gcb;
userData = get_param(modelBlock, 'UserData');
userData.sysReady = 1;
userData.Text = 'STOP';
set_param(modelBlock, 'UserData', userData);
% Force refresh of the block
val = str2double(get_param(modelBlock, 'forceRedraw'));
if val>=1000
    val = 0;
end
set_param(modelBlock, 'forceRedraw' , num2str(val+1));
clear userData modelBlock val