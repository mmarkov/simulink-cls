% Play stop audio cue
audioCue = get_param(gcb,'audioCue' );  
RES = [evalin('base','PATH.get(gcb)') '/res/'];

if strcmp(audioCue,'on')
    system_dependent(14, 0);
    [soundCue, soundFs] = audioread([RES 'simStopSound.wav']);
    sound(soundCue*2, soundFs*1.6);
else
    system_dependent(14, 1);
end

clear audioCue RES soundCue soundFs