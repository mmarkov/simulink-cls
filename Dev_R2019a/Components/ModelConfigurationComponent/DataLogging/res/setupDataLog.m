% Prevent the library to modify itself - only the reference is modified
function setupDataLog(model_Handle, block_Handle, maxSN)
if strcmp(get_param(model_Handle, 'LibraryType'), 'None') && strcmp(get_param(model_Handle, 'Type'), 'block_diagram') && ...
          ((strcmp( get_param(model_Handle,'SimulationStatus'), 'stopped')) || (strcmp( get_param(model_Handle,'SimulationStatus'), 'initializing'))) 
    cs = getActiveConfigSet(model_Handle);
    if get_param(model_Handle,'ExtModeTrigDuration') ~= maxSN && isempty(strfind(cs.get_param('Name'), 'Arduino ERT')) && isempty(strfind(cs.get_param('Name'), 'External Mode Configuration'))   
        set_param(model_Handle,'ExtModeTrigDuration', maxSN)
    end
    
    if (~isempty(strfind(cs.get_param('Name'), 'Arduino ERT')) || ~isempty(strfind(cs.get_param('Name'), 'RaspberryPi ERT'))) && (get_param(model_Handle,'ExtModeTrigDuration') ~= 2)
        set_param(model_Handle,'ExtModeTrigDuration', 2); % Ignore the parameter value when running on Arduino or RaspberryPi
    end
    
    try
%         countTs = get_param(model_Handle,'FixedStep');
        set_param([block_Handle '/CurrentSample'], 'tsamp', '-1');
        % Retrieve all the blocks we need
        gotostmp = find_system(model_Handle, 'BlockType', 'Goto');
        
        toWorkspaces = find_system(block_Handle, 'FollowLinks', 'on', 'LookUnderMasks', 'all', 'BlockType', 'ToWorkspace');
        terminators = find_system(block_Handle, 'FollowLinks', 'on', 'LookUnderMasks', 'all', 'BlockType', 'Terminator');
        froms = find_system(block_Handle,'FollowLinks', 'on', 'LookUnderMasks', 'all', 'BlockType', 'From');
        
        gotos = {};
        for i = 1:size(gotostmp,1)
            if strcmp(get_param(gotostmp(i), 'TagVisibility'), 'global')
                gotos = [gotos, gotostmp(i)];
            end
        end
        
        numberGotos = max(size(gotos));
        numberFroms = size(froms,1);
        
        % Delete all From, Terminator and To Workspace blocks
        if numberFroms > 0
            for i = 1:numberFroms
                if isempty(terminators)
                    delete_line(block_Handle, ['From' num2str(i) '/1'], ['To Workspace' num2str(i) '/1']);
                else
                    delete_line(block_Handle, ['From' num2str(i) '/1'], ['Terminator' num2str(i) '/1']);
                end
            end
            for i = 1:numberFroms
                delete_block(froms{i});
                if isempty(terminators)
                    delete_block(toWorkspaces{i});
                else
                    delete_block(terminators{i});
                end
            end
        end
        
        % Add new blocks according to the number of Goto blocks in our model
        
        if (numberGotos > 0)
            for i1 = 1:numberGotos
                tmp1 = add_block('built-in/From', [block_Handle '/From' num2str(i1)], 'Position', [500 0+i1*50 550 25+i1*50]);
                set_param(tmp1, 'GotoTag', get_param(gotos{i1},'GotoTag'));
                if strcmp(get_param(block_Handle, 'logData'), 'on') == 1
                    tmp2 = add_block('built-in/ToWorkspace', [block_Handle '/To Workspace' num2str(i1)], 'Position', [650 0+i1*50 725 25+i1*50]);
                    add_line(block_Handle, ['From' num2str(i1) '/1'], ['To Workspace' num2str(i1) '/1']);
                    set_param(tmp2, 'VariableName', ['simout' get_param(gotos{i1},'GotoTag')]);
                    set_param(tmp2, 'MaxDatapoints', num2str(maxSN));
                    set_param(tmp2, 'SampleTime', '-1');
                    set_param(tmp2, 'SaveFormat', 'Structure With Time');
                else
                    add_block('built-in/Terminator', [block_Handle '/Terminator' num2str(i1)], 'Position', [650 0+i1*50 725 25+i1*50]);
                    add_line(block_Handle, ['From' num2str(i1) '/1'], ['Terminator' num2str(i1) '/1']);
                end
            end
        end
        
        disp('Configuring data logging ... Done!');
    
    catch ME
    end
    
end

end