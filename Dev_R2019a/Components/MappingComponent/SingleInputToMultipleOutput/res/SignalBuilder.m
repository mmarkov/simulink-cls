function SignalBuilder()
% UISIGNALBUILDER Signal Builder GUI
%
% UISIGNALBUILDER allows the user to create, delete and drag control points in
% order to build a signal using different interpolation methods.
%
% Useful commands:
%  - double-click on a blank spot to create a control point
%  - double-click on an existing control point to delete it
%  - right-click on a control point to specify its coordinates manually
%  - drag control points to adjust interpolation
%
% Example:
%  >> uisignalbuilder()
%
% Copyright and legal notice:
%
% Author: Laurent VAYLET
% Release: 1.0
% Release date: 04/22/2009
%
% Copyright 2009 - Laurent VAYLET
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%     * Neither the name of the The MathWorks, Inc nor the names
%       of its contributors may be used to endorse or promote products derived
%       from this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.
%
% Modifications made by John Linde




% Create controls
% Do not specify positions as GridBagLayout will take care of the layout later
screenSize = get(0,'screensize');
figureWidth = 600;
figureHeight = 650;

hFig = figure('Name', 'Signal Builder', ...
    'NumberTitle', 'off', ...
    'MenuBar', 'none', ...
    'Color', 'w', ...
    'Position', [screenSize(3)/2 - figureWidth/2, screenSize(4)/2 - figureHeight/2, figureWidth, figureHeight]);

hAxes = axes('Parent', hFig, ...
    'ButtonDownFcn', @(src,evt)axesbuttondown());

hHelp = uicontrol(hFig, ...
    'Style', 'pushbutton', ...
    'BackgroundColor', 'w', ...
    'String', 'Help', ...
    'Callback', @(srv,evt)showhelp());

hOptions = uipanel(hFig, 'Title', ' Options ', 'FontWeight', 'bold', 'BackgroundColor', 'w', 'ForegroundColor', [0.8 0.5 0.1]);

hLookup = uipanel(hFig, 'Title', ' Lookup Table ', 'FontWeight', 'bold', 'BackgroundColor', 'w', 'ForegroundColor', [0.8 0.5 0.1]);

hMatExpr = uipanel(hFig, 'Title', ' Matlab Expression ', 'FontWeight', 'bold', 'BackgroundColor', 'w', 'ForegroundColor', [0.8 0.5 0.1]);

hSamplingPeriodLbl = uicontrol(hOptions, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'Sampling period (s):', ...
    'HorizontalAlignment', 'left');
hSamplingPeriod = uicontrol(hOptions, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', '0.01', ...
    'Callback', @(src,evt)changesamplingperiod(src), ...
    'ToolTipString', 'Sampling period (s)');
hSignalLengthLbl = uicontrol(hOptions, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'X limits:', ...
    'HorizontalAlignment', 'left');
hSignalLength = uicontrol(hOptions, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', '-1 1', ...
    'Callback', @(src,evt)changesignallength(src), ...
    'ToolTipString', 'Change the horizontal limits of the signal.');
hGridIncLbl = uicontrol(hOptions, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'Grid resolution:', ...
    'HorizontalAlignment', 'left');
hGridInc = uicontrol(hOptions, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', '0.1 0.1', ...
    'Callback', @(src,evt)changegridinc(src), ...
    'ToolTipString', 'Changes the grid resolution');
hYLimsLbl = uicontrol(hOptions, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'Y limits:', ...
    'HorizontalAlignment', 'left');
hYLims = uicontrol(hOptions, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', '0 1', ...
    'Callback', @(src,evt)changeylims(src), ...
    'ToolTipString', 'Y limits');
hSnapGrid = uicontrol(hOptions, ...
    'Style', 'checkbox', ...
    'BackgroundColor', 'w', ...
    'String', 'Snap To Grid', ...
    'Value', 1, ...
    'Callback', @(src,evt)togglesnapgrid());
hDiscrete = uicontrol(hOptions, ...
    'Style', 'pushbutton', ...
    'BackgroundColor', 'w', ...
    'String', 'Discrete', ...
    'Value', 1, ...
    'Callback', @(src,evt)makeDiscrete(), ...
    'ToolTipString', 'Discretize given points');
hSaveBp = uicontrol(hOptions, ...
    'Style', 'pushbutton', ...
    'BackgroundColor', 'w', ...
    'String', 'Save', ...
    'Value', 1, ...
    'Callback', @(src,evt)savebp(), ...
    'ToolTipString', 'Save lookup table to workspace');

hBreakpointsLbl = uicontrol(hLookup, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'Input:', ...
    'HorizontalAlignment', 'left');
hBreakpoints = uicontrol(hLookup, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', '0 1', ...
    'Callback', @(src,evt)changeBreakpoints(), ...
    'ToolTipString', 'Breakpoints');
hValuesLbl = uicontrol(hLookup, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'Output:', ...
    'HorizontalAlignment', 'left');
hValues = uicontrol(hLookup, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', '0 0', ...
    'Callback', @(src,evt)changeValues(), ...
    'ToolTipString', 'Values');

hExprLbl = uicontrol(hMatExpr, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'Expression:', ...
    'HorizontalAlignment', 'left');
hExpr = uicontrol(hMatExpr, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', 'sqrt(x)', ...
    'ToolTipString', 'Matlab Expression');
hResolutionLbl = uicontrol(hMatExpr, ...
    'Style', 'text', ...
    'BackgroundColor', 'w', ...
    'String', 'Resolution:', ...
    'HorizontalAlignment', 'left');
hResolution = uicontrol(hMatExpr, ...
    'Style', 'edit', ...
    'BackgroundColor', 'w', ...
    'String', '5', ...
    'ToolTipString', 'Resolution in samples per one unit');
hEvaluate = uicontrol(hMatExpr, ...
    'Style', 'pushbutton', ...
    'BackgroundColor', 'w', ...
    'String', 'Evaluate', ...
    'Value', 1, ...
    'Callback', @(src,evt)evalExpr(), ...
    'ToolTipString', 'Evaluate Matlab expression');

% Layout controls using GridBagLayout
figureLayout = layout.GridBagLayout(hFig, 'HorizontalGap', 15, 'VerticalGap', 15);
figureLayout.add(hAxes, [1 2], 1, 'Fill', 'Both');
figureLayout.add(hHelp, 1, 2, 'Fill', 'Horizontal', 'MinimumHeight', 25);
figureLayout.add(hOptions, [2 3 4], 2, 'Fill', 'Vertical', 'MinimumWidth', 130);
figureLayout.add(hLookup, 3, 1, 'Fill', 'Horizontal', 'MinimumWidth', 130, 'MinimumHeight', 80);
figureLayout.add(hMatExpr, 4, 1, 'Fill', 'Horizontal', 'MinimumWidth', 130, 'MinimumHeight', 80);
figureLayout.VerticalWeights = [0 1];
figureLayout.HorizontalWeights = [1 0];
figureLayout.setConstraints([1 2], 1, ... % hAxes
    'LeftInset', 25, ...
    'BottomInset', 25, ...
    'RightInset', 15, ...
    'TopInset', 15);

optionsLayout = layout.GridBagLayout(hOptions, 'HorizontalGap', 15, 'VerticalGap', 0);
optionsLayout.add(hSamplingPeriodLbl, 1, 1, 'Fill', 'Horizontal');
optionsLayout.add(hSamplingPeriod,    2, 1, 'Fill', 'Horizontal');
optionsLayout.add(hSignalLengthLbl,   5, 1, 'Fill', 'Horizontal');
optionsLayout.add(hSignalLength,      6, 1, 'Fill', 'Horizontal');
optionsLayout.add(hYLimsLbl,          7, 1, 'Fill', 'Horizontal');
optionsLayout.add(hYLims,             8, 1, 'Fill', 'Horizontal');
optionsLayout.add(hGridIncLbl,        9, 1, 'Fill', 'Horizontal');
optionsLayout.add(hGridInc,           10, 1, 'Fill', 'Horizontal');
optionsLayout.add(hSnapGrid,          11, 1, 'Fill', 'Horizontal', 'Anchor', 'North');
optionsLayout.add(hDiscrete,          12, 1, 'Fill', 'Horizontal', 'MinimumHeight', 30, 'Anchor', 'North');
optionsLayout.add(hSaveBp,            13, 1, 'Fill', 'Horizontal', 'MinimumHeight', 30, 'Anchor', 'South');
optionsLayout.VerticalWeights = [0 0 0 0 0 0 0 0 0 0 0 1 0];
optionsLayout.HorizontalWeights = 1;
optionsLayout.setConstraints(1, 1, ... % hSamplingPeriodLbL
    'TopInset', 15);
optionsLayout.setConstraints(5, 1, ... % hSignalLengthLbL
    'TopInset', 15);
optionsLayout.setConstraints(7, 1, ... % hYLims
    'TopInset', 15);
optionsLayout.setConstraints(9, 1, ... % hGridInc
    'TopInset', 15);
optionsLayout.setConstraints(11, 1, ... % hSnapGrid
    'TopInset', 15);
optionsLayout.setConstraints(12, 1, ... % hDiscrete
    'TopInset', 15);
optionsLayout.setConstraints(13, 1, ... % hSaveBp
    'BottomInset', 15);

lookupLayout = layout.GridBagLayout(hLookup, 'HorizontalGap', 15, 'VerticalGap', 10);
lookupLayout.add(hBreakpointsLbl,  1, 1, 'Fill', 'Horizontal');
lookupLayout.add(hBreakpoints,     2, 1, 'Fill', 'Horizontal');
lookupLayout.add(hValuesLbl,       1, 2, 'Fill', 'Horizontal');
lookupLayout.add(hValues,          2, 2, 'Fill', 'Horizontal');

lookupLayout.setConstraints(1, 1, ...
    'TopInset', 10);
lookupLayout.setConstraints(1, 2, ...
    'TopInset', 10);

matExprLayout = layout.GridBagLayout(hMatExpr, 'HorizontalGap', 15, 'VerticalGap', 10);
matExprLayout.add(hExprLbl,  1, 1, 'Fill', 'Horizontal');
matExprLayout.add(hExpr,     2, 1, 'Fill', 'Horizontal');
matExprLayout.add(hResolutionLbl,       1, 2, 'Fill', 'Horizontal');
matExprLayout.add(hResolution,          2, 2, 'Fill', 'Horizontal');
matExprLayout.add(hEvaluate,          2, 3, 'Fill', 'Horizontal');

matExprLayout.setConstraints(1, 1, ...
    'TopInset', 10);
matExprLayout.setConstraints(1, 2, ...
    'TopInset', 10);
matExprLayout.setConstraints(2, 1, ...
    'MinimumWidth', 130);

% Perform some initializations
hMarkers = []; % handles to markers
hSignal  = []; % handle to interpolated signal
setappdata(gcf, 'changebp', @changeBreakpoints);
setappdata(gcf, 'changeval', @changeValues);

% Add some default markers
hold(hAxes, 'all')
signalLength = str2double(strsplit(get(hSignalLength, 'String')));
addmarker([signalLength(1) signalLength(2)], zeros(1,2));

% Freeze axis
axis tight
ylim([0 1])
axis equal
axis manual % freeze axis

% Add a grid
xLim = xlim(hAxes);
yLim = ylim(hAxes);
xGridInc = 0.1;
yGridInc = 0.1;
xGrid = xLim(1):xGridInc:xLim(2);
yGrid = yLim(1):yGridInc:yLim(2);
[XGrid,YGrid] = meshgrid(xGrid, yGrid);
hGrid = plot(XGrid, YGrid, ...
    'Color', [.6 .6 .6], ...
    'LineStyle', 'none', ...
    'Marker', '.', ...
    'MarkerSize', 1, ...
    'HitTest', 'off');
snapGrid = true;

% Update plot
updateplot()
updatetable()

% -------------------------------------------------------------------------

    function selectmarker(hMarker)
        
        switch get(hFig, 'SelectionType')
            
            case 'normal' % left click: drag
                set(hFig, 'WindowButtonMotionFcn', @(src,evt)drag(hMarker), ...
                    'WindowButtonUpFcn',@(src,evt)releasemarker());
                
            case 'open' % double click: delete
                if length(hMarkers) > 2
                    delete(hMarker);
                    hMarkers(hMarkers == hMarker) = [];
                    updateplot()
                    updatetable()
                else
                    warning('UISIGNALBUILDER:TwoMarkersNeeded', 'Cannot delete this marker. At least two markers are needed.');
                end
                
            case 'alt' % right click: change coordinates
                prompt = {'X:', 'Y:'};
                dlgTitle = 'Coordinates?';
                numLines = 1;
                defaults = {num2str(get(hMarker, 'XData')), ...
                    num2str(get(hMarker, 'YData'))};
                answer = inputdlg(prompt, dlgTitle, numLines, defaults);
                if ~isempty(answer)
                    set(hMarker, 'XData', str2double(answer{1}), ...
                        'YData', str2double(answer{2}));
                    updateplot()
                    updatetable()
                end
        end
        
    end

% -------------------------------------------------------------------------

    function [x,y] = getclosestgridpoint(x,y)
        
        x = xGrid(abs(xGrid - x) <= xGridInc/2);
        y = yGrid(abs(yGrid - y) <= yGridInc/2);
        
    end

% -------------------------------------------------------------------------

    function drag(hMarker)
        
        oldX = cell2mat(get(hMarkers, 'XData'));
        oldX(oldX == get(hMarker, 'XData')) = [];
        cp = get(hAxes, 'CurrentPoint');
        x = cp(1,1);
        y = cp(1,2);
        
        if x > xLim(2)
            x = xLim(2);
        end
        if x < xLim(1)
            x = xLim(1);
        end
        
        if y > yLim(2)
            y = yLim(2);
        end
        if y < yLim(1)
            y = yLim(1);
        end
        
        if snapGrid
            [x, y] = getclosestgridpoint(x, y);
        end
        
        if(isempty(oldX(oldX == x)))
            set(hMarker, 'XData', x, 'YData', y, 'ZData', -1);
        end
        
        updateplot()
        updatetable()
        
    end

% -------------------------------------------------------------------------

    function releasemarker()
        
        set(hFig,'WindowButtonMotionFcn',[]);
        
    end

% -------------------------------------------------------------------------

    function updateplot()
        
        chosenMethod = 'linear';
        
        x = cell2mat(get(hMarkers, 'XData'));
        [x, sortOrder] = sort(x);
        y = cell2mat(get(hMarkers, 'YData'));
        y = y(sortOrder);
        
        % Use specified sampling period for interpolating values
        xi = xLim(1):str2double(get(hSamplingPeriod, 'String')):xLim(2);
        
        try
            yi = interp1(x, y, xi, chosenMethod);
        catch me
            if strcmp(me.identifier, 'MATLAB:interp1:RepeatedValuesX')
                yi = NaN(size(xi)); % plot nothing
            end
        end
        
        % Plot if signal does not exist yet, update it otherwise
        if ~isempty(hSignal)
            set(hSignal, 'XData', xi, 'YData', yi, 'Visible', 'on');
        else
            hSignal = plot(xi, yi, 'b', 'HitTest', 'off', 'LineWidth', 2, 'Color', [0.8 0.5 0.1]);
        end
        
    end

% -------------------------------------------------------------------------

    function axesbuttondown()
        
        if strcmp(get(hFig, 'SelectionType'), 'open') % double click
            cp = get(hAxes, 'CurrentPoint');
            addmarker(cp(1,1), cp(1,2));
            updateplot();
            updatetable();
        end
        
    end

% -------------------------------------------------------------------------

    function addmarker(x,y)
        
        narginchk(0, 2)
        
        for k = 1:length(x)
            hMarkers(end+1) = plot3(hAxes, x(k), y(k), -1, 'o', ...
                'MarkerEdgeColor', 'k', ...
                'MarkerFaceColor', 'y', ...
                'MarkerSize', 8, ...
                'ButtonDownFcn', @(src,evt)selectmarker(src)); %#ok<AGROW>
        end
        
    end

% -------------------------------------------------------------------------

    function togglesnapgrid()
        
        snapGrid = ~snapGrid;
        
    end

% -------------------------------------------------------------------------

%     function save()
%         
%         checkLabels = {'Save X coordinates to variable named:' ...
%             'Save Y coordinates to variable named:'};
%         varNames = {'x', 'y'};
%         items = {get(hSignal, 'XData'), get(hSignal, 'YData')};
%         export2wsdlg(checkLabels, varNames, items, 'Save to Workspace');
%         
%     end

% -------------------------------------------------------------------------

    function changesamplingperiod(src)
        
        % Get new sampling period
        newValue = str2double(get(src, 'String'));
        % Validate
        if isempty(newValue)
            warning('UISIGNALBUILDER:InvalidSamplingPeriod', 'Invalid value for sampling period. Resetting to 0.01.');
            newValue = 0.01;
            set(src, 'String', num2str(newValue));
        end
        
        updateplot()
        updatetable()
        
    end

% -------------------------------------------------------------------------

    function changeylims(src)
        
        % Get new sampling period
        newValue = strsplit(get(src, 'String'));
        newValue = str2double(newValue);
        % Validate
        if isempty(newValue)
            warning('UISIGNALBUILDER:InvalidYLimits', 'Invalid value for Y Limits. Resetting to 0 1.');
            newValue = [0 1];
            set(src, 'String', num2str(newValue));
        end
        
        ylim(hAxes, newValue)
        updategrid()
        updateplot()
        
    end

% -------------------------------------------------------------------------

    function changesignallength(src)
        
        % Get new sampling period
        newValue = str2double(strsplit(get(src, 'String')));
        % Validate
        if isempty(newValue)
            warning('UISIGNALBUILDER:InvalidXLimits', 'Invalid value for X limits. Resetting to 0 1.');
            newValue = [0 1];
            set(src, 'String', num2str(newValue));
        end
        
        xlim(hAxes, [newValue(1) newValue(2)])
        updategrid()
        updateplot()
        
    end

% -------------------------------------------------------------------------

    function updategrid()
        
        delete(hGrid)
        xLim = xlim(hAxes);
        yLim = ylim(hAxes);
        xGrid = xLim(1):xGridInc:xLim(2);
        yGrid = yLim(1):yGridInc:yLim(2);
        [XGrid,YGrid] = meshgrid(xGrid, yGrid);
        hGrid = plot(XGrid, YGrid, ...
            'Color', [.6 .6 .6], ...
            'LineStyle', 'none', ...
            'Marker', '.', ...
            'MarkerSize', 1, ...
            'HitTest', 'off');
        
    end

% -------------------------------------------------------------------------

    function showhelp()
        
        mes = {' - Drag a marker to change its position', ...
            ' - Double-click anywhere to create a marker', ...
            ' - Double-click on a marker to delete it', ...
            ' - Right-click on a marker to fine tune its position', ...
            ' ------------------------------------------------------------------ ', ...
            ' - For Matlab Expressions, use x as the time variable'};
        uiwait(msgbox(mes,'Help','help','modal'))
        
    end


% -------------------------------------------------------------------------

    function updatetable()
        
        x = cell2mat(get(hMarkers, 'XData'));
        [x, xIdx] = sort(round((x*1000))/1000);
        set(hBreakpoints, 'String', mat2str(x'));
        
        y = cell2mat(get(hMarkers, 'YData'));
        y = round((y(xIdx)*1000))/1000;
        set(hValues, 'String', mat2str(y'));
        
    end


% -------------------------------------------------------------------------

    function changeBreakpoints()
        
        stringDatatmp = get(hBreakpoints, 'String');
        
        if isempty(stringDatatmp)
            warning('UISIGNALBUILDER:InvalidBreakpoints', 'Invalid value for breakpoints. Resetting to 0 1.');
            stringDatatmp = '[0 1]';
            set(hBreakpoints, 'String', stringDatatmp);
        end
        
        stringData = strsplit(regexprep(stringDatatmp, {'[' ']'}, ''));
        y = str2double(strsplit(regexprep((get(hValues, 'String')), {'[' ']'}, '')))';
        
        hold on
        
        x = zeros(length(stringData),1);
        for i = 1:length(stringData)
            x(i) = str2double(stringData(i));
        end
        
        
        while length(hMarkers) < length(x)
            tmp = cell2mat(get(hMarkers, 'XData'));
            addmarker(tmp(end) + 0.1, 0);
        end
        
        
        while length(hMarkers) > length(x)
            delete(hMarkers(1));
            hMarkers(1) = [];
        end
        
        if length(y) < length(x)
            y = [y; zeros(length(x)-length(y), 1)];
        elseif length(y) > length(x)
            y = y(1:length(x));
        end

        y = y';
        for i = 1:length(hMarkers)
            set(hMarkers(i), 'XData', sort(x(i)), 'YData', y(i));     
        end    
        set(hValues, 'String', mat2str(y));
        
        xLims = str2double(strsplit(get(hSignalLength, 'String')));
        if min(x) < xLims(1)
            xLims(1) = min(x);
        end
        if max(x) > xLims(2)
            xLims(2) = max(x);
        end
        set(hSignalLength, 'String', regexprep(num2str(xLims), ' *', ' '));
        
        updateplot()
        changesignallength(hSignalLength)
        
    end
    
    
% -------------------------------------------------------------------------   
    
    function changeValues()
        
        stringDatatmp = get(hValues, 'String');
        
        if isempty(stringDatatmp)
            warning('UISIGNALBUILDER:InvalidBreakpoints', 'Invalid value for breakpoints. Resetting to 0 0.');
            stringDatatmp = '[0 0]';
            set(hValues, 'String', stringDatatmp);
        end
        
        stringData = regexprep((get(hValues, 'String')), {'[' ']'}, '');
        stringData = strsplit(regexprep(stringData, ';', ' '));
        x = sort(cell2mat(get(hMarkers, 'XData')));
        
        y = zeros(length(stringData),1);
        for i = 1:length(stringData)
            y(i) = str2double(stringData(i));
        end
        
        if length(x) ~= length(y)
            warning('Value dimension is not compatible with breakpoint dimension!')
        else        
            for i = 1:length(hMarkers)
                set(hMarkers(i), 'XData', x(i), 'YData', y(i));     
            end    
        end        
        
        yLims = str2num(get(hYLims, 'String')); %#ok<ST2NM>
        if min(y) < yLims(1)
            yLims(1) = min(y);
        end
        if max(y) > yLims(2)
            yLims(2) = max(y);
        end
        set(hYLims, 'String', regexprep(num2str(yLims), ' *', ' '));
            
        updateplot()
        changeylims(hYLims)
    
    end

    
 % ------------------------------------------------------------------------- 
 
    function evalExpr()
    
        expr = get(hExpr, 'String');
        reso = str2double(get(hResolution, 'String'));
        sigLength = str2double(strsplit(get(hSignalLength, 'String')));
        
        x = sigLength(1):1/reso:sigLength(2);            
        
        try
            y = (round(eval(expr)*1000)/1000);
            set(hBreakpoints, 'String', ['[' regexprep(num2str(x), ' *', ' ') ']']);
            set(hValues, 'String', regexprep(num2str(y), ' *', ' '));
        catch
            warning('UISIGNALBUILDER:InvalidExpression', 'Invalid Matlab expression.')
        end
        
        changeBreakpoints()
        changeValues()
        
    end
 

 % ------------------------------------------------------------------------- 

 function changegridinc(src)
        
        % Get new grid inc
        newValue = strsplit(get(src, 'String'));
        newValue = str2double(newValue);
        % Validate
        if isempty(newValue)
            warning('UISIGNALBUILDER:InvalidGridInc', 'Invalid value for Grid Inc. Resetting to 0.1 0.1.');
            newValue = [0.1 0.1];
            set(src, 'String', num2str(newValue));
        end
        
        xGridInc = newValue(1);
        yGridInc = newValue(2);
        updategrid()
        updateplot()
        
 end


 % ------------------------------------------------------------------------- 

     function savebp()
        
        checkLabels = {'Save breakpoints to variable named:' ...
            'Save values to variable named:'};
        varNames = {'bp', 'val'};
        items = {eval(get(hBreakpoints, 'String')), eval(get(hValues, 'String'))};
        export2wsdlg(checkLabels, varNames, items, 'Save to Workspace');
        
    end

% -------------------------------------------------------------------------
 
    function makeDiscrete()
        
        x = cell2mat(get(hMarkers, 'XData'))';
        [x, sortOrder] = sort(x); %#ok<TRSRT>
        y = cell2mat(get(hMarkers, 'YData'))';
        y = y(sortOrder);
        
        newX = [x(1) kron(x(2:end), ones(1,2))];
        
        while ~isempty(find(diff(newX) == 0, 1));
            idx = find(diff(newX) == 0);
            for i = 1:length(idx)
                newX(idx(i)) = newX(idx(i)) - 0.00001;
            end
        end
        newY = [kron(y(1:end-1), ones(1,2)) y(end)];
        
        try
            set(hBreakpoints, 'String', ['[' regexprep(num2str(newX), ' *', ' ') ']']);
            set(hValues, 'String', regexprep(num2str(newY), ' *', ' '));
        catch
            warning('UISIGNALBUILDER:InvalidDiscretization', 'Invalid discretization.')
        end
        
        changeBreakpoints()
        changeValues()
        
    end    
        
% -------------------------------------------------------------------------

end
