function convertOldMappingFile(fname, pname)
%   Takes a mapping file that has been created with the old mapping
%   library and converts it to be compatible with the new mapping block

    oldFile = load([pname fname]);
    mappData = oldFile.mappData;
    newFile = cell(16,3);
    
    for i = 1:16
        if ~isempty(mappData{i,1})
            tmp = mappData{i,1};
            tmp = regexprep(tmp, ',', '');
            tmp = ['[' tmp ']'];
            newFile{i,1} = tmp;
        end
        
        if ~isempty(mappData{i,2})
            tmp = mappData{i,2};
            tmp = regexprep(tmp, ',', '');
            tmp = ['[' tmp ']'];
            newFile{i,2} = tmp;
        end
        
        if ~isempty(mappData{i,3})
            tmp = mappData{i,3};
            tmp = regexprep(tmp, ',', '');
            tmp = ['[' tmp ']'];
            newFile{i,3} = tmp;
        end
    end

    mappData = newFile;
    defPath = which('convertOldMappingFile.m');
    defPath = defPath(1:end-length('res\convertOldMappingFile.m'));
    defPath = [defPath '\DefaultMappings\'];
    [newfname, newpname] = uiputfile('*.mat', 'Select the location of the new file', [defPath fname]);
    save([newpname newfname], 'mappData');
    
end