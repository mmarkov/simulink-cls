function loadOK = loadMappData( model, fname )

%LOADMAPPDATA Summary of this function goes here
%   Detailed explanation goes here

% Initialize paths
%currentDir = which('MappingBlock.slx');
%currentDir = currentDir(1:end-length('MappingBlock.slx'));
%currDir = [currentDir, 'DefaultMappings'];
loadOK = -1;  % -1 = file does not exist, 0 = file read error (i.e., wrong file format)
handles.FeedbackMdl = model;

% Load mapps

if exist(fname,'file')
    
    loadOK = 0;
    tmp = load(fname);
    try
        mappStruct.data = tmp.mappData;
        % Prealoc data
        numCh = size( mappStruct(1).data, 1 );
        rangeMapp = NaN(numCh, 15);
        intensMapp = NaN(numCh, 15);
        freqMapp = NaN(numCh, 15);
        
        % Convert the table
        
        for iChannel = 1:numCh
            
            mappData = mappStruct.data;
            
            %#ok<*ST2NM>
            
            if ~isempty(mappData{ iChannel, 1})
                inputTemp = str2num(mappData{ iChannel, 1});
            else
                inputTemp = NaN;
                intensTemp = NaN;
                freqTemp = NaN;
            end
            
            if ~isempty(mappData{ iChannel, 2})
                intensTemp = str2num(mappData{ iChannel, 2});
            else
                intensTemp = NaN;
            end
            
            if ~isempty(mappData{ iChannel, 3})
                freqTemp = str2num(mappData{ iChannel, 3});
            else
                freqTemp = NaN;
            end
            
            
            % Format the columns
            numDots = length(inputTemp);
            if length(intensTemp) < numDots
                intensTemp = repmat(intensTemp,[1 length(inputTemp)]);
            end
            if length(freqTemp) < numDots
                freqTemp = repmat(freqTemp,[1 length(inputTemp)]);
            end
            if (length(intensTemp) ~= numDots) || (length(freqTemp) ~= numDots)
                error('The X and Y data values must have the same length!');
            end
            
            % Input the columns
            inputTemp(1) = inputTemp(1) + eps;
            rangeMapp(iChannel, 1:numDots) = inputTemp;
            intensMapp(iChannel, 1:numDots) = intensTemp;
            freqMapp(iChannel, 1:numDots) = freqTemp;
        end
        
        % Update the model
        maskData = get_param(handles.FeedbackMdl, 'UserData');
        maskData.intensMapp = intensMapp;
        maskData.rangeMapp = rangeMapp;
        maskData.freqMapp = freqMapp;
        set_param(handles.FeedbackMdl, 'UserData', maskData);
        loadOK = 1;
    catch
    end
end