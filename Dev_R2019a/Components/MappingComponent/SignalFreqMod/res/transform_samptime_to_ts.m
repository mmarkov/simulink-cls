function outData = transform_samptime_to_ts(inData)

outData.NewBlockPath = '';
outData.NewInstanceData = [];

if (~ismember('Ts', {inData.InstanceData.Name}))
    outData.NewInstanceData.Name = 'Ts';
    outData.NewInstanceData.Values = inData.InstanceData(1).Value;
end