function iconImage = blockLoadIcon(varargin)
% Locate resources folder


RES = [evalin('base','PATH.get(gcb)')  '/res/'];
iconImage = ones(100,100,3,'uint8')*255;

% Read the image
try
    if nargin == 0
        iconImage = imread([RES 'pic.jpg']);
    else
        iconImage = imread([RES varargin{1}]);
    end
catch
    display(['The icon for block: ' gcb ' cannot be found!'])
    return
end

% If the image is grayscale then extend to RGB
if size(iconImage,3) == 1
    iconImage = repmat(iconImage, [1, 1, 3]);
end

end