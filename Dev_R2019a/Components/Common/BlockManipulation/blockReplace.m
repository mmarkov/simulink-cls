function blockReplace(oldblock,newblock)
% Function to replace selected block in the subsystem with the
% desired one
pos = get_param(oldblock,'Position');
orient = get_param(oldblock,'Orientation');
delete_block(oldblock);
add_block(newblock,oldblock,'Position',pos,'Orientation',orient);
end