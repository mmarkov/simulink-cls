% Output
color('black');
numberOfPorts = get_param(gcb, 'Ports');
if ~exist( 'outNames' , 'var') || ~iscell( outNames )
    outNames = {'Out', 'Out2', 'Out3', 'Out4', 'Out5', 'Out6', 'Out7', 'Out8'};
end
for i=1 : numberOfPorts(2) 
    port_label('output', i,outNames {i});
end
% Input
if ~exist( 'inNames' , 'var') || ~iscell( inNames )
    inNames = {'In', 'In2', 'In3', 'In4', 'In5', 'In6', 'In7', 'In8'};
end
for i=1 : numberOfPorts(1)
    port_label('input', i, inNames {i}); 
end
% Important info
color('blue');
if  exist('Ts', 'var')
    if isempty(Ts)
        text(0.5,0.075,'Fs = NaN Hz','HorizontalAlignment', 'center');
    else
        text(0.5,0.075,['Fs = ' num2str(round(1/Ts)) ' Hz'],'HorizontalAlignment', 'center');
    end
end

clear i numberOfPorts outNames inNames