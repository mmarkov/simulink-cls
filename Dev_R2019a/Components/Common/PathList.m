classdef PathList < handle
	properties
		array
	end
	
	methods
		%Usage PATH = PathList()
		function list = PathList()
            list.array = {'currentBlock' 'Path'};
		end
		
		%Usage Path.get(gcb)
		function path = get(list,currentBlock)
            currentLibData =  libinfo(currentBlock,'SearchDepth',0);
            if isempty(currentLibData)
                % We are in the library itself
                while ~strcmp(get_param(currentBlock,'Referenceblock'),'')
                    currentBlock = get_param(currentBlock,'Referenceblock');
                end
                origBlockName = get_param(currentBlock,'Name');
            else
                % We are outside the library
                origBlockName = strsplit(currentLibData.ReferenceBlock,'/');
                origBlockName = origBlockName{end};
            end
            
			[n, ~] = size(list.array);
			
			%Get path
			path = 'null';
			for i=1:n
				if(strcmp(list.array(i,1),origBlockName))
					path = list.array(i,2);
					break;
				end
			end
			
			if ~strcmp(path, 'null')
				path = path{1};
			end
		end
	
		%Usage Path.add(gcb)
		function add(list, currentBlock)
			currentLibData =  libinfo(currentBlock,'SearchDepth',0);
            if isempty(currentLibData)
                % We are in the library itself
                while ~strcmp(get_param(currentBlock,'Referenceblock'),'')
                    currentBlock = get_param(currentBlock,'Referenceblock');
                end
                origBlockName = get_param(currentBlock,'Name');
                currentLib = currentBlock(1:find(currentBlock == '/')-1);
            else
                % We are outside the library
                origBlockName = strsplit(currentLibData.ReferenceBlock,'/');
                origBlockName = origBlockName{end};
                currentLib = currentLibData.Library;
            end

			
			[n, ~] = size(list.array);
			%calculate path (using origin library/system of the currentBlock) and get name
			library = [currentLib '.slx'];
			path = which(library);
			path = path(1:end-(length(library)+1));
			
			%check if there is already an entry with the same name
			idx = n+1;
			for i=1:n
				if strcmp(list.array(i,1), origBlockName)
					idx = i;
					break;
				end
			end
			
			%add path and name to list (or replace)
			list.array(idx,1) = {origBlockName};
			list.array(idx,2) = {path};
		end
		
		%Usage Path.show()
		function show(list)
			list.array
		end
	end
end

