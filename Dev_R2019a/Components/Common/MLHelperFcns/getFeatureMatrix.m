function varargout = getFeatureMatrix(rawDataMatrix, features,  thrZC, sscThresh, windowLength, interOffset, samplesPerTrial)
%getFeatureMatrix calculates the given features of a raw data matrix,
%rawDataMatrix   : rows = channels, colums = time. CHANNELS HAVE TO BE ZERO MEAN
%features:       : cell array with feature names {'RMS', 'ZC',...}
%                : choose from:
%                   RMS: root mean square: the power (variance) of the signal
%                   MAV: mean absolute value: mean of rectified signal in
%                        that window
%                   ENV: Envelop of the signal, 3Hz filtered
%                logVAR: logarithm of the variance
%                   FPB: Fokker-Planck Bayes Filter, by David Hoffmann, based on work by Sanger/Clancy
%                   ZC:  the number of times the signal crosses the zero
%                        line. Thus a simple measure of lower-frequency
%                        content
%                   SSC: The number of times the slope sign changes
%                        (rise-fall or fall-rise transition) - number of turning
%                        points. Tus a simple measure of higher-frequency
%                        content
%                   WL:  Wave length - imagine to "pull" the signal on both
%                        ends like a rope until its flat - WL is the
%                        resulting length of the "rope".
%                        Thus a simple combined measure of frequency and
%                        amplitude
%windowLength    : number of samples to calculate RMS over
%interOffset     : number of samples window is shifted foward
%samplesPerTrial : number of samples per single move, to avoid over-shoot
%                  of windows into next movement. To disable, set to inf

if isempty(rawDataMatrix)
    featMatrix = [];
    varargout{1} = featMatrix;
    return;
end

[nChannels, nSamples] = size(rawDataMatrix);

if isempty(features)
    % The user might choose to train using the raw data
    nFeatures = 1;
    nWindowsPerTrial = samplesPerTrial;
    nTrials = nSamples/samplesPerTrial;
    featMatrix = zeros(nChannels*nFeatures, nWindowsPerTrial*nTrials);
    featMatrix(:,:) = rawDataMatrix;
else
   % The user has selected some features 
    nFeatures = length(features);
    nWindowsPerTrial = floor((samplesPerTrial-windowLength)/interOffset) + 1;
    
    if mod(nSamples, samplesPerTrial) ~= 0 && samplesPerTrial<inf
        error ('Total number of samples is not a multiple of samplesPerTrial');
    end
    nTrials = nSamples/samplesPerTrial;
    if samplesPerTrial< inf
        featMatrix = zeros(nChannels*nFeatures, nWindowsPerTrial*nTrials); %preallocate memory for speed up
    else
        featMatrix = []; %we dont know the size in advance... doesnt matter but slower.
    end
    
    [b, a] = butter(4, 3/500, 'low'); %3rd order, 1000Hz sampl.freuq*0.003 = 3Hz cut off
    
    %Parameters for Bayes feature FPB
    params.alpha=10^-30;
    params.beta =10^-30;
    params.dt = 1/1000;
    params.MVCstd = 10000;
    params.type = 'gauss';
    nbins = 200;
    prior = ones(nbins,nChannels) / nbins; %100 bins for statistics (histogram)
    FPBwin = zeros(1,windowLength);
    FPBMethod = 2; %1: average over all samples in Window (more accurate, slower)
    %2: simply take first value of Window (less accurate, faster)
    
    for channel = 1:nChannels
        file = 1;
        window = 0;
        startOfWindow = -interOffset + 1;
        
        
        if strcmp(features{1}, 'Envelope')
            featMatrix(:,1) = zeros(8,1);
            for iSamp=2:length(rawDataMatrix)
                featMatrix(channel,iSamp) = featMatrix(channel,iSamp-1)*0.995 + abs(rawDataMatrix(channel, iSamp))*0.005;
            end
        end
        
        while (startOfWindow + interOffset + windowLength-1) <= nSamples   %make sure that windows do not overlap
            startOfWindow = startOfWindow + interOffset;                   %between 2 moves, because that would
            if (startOfWindow + windowLength-1  ) > (samplesPerTrial*file) %mix up the signals
                startOfWindow = samplesPerTrial*file+1;                    %so if one file would be exceeded, start
                file = file + 1;                                           %new with the next move file
            end                                                            %
            window = window + 1;
            ZC = 0; SSC = 0; WL = 0; %reset counters for features for new window
            
            currSamples = rawDataMatrix(channel, startOfWindow:startOfWindow+windowLength-1); %temp array with the current window data
            
            for iFeat = 1:nFeatures
                idx = (channel-1)*nFeatures + iFeat;
                switch features{iFeat}
                    case 'RMS' %root - mean - square (~variance)
                        featMatrix(idx, window) = sqrt( (sum(currSamples.^2))/windowLength );
                    case 'MAV' %mean absolute value
                        featMatrix(idx, window) = mean(abs(currSamples));
                    case 'ENV' %envelope of signal
                        featMatrix(idx, window) = mean(filter(b,a,(abs(currSamples))'))'; %filter works along 1st dimension
                    case 'logVAR' %logarithm of variance
                        featMatrix(idx, window) = log( (sum(currSamples.^2))/windowLength );
                    case 'FPB'
                        if FPBMethod == 1
                            for ii=1:windowLength
                                [FPBwin(ii), prior(:,channel)] = bayesSolver(abs(currSamples(ii)), prior(:, channel), params);
                            end
                            featMatrix(idx, window) = mean(FPBwin);
                        else
                            [featMatrix(idx, window), prior(:,channel)] = bayesSolver(abs(currSamples(1)), prior(:, channel), params);
                        end
                    case 'ZC'  % number of zero crossings
                        for i=1:windowLength-1
                            if (currSamples(i) * currSamples(i+1) < 0 && abs(currSamples(i) - currSamples(i+1)) > thrZC)
                                ZC = ZC +1;
                            end
                        end
                        featMatrix(idx, window) = ZC/windowLength;
                    case 'SSC'  % number of Slope Sign Changes
                        for i=2:windowLength-1
                            if (currSamples(i) - currSamples(i-1)) * (currSamples(i+1) - currSamples(i)) < 0 && abs(currSamples(i+1) - currSamples(i)) > sscThresh
                                SSC = SSC + 1;
                            end
                        end
                        featMatrix(idx, window) = SSC/windowLength;
                    case 'WL'  %Wave (Form) Length
                        for i=1:windowLength-1
                            WL = WL + abs(currSamples(i) - currSamples(i+1));
                        end
                        featMatrix(idx, window) = WL/windowLength;
                    case 'Envelope'
                        %handled already
                    otherwise
                        error (['unknown feature ', features{iFeat}]);
                end %end switch-case
            end  %end for features
        end %end while windows for channel
        nWindowsPerTrial = window;
    end %end for channels
    
end

varargout{1} = featMatrix;
if nargout == 2
    varargout{2} = nWindowsPerTrial/nTrials;
end
if nargout > 2
    varargout{2} = nWindowsPerTrial/nTrials;
    varargout{3} = nChannels;
    varargout{4} = nFeatures;
end

end %end main function


