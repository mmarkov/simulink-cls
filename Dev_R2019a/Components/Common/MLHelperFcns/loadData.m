function [folderPath, trainingFileStr, rawDataMatrixTrain, rawDataTargetsTrain, classesOfInterest, trainClasses, rawDataFromTo, nChannelsPre, nTrialsTrain, waitH] = loadData(algoType, artefactRemovalChannel)
rawDataMatrixTrain = []; % will be channels x samples, including all classes
trainClasses = [];
rawDataFromTo = [];
waitVal = 0;
fSep = filesep;
%% User input
oldFolder = get_param(gcb,'trainingFolder');
if ~exist(oldFolder,'dir')
    [fileName,folderPath] = uigetfile('*.mat', 'MultiSelect','on', [algoType ': Select calibration files']);
else
    [fileName,folderPath] = uigetfile([oldFolder,'*.mat'], 'MultiSelect','on', [algoType ': Select calibration files']);
end

trainingFileStr = '';
if ~iscell(fileName) && length(fileName) == 1
    rawDataMatrixTrain = []; % will be channels x samples, including all classes
    rawDataTargetsTrain = [];
    classesOfInterest =[];
    trainClasses = [];
    rawDataFromTo = [];
    nChannelsPre = [];
    nTrialsTrain = [];
    waitH = [];
    return;
else
    waitH = waitbar(waitVal, 'Loading files ...','Units', 'centimeters', ...
        'Position', [5 5 10 2], 'Visible', 'off', 'Color','white');
    set(waitH,'Units', 'normalized');
    currentBarPos = get(waitH, 'Position');
    set(waitH, 'Visible', 'on', ...
        'Position', [(1-currentBarPos(3))/2 0.95-currentBarPos(4) currentBarPos(3) currentBarPos(4)]);
    
    if iscell(fileName)
        numOfFiles = numel(fileName);
        for i=1:numOfFiles
            [Levels,rawDataFromToTemp] = formatDataFromCLSForTraining(fileName{i},folderPath);
            trainingFileStr = [trainingFileStr,'\n',fileName{i}];
            if isempty(rawDataFromTo)
                rawDataFromTo = rawDataFromToTemp;
            elseif numel(rawDataFromToTemp) ~= numel(rawDataFromTo)
                warning('The training data length is of unequal size. It will be truncated to the smallest size!')
                rawDataFromTo = 1: min(numel(rawDataFromTo),numel(rawDataFromToTemp));
            end
        end
    else
        [Levels,rawDataFromTo] = formatDataFromCLSForTraining(fileName,folderPath);
        trainingFileStr = fileName;
    end
end

folderPathDAQ = [folderPath(1:end-1) fSep 'DATA' fSep 'DAQ'];
folderDirs = dir(folderPathDAQ);
folderNames =cell(1, length(folderDirs)-2);
j = 0;
for i = 3:length(folderDirs)
    j = j + 1;
    folderNames{j} = folderDirs(i).name;
end
if iscell(fileName)
    folderNames=regexprep(fileName,'.mat','');
    numReps = length(folderNames);
else
    numReps=1;
    folderNames={regexprep(fileName,'.mat','')};
end
nTrialsTrain = Levels*numReps;
waitVal = waitVal + 0.1;
waitbar(waitVal, waitH, [algoType ': Processing files ...']);
%% Load the data
temptrainClasses = cell(1, numReps);
temptrainArmPos = cell(1, numReps);
for i=1:numReps
    trainPath = [folderPathDAQ fSep folderNames{i} fSep];
    [~, ~, temptrainClasses{i}, temptrainArmPos{i}] = readFilesToCell(trainPath, '.mat', false);
end
waitVal = waitVal + 0.1;
waitbar(waitVal, waitH, [algoType ': Waiting for user input ...'],'Color','cyan');
[classesOfInterest, armPosOfInterest] = MlClassSelectGUI(folderNames, temptrainClasses, temptrainArmPos); % from here the classes start at idx 1
waitbar(waitVal, waitH, [algoType ': Processing files ...'], 'Color', 'white');
nTrainFiles= 0;
RawDataCellTrain = {};
trainClasses = [];
for i=1:numReps
    trainPath = [folderPathDAQ fSep folderNames{i} fSep];
    [tempRawDataCellTrain, ~, temptrainClasses, temptrainArmPos] = readFilesToCell(trainPath, '.mat', false);
    classesOfInterest{i} = unique(classesOfInterest{i});
    for  j = 1:numel(classesOfInterest{i})
        motion = classesOfInterest{i}(j);
        if (motion)
            for k = 1:numel(armPosOfInterest{i})
                armPos = armPosOfInterest{i}(k);
                if (armPos)
                    indtemp = find(temptrainClasses == (motion - 1) & temptrainArmPos == (armPos - 1));
                    RawDataCellTrain = [RawDataCellTrain; tempRawDataCellTrain(indtemp)];
                    trainClasses = [trainClasses temptrainClasses(indtemp)+1]; %idx starts from 1
                    nTrainFiles = nTrainFiles + length(indtemp);
                end
            end
        end
    end
end
classesOfInterest = cell2mat(classesOfInterest);
classesOfInterest = sort(unique(classesOfInterest(:)))';
%% Resort
% % trainClassesOrdered=[];
% % RawDataCellTrainOrdered=[];
% % for j = 1:nTrainFiles/Levels
% %     for i = 1:1
% %         for k=1:Levels
% %             order=(i-1)*nTrainFiles+(j-1)*Levels+k;
% %             trainClassesOrdered=[trainClassesOrdered, trainClasses(order)];
% %             RawDataCellTrainOrdered=[RawDataCellTrainOrdered; RawDataCellTrain(order)];
% %         end
% %     end
% % end

[trainClasses, idxSort] = sort(trainClasses);
RawDataCellTrain = RawDataCellTrain(idxSort,:);

%% Generate rawDataMatrices containing only the raw data of interest for the train set
useFiles=[];
for file = 1:nTrainFiles
    if ~any(classesOfInterest == trainClasses(file))
        trainClasses(file)=NaN;
    else
        useFiles=[useFiles,file];
    end
end

rawDataTargetsTrainC =  cell(100,1);
for file = useFiles
    rawDataMatrixTrain = [rawDataMatrixTrain, RawDataCellTrain{file}.outdata(:,rawDataFromTo)];
    rawDataTargetsTrainC{trainClasses(file),1} = [rawDataTargetsTrainC{trainClasses(file),1}, RawDataCellTrain{file}.reference(rawDataFromTo)'];
end
rawDataTargetsTrain = cell2mat(rawDataTargetsTrainC);


%% Artefact removal channel
nChannelsPre=size(rawDataMatrixTrain,1);
if isempty(artefactRemovalChannel)
    artefactRemovalChannel=0;
end
if (all(artefactRemovalChannel >= 1)) && (all(artefactRemovalChannel <= nChannelsPre))
    rawDataMatrixTrain(artefactRemovalChannel,:)=[];
end

waitVal = waitVal + 0.2;
waitbar(waitVal, waitH, [algoType ': Training ...']);
end

%% Subfunctions
function [classesOfInterest, armPosOfInterest] = MlClassSelectGUI (folderNames, inClasses, inArmPos)

noOfFiles = numel(folderNames);
noOfClasses = max(cellfun(@max,inClasses))+1;
noOfArmPos = max(cellfun(@max,inArmPos))+1;
classesOfInterest = cell(1, noOfFiles);
armPosOfInterest = cell(1, noOfFiles);

fontS = 11;

cW = 2.5;
cH = 0.8;

pW = (cW + 2);
pH = (cH + 0.1) * (noOfClasses + 1);

bW = pW;
bH = 1;

tW = pW;
tH = bH;

fH = pH+tH+bH+1;
fW = (pW+0.5)*noOfFiles*noOfArmPos+0.5;

temp.closeReq = false;
temp.classSelection = [];
figureH = figure('Visible','off', 'Units', 'centimeters', ...
    'Position', [5 5 fW fH],'ToolBar', 'none', 'Menubar', 'none', ...
    'CloseRequestFcn', '','UserData', temp, 'Color', 'w', ...
    'Name', 'Class selection');
set(figureH,'Units', 'normalized');
currentFigPos = get(figureH, 'Position');
set(figureH, 'Position', [(1-currentFigPos(3))/2  0.8-currentFigPos(4) currentFigPos(3) currentFigPos(4)]);

chkBoxH = zeros(noOfClasses, noOfFiles*noOfArmPos);
panelH = zeros(1,noOfFiles*noOfArmPos);

for i = 1:noOfFiles
    noOfFileChar = min(length(folderNames{i}),7);
    for j = 1:noOfArmPos
        idxTemp = (i-1)*noOfArmPos + j;
        panelH(idxTemp) = uipanel('parent', figureH, 'Units', 'centimeters', ...
        'Title', ['...' folderNames{i}(end-noOfFileChar+1:end) '::ArmPos ' num2str(j-1)], 'BackgroundColor','w', ...
        'Position', [(idxTemp-1)*pW+0.5*i fH-pH-0.25 pW pH], 'FontSize', fontS-1);
        for k = 1:noOfClasses
            chkBoxH(k, idxTemp) = uicontrol('parent', panelH(idxTemp), 'Style', 'checkbox', ...
                'BackgroundColor','w', 'String', ['Class ' num2str(k-1)], ...
                'Units', 'centimeters', 'Position' , ...
                [((pW-cW)/2) pH-((k+1)*cH + k*0.1) cW cH], 'FontSize', fontS);
            if any((k == inClasses{i}+1) & (j == inArmPos{i}+1))
                set(chkBoxH(k,idxTemp),'Value', 1);
                set(chkBoxH(k,idxTemp),'Enable', 'on');
            else
                set(chkBoxH(k,idxTemp),'Value', 0);
                set(chkBoxH(k,idxTemp),'Enable', 'off');
            end
        end
    end
end

textBoxH = uicontrol('Parent', figureH, 'Style', 'text', ...
    'BackgroundColor','w', 'Units', 'centimeters', 'String' ,'Select active classes', ...
    'Position', [(fW-bW)/2 0.25 tW tH], 'FontSize', fontS);

uicontrol('Parent', figureH, 'Style', 'pushbutton', ...
    'BackgroundColor','w', 'Units', 'centimeters', 'String' ,'Confirm sel.', ...
    'Position', [(fW-bW)/2 tH+0.5 bW bH], 'FontSize', fontS, ...
    'Callback', {@OKbuttonClikCallback, chkBoxH, textBoxH});

set(figureH, 'Visible', 'on');

while true
    pause(0.3);
    temp = get(figureH, 'UserData');
    if (temp.closeReq)
        classSelection = temp.classSelection;
        for i = 1 : noOfFiles
            classesOfInterest{i} = false(size(inClasses{i}));
            armPosOfInterest{i} = zeros(1, noOfArmPos);
            for j = 1:length(armPosOfInterest{i})
                if any(classSelection(:,(i-1)*noOfArmPos+j))
                    armPosOfInterest{i}(j) = j;
                else
                    armPosOfInterest{i}(j) = 0;
                end
            end
            for j = 1:length(inClasses{i})
                if classSelection(inClasses{i}(j)+1,(i-1)*noOfArmPos+inArmPos{i}(j)+1)
                    classesOfInterest{i}(j) = true;
                else
                    classesOfInterest{i}(j) = false;
                end
            end
            classesOfInterest{i} = inClasses{i}(classesOfInterest{i})+1;
        end
        close(figureH);
        delete(figureH);
        break;
    end
end

end


function OKbuttonClikCallback(src, ~, chkBoxH, textBoxH)

fH = get(src, 'Parent');
classSelection = arrayfun(@(x)get(x,'Value'), chkBoxH);
selectionSum = sum(classSelection, 2);
selectionSum(selectionSum == 0) = [];

if ~isempty(selectionSum) %&& all(diff(selectionSum) == 0)
    temp = get(fH, 'UserData');
    temp.closeReq = true;
    temp.classSelection = classSelection;
    set(fH,'UserData', temp);
else
    if isempty(selectionSum)
        set(textBoxH, 'String', 'Nothing selected!');
    else
        set(textBoxH, 'String', 'Number of samples must match!');
    end
end
end

function [dataCell, numberOfFilesRead, classes, armPos] = readFilesToCell(folderPath, fileExtension, varargin)
%readFilesToCell [dataCell, numberOfFilesRead] = readFilesToCell(folderPath, fileExtension, verbose)
%reads all data of given file extension within a folder from a given folder folderPath to a cell of data
%sample call:
%folderPath = 'C:\MYDATA\20120713_111449\'; => dont forget the last '\'!
%rawData = readFilesToCell(folderPath, '.mat');
%rawData = readFilesToCell(folderPath, '.mat', true/false); also specifies if
%the filename of the currently read files are displayed
verbose = false;

filesToProcess = 0;
classes = []; %will be [1 1 1 1 2 2 2 2 3 3 3 3....]
armPos = [];
%get the number of files of interest, to create a cell of the correct
%length. Might be slow and stupid to do this loop twice, but no better idea
filelisting = dir (folderPath); %get files of directory
for f=1:length(filelisting) %iterate through files
    filename = strcat(folderPath,filelisting(f).name); %get full filename
    if strfind(filename, fileExtension) %is it a file of the desired format?
        filesToProcess = filesToProcess+1;
    end
end

%create a cell array for the data
dataCell = cell(filesToProcess, 1);

%iterate through filelisting again, this time loading the data to the
%dataCell
numberOfFilesRead = 0;

for f=1:length(filelisting) %iterate through files
    filename = strcat(folderPath,filelisting(f).name); %get full filename
    if strfind(filename, fileExtension) %is it a file of the desired format?
        tempcell = load(filename);    %load data to data cell
        if ~isfield(tempcell, 'nfile') || strcmp(getClassIdentifier(tempcell.nfile), '') %only proceed if file is of interest
            continue;
        end
        if verbose
            disp(strcat('file being read in: ', filelisting(f).name))
        end
        numberOfFilesRead = numberOfFilesRead + 1; %remember number of files
        dataCell{numberOfFilesRead} = tempcell;    %load data to data cell
        %
        
        %             if numberOfFilesRead == 1 %in first iteration, initialize the classIdentifier and the class counter
        %                 classIdentifier = getClassIdentifier(dataCell{1}.nfile); %get C001, C010, ...
        %                 classNumber = 1; %initial value for first class
        %             end
        %             if ~strcmp(classIdentifier, getClassIdentifier(dataCell{numberOfFilesRead}.nfile)) %if change in identifier
        %                 classNumber = classNumber+1; %set new class
        %             end
        classIdentifier = getClassIdentifier(dataCell{numberOfFilesRead}.nfile); %remember current class
        classNumber = str2double(classIdentifier(end-1:end));
        armPosIdentifier = getArmPosIdentifier(dataCell{numberOfFilesRead}.nfile);
        armPosNumber = str2double(armPosIdentifier(end-1:end));
        classes(1,numberOfFilesRead) = classNumber; %add current read file class to classes vector
        armPos(1,numberOfFilesRead) = armPosNumber;
    end
end
end


%subfunction to get the classIdentifiers (C001, C010, C011, C012...)
%from the file name
function classIdentifier = getClassIdentifier(fileName)
classIdentifier = ''; %identifier has to be assigned, also if if-statement is not entered
Cstart = strfind(fileName, 'C0'); %find the location of the 'C0xx' string in the filename, identifying the class
if ~isempty(Cstart) %if it is found
    classIdentifier = fileName(Cstart(length(Cstart)): Cstart(length(Cstart))+3); %read from that position + 3chars
else
    classIdentifier = '';
end
end

%subfunction to get the armPosidentifier (A001, A010, A011, A012...)
%from the file name
function ArmPosIdentifier = getArmPosIdentifier(fileName)
ArmPosIdentifier = ''; %identifier has to be assigned, also if if-statement is not entered
Cstart = strfind(fileName, 'A0'); %find the location of the 'C0xx' string in the filename, identifying the armPos
if ~isempty(Cstart) %if it is found
    ArmPosIdentifier = fileName(Cstart(length(Cstart)): Cstart(length(Cstart))+3); %read from that position + 3chars
else
    ArmPosIdentifier = '';
end
end

function [Levels,rawDataFromTo] = formatDataFromCLSForTraining(fileName,folderPath)
fSep = filesep;
load([folderPath,fileName])
dataFolder=[folderPath  'DATA' fSep 'DAQ' fSep regexprep(fileName,'.mat','')];
try
    rmdir([folderPath  'DATA' fSep 'DAQ' fSep regexprep(fileName,'.mat','')], 's'); % remove the root folder
end
mkdir(dataFolder)
%%
classTrace=squeeze(simoutMovementID.signals.values);
cueTrace=squeeze(simoutTrainProfile.signals.values);
cueTrace = round(cueTrace*1000)/1000; %prevent wierd round off errors
% emgTrace=squeeze(simoutEMG.signals.values);
emgTrace = reshape(simoutEMG.signals.values, size(simoutEMG.signals.values,1)*size(simoutEMG.signals.values,2), size(simoutEMG.signals.values,3));
if exist('simoutArmPosID','var')
    armPosTrace = squeeze(simoutArmPosID.signals.values);
else
    armPosTrace = zeros(size(classTrace));
end
pauseTimepoints=find(cueTrace==-1);
classTrace(pauseTimepoints)=[];
cueTrace(pauseTimepoints)=[];
emgTrace(:,pauseTimepoints)=[];
armPosTrace(pauseTimepoints)=[];

%% get only plateaus
% endPoints=intersect(find(diff(cueTrace)<0)+1,find(cueTrace(1:end-1)==0));
endPoints=intersect(find(diff(cueTrace)<0)+1,find(cueTrace(1:end)==0));
endPointsPre=[1;endPoints(1:end-1)+1];
for i=1:size(endPoints)
    interval{i}=cueTrace(endPointsPre(i):endPoints(i));
    maxi(i)=max(interval{i});
    timingPeak{i}=intersect(find(cueTrace==maxi(i)), endPointsPre(i):endPoints(i));%interval{i}(1)
    cueTracePeak{i}=cueTrace(timingPeak{i});
    classTracePeak{i} = classTrace(timingPeak{i});
    emgTracePeak{:,i} = emgTrace(:,timingPeak{i});
    armPosTracePeak{i} = armPosTrace(timingPeak{i});
    motion(i) = unique(classTracePeak{i});
    armPos(i) = unique(armPosTracePeak{i});
end
rawDataFromTo=1:size(timingPeak{1},1);

levelVals=unique(maxi);
Levels=size(levelVals,2);
for i=1:size(endPoints)
    motionVal = motion(i);
    armPosVal = armPos(i);
    if motionVal<10
        labC = ['C00' num2str(motionVal)];
    else
        labC = ['C0' num2str(motionVal)];
    end
    if armPosVal<9
        labA = ['A00' num2str(armPosVal)];
    else
        labA = ['A0' num2str(armPosVal)];
    end
    clear outdata reference nfile
    outdata=emgTracePeak{:,i};
    nfile=[labA,'_',labC,'_',num2str(maxi(i)*100),'_label',num2str(i),regexprep(fileName,'.mat','')];
    reference=cueTracePeak{i};
    save([dataFolder,fSep,nfile],'nfile','outdata','reference');
    
end

end