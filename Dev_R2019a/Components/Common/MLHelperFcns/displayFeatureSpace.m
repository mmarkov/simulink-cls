function varargout = displayFeatureSpace(featureMatrix, classes, optionsPlot)
%displayFeatureSpace(featureMatrix, numClasses, options)
%calculates and shows point clouds of feature matrix
% exampleCall: displayFeatureSpace(TDFMatrixTrain, 8, options); %in
% TDFMatrixTrain there are 8 classes equally distributed among the feature
% vectors.
%
%input: - feature matrix (features x observations)
%       - classes: number of different classes in feature matrix,
%           assuming there are equally many observations per class
%           OR
%           a vector containing the class labels for each data point
%
%       - options:
%           - options.Method: choose from 'PCA', 'LDA', 'LPP'
%           - options.LPPoptions: options that will be forwarded to LPP
%           - options.Dim: 2 for 2D or 3 for 3D display
%           - options.Colors: color for each class. length must be same as
%                             length(unique(classes))
%           - options.NewFig: if 1 a new figure will be
%             created, if 0 the last active figure is overwritten
%
%output: - [coeff] = displayFeatureSpace... outputs the transformation
%                                           coeffitients
%        - [coeff, h] = displayFeatureSpace... additionally outputs the
%                                              handle to the used figure
%
%       trainData = [rand(10,50), rand(10,50)+2];
%       testData = rand(10,20)+2;
%       classes = [ones(50,1); ones(50,1)*2];
%       options = [];
%       options.Method = 'PCA';
%       options.Dim = 3;
%       options.Coeffs = displayFeatureSpace(trainData, classes, options);
%       options.NewFig = 0;
%       options.Colors = {[0.3 0.2 0.2], [1,0,0], 'k']}; for up to 3 classes
%       displayFeatureSpace(testData, 1, options);
%       axis equal

if  nargin < 2 || isempty(featureMatrix)
    display ('error in calling displayFeatureSpace');
    help displayFeatureSpace;
    return;
end

if (~exist('optionsPlot','var'))
    optionsPlot = [];
end

if ~isfield(optionsPlot,'Method')
    optionsPlot.Method = 'PCA';
end

if ~isfield(optionsPlot,'Dim')
    optionsPlot.Dim = 3;
end

if ~isfield(optionsPlot,'NewFig')
    optionsPlot.NewFig = 1;
end

if ~isfield(optionsPlot,'LPPoptions')
    optionsPlot.LPPoptions = [];
    optionsPlot.LPPoptions.NeighborMode = 'Supervised';
    optionsPlot.LPPoptions.n = 1;
    optionsPlot.LPPoptions.gnd = classes;
end

if isfield(optionsPlot, 'Center') && optionsPlot.Center == 1
    featureMatrix = bsxfun(@minus, featureMatrix', mean(featureMatrix,2)')'; %center
end

if ~isfield(optionsPlot,'Coeffs')
    switch optionsPlot.Method
        case 'PCA'
            %[coeff, ~] = princomp(featureMatrix'); %version with Statistics Toolbox
            
            %manual PCA
            [coeff, ~] = svd(cov(featureMatrix')); %version without Statistics Toolbox
        case 'LDA'
            [coeff,~] = LDA(classes,[], featureMatrix'); %required from http://www.cad.zju.edu.cn/home/dengcai/Data/code.zip
        case 'LPP'
            
            W = constructW(featureMatrix', optionsPlot.LPPoptions);
            [coeff,~] = LPP(W, [], featureMatrix'); %required from http://www.cad.zju.edu.cn/home/dengcai/Data/code.zip
    end
else
    coeff = optionsPlot.Coeffs;
end

projection = coeff' * featureMatrix;
nFeatureSpacePoints = size(projection,2);

if optionsPlot.NewFig
    figure('Color', 'w','Units', 'normalized', 'Position', [0.3 0.2 0.3 0.4]);
end

if isempty(classes)
    colorDisplayClasses = 0;
    classes = 1;
else
    colorDisplayClasses = 1;
end
if isscalar(classes)
    cs = repmat(1:classes, nFeatureSpacePoints/classes,1); %classes
    cs = cs(:);
else
    cs = classes;
end

hold on;
if ~isfield(optionsPlot,'Colors')
    colorsPlot = {[0 0 1],[1 0 0.05], [0.55 0, 1], [1 0.55 0], [0 0.39 0], [1 0.35 0.64], [0.47 0.53 0.6], [0.67 0.47 0.31], 'c', 'k',};
else
    colorsPlot = optionsPlot.Colors;
end
for iClass=1:max(cs)
    if optionsPlot.Dim == 2
        h = plot (projection(1,cs==iClass), projection(2,cs==iClass));
    else
        h = plot3 (projection(1,cs==iClass), projection(2,cs==iClass), projection(3,cs==iClass));
    end
    if iClass<=length(colorsPlot) && colorDisplayClasses
        color = colorsPlot{iClass};
    else
        color = 'k';
    end
    set (h, 'Color', color, 'Marker', '*', 'LineStyle','none');
end
grid on;
hold off;

if isfield(optionsPlot,'MotionLabels')
    legend(optionsPlot.MotionLabels, 'FontSize', 11);
    legend('boxoff');
end
varargout{1} = coeff;
if nargout>1
    varargout{2} = h;
end

if isfield(optionsPlot,'FigTitle')
    strTitle = optionsPlot.FigTitle;
    strTitle = strrep(strTitle, '\n',' ');
    strTitle = strrep(strTitle, '_','');
    title(strTitle);
end
end


