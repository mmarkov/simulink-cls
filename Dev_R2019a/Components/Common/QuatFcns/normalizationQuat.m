function qout = normalizationQuat( q )

%Calculation of a normalized quaternion for an input quaternion.

qout = q./(modQuat( q )* ones(1,4));
