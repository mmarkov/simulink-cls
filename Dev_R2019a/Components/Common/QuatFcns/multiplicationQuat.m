function qOut = multiplicationQuat( q, varargin )
%
% quatMultiplication multiplicates any number of quaternions.
%
% Quaternion multiplication is not commutative!!
%
% -> q1 * q2 != q2 * q1
%
% A quaternion (q = a + bi + cj + dk) consist of a scalar part (a) and a vector part
% (bi + cj + dk). All quaternions have to be fed into this function in the
% form (M x [a, b, c, d]). For more infromation about quaternion multiplication,
% please consult "Quaternion Computation" by Neil Dantam (2014).
%
%   input:  
%               q           first quaternion (M-by-4)
%               varargin    variable number of quaternions (M-by-4) each
%
%   output:
%               qOut        product of input quaternions, following the
%                           rules for quaternion multiplication
%
%
%   Examples:
%            
%   Product of to 1-by-4 quaternions:
%       q1 = [1, 0, 1, 0];
%       q2 = [0.5, 0.75, 0.75, 0.5];
%       qOut = quatMultiplication(q1, q2)  
%
%       (q1 * q2 = qOut)
%
%
%   Product of a 1-by-4 quaternion by itself
%       q1 = [1, 0, 1, 0];
%       qOut = quatMultiplication(q1) 
%
%       (q1 * q1 = qOut)
%
%
%   Product of three 1-by-4 quaternions
%       q1 = [1, 0, 1, 0];
%       q2 = [0.5, 0.75, 0.75, 0.5];
%       q2 = [1, 0.3, 0.75, 0.8];
%       qOut = quatMultiplication(q1, q2, q3) 
%
%       (q1 * q2 * q3 = qOut)
%
%
%   Product of two 1-by-4 quaternions and a 3-by-4 quaternion
%       q1 = [1, 0, 1, 0];
%       q2 = [1, 0.3, 0.75, 0.8; 1, 0.75, 0.3, 0.2; 2, 1, 0.1, 0.1];
%       q3 = [0.5, 0.75, 0.75, 0.5];
%       qOut = quatMultiplication(q1, q2, q3) 
%
%       (q1 * q2(1,:) *q3 = qOut(1,:)
%        q1 * q2(2,:) *q3 = qOut(2,:)
%        q1 * q2(3,:) *q3 = qOut(3,:))
%               
% See also quatNormalize, quatConjugate, quatInverse, quatToEuler

qOut = zeros(size(q), 'like', q);


if isempty(varargin)
    
    qOut = quatSubMulti( q, q);
    
else
    for i = 1:length(varargin)
        qOut = quatSubMulti( q, varargin{i});
        q = qOut;
    end
    
end

end