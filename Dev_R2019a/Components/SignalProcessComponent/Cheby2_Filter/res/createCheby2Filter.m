function Hd = createCheby2Filter(N, Fs, Fcutoff, ftype, Astop)
%UNTITLED Returns a discrete-time filter object.

% MATLAB Code
% Generated by MATLAB(R) 8.1 and the Signal Processing Toolbox 6.19.
% Generated on: 18-Nov-2014 12:31:24

% Chebyshev Type 2 filter designed using FDESIGN.

% All frequency values are in Hz.

% Construct an FDESIGN object and call its CHEBY2 method.
switch ftype
    case 1
        h  = fdesign.lowpass('N,F3dB,Ast', N, Fcutoff, Astop, Fs);
    case 2
        h  = fdesign.highpass('N,F3dB,Ast', N, Fcutoff, Astop, Fs);
    otherwise 
        error('Unsuported filter type');
end
Hd = design(h, 'cheby2');
end

% [EOF]
