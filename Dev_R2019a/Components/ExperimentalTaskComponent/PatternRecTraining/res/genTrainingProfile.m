% Calculate the training profile
trialDur = profileRdur + sum(profileSdur) + sum(profilePdur);

baseProfileTime = [0 profilePdur(1) profilePdur(1)+profileSdur(1) ...
    profilePdur(1)+profileSdur(1)+profileRdur ...
    profilePdur(1)+profileSdur(1)+profileRdur+profileSdur(2) ...
    profilePdur(1)+profileSdur(1)+profileRdur+profileSdur(2)+profilePdur(2)-Ts];

noOfBreakP = numel(baseProfileTime);

i = 1;
while true
    if (i > noOfBreakP  - 1)
        break
    end
    
    if (baseProfileTime(i) == baseProfileTime(i+1))
        baseProfileTime(i+1:end) = baseProfileTime(i+1:end) + Ts;
        i = 1;
    end
    i = i+1;
end

timeVector = [];
profileVector = [];
classVector = [];
armPosVector = [];
totNumOfReps = numel(armPosOrder)*classRep*numel(classOrder)*numel(profileLvl);
armOrderVec = reshape(repmat(armPosOrder, numel(profileLvl)* classRep * numel(classOrder), 1), 1, totNumOfReps);
classOrderVec = repmat(reshape(repmat(classOrder, numel(profileLvl)* classRep, 1), 1, totNumOfReps/numel(armPosOrder)), 1, numel(armPosOrder));
lvlOrderVec = repmat(profileLvl, 1, classRep * numel(classOrder)*numel(armPosOrder));
if randON
    randVec = randperm(totNumOfReps);
    armOrderVec = armOrderVec(randVec);
    classOrderVec = classOrderVec(randVec);
    lvlOrderVec = lvlOrderVec(randVec);
end

for i = 1:totNumOfReps
    if classOrderVec(i)>= 0
        profileVector = [profileVector 0 0 lvlOrderVec(i) lvlOrderVec(i) 0 0];
        if isempty(timeVector)
            timeVector = baseProfileTime;
        else
            timeVector = [timeVector timeVector(end) + Ts + baseProfileTime];
        end
        classVector = [classVector repmat(classOrderVec(i), 1, noOfBreakP)];
        armPosVector = [armPosVector repmat(armOrderVec(i), 1, noOfBreakP)];
    end
    
end

set_param([gcb '/TrainingProfiles/Profile'], 'TimeValues', ['[' num2str(timeVector) ']']);
set_param([gcb '/TrainingProfiles/Class'], 'TimeValues', ['[' num2str(timeVector) ']']);
set_param([gcb '/TrainingProfiles/ArmPosition'], 'TimeValues', ['[' num2str(timeVector) ']']);
set_param([gcb '/TrainingProfiles/Profile'], 'OutValues', ['[' num2str(profileVector) ']']);
set_param([gcb '/TrainingProfiles/Class'], 'OutValues', ['[' num2str(classVector) ']']);
set_param([gcb '/TrainingProfiles/ArmPosition'], 'OutValues', ['[' num2str(armPosVector) ']']);

set_param([gcb '/ControlExecTime/ExecTime'], 'Value', num2str(timeVector(end)));

%clearvars -except PATH sysTs simTs simDur ctrlTs expTs inTs feedTs