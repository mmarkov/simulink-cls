# -*- coding: utf-8 -*-
"""
@author: Charlotte Burmeister
"""
import zmq
from msgpack import loads
import socket
import numpy as np
from argparse import ArgumentParser

class PupilRemote:
    '''
    Serves as a bridge between Simulink and the Pupil Labs Core eye-tracker.
    Listens on a subport to the broadcast from the eye-tracker and then sends data to Simulink.
    '''

    def __init__(self, pupil_ip, pupil_port, simulink_port):
        if not pupil_ip:
            self.ip = 'localhost'
        if not pupil_port:
            self.pupil_port = 50020
        if not simulink_port:
            self.simulink_port = 8821
        self.ip = pupil_ip
        self.pupil_port = pupil_port
        self.simulink_port = simulink_port

        self.ctx = zmq.Context()
        
    def connect_matlab(self):
        # Matlab Address for relaying info
        my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        my_socket.connect(('127.0.0.1', self.simulink_port)) # communication with simulink is always local
        return my_socket
        
    def open_socket(self):
        pupil_remote = zmq.Socket(self.ctx, zmq.REQ)
        pupil_remote.connect('tcp://{}:{}'.format(self.ip,self.pupil_port))
        return pupil_remote
        
    # receive data from pupil
    def subscribe_to_topic(self,pupil_remote, topic=''):
        # ask for the sub port
        pupil_remote.send_string('SUB_PORT')
        sub_port = pupil_remote.recv_string()
        
        # open a sub port to listen to pupil
        sub = self.ctx.socket(zmq.SUB)
        sub.connect("tcp://{}:{}".format(self.ip, sub_port)) 
    
        # set subscriptions to topics
        # recv just pupil/gaze notifications
        # sub.setsockopt_string(zmq.SUBSCRIBE, 'pupil.')
        # sub.setsockopt_string(zmq.SUBSCRIBE, 'gaze')
        # or everything:
        sub.setsockopt_string(zmq.SUBSCRIBE, topic)
        return sub
    
    def receive_send_data(self, sub):
        while True:
            try:
                frames  = sub.recv_multipart()
                if(len(frames) == 2):
                    topic, msg = frames
                elif(len(frames) == 3) :
                    topic,msg , _ = frames
                # parse msgpack
                msg = loads(msg, strict_map_key = False )
                # payload for pupil
                # payload = [eye_id, timestamp, confidence, pupil_diameter, None, None ]
                # payload for gaze
                # payload = [eye_id, timestamp, confidence, gaze_pos_x, gaze_pos_y, gaze_pos_z ]
                payload = np.zeros(6).astype('float64')
                
                # add arbritary number here, for testing reasons
                payload = np.add(payload,5)
                
                # check if topic is pupil (can be either zero=right eye or one=left eye)
                # also check if diameter is not zero, else we don't want to send pupil data, bc pupil not detected
                topic = topic.decode("utf-8")
                if (topic == "pupil.0.3d" or topic == "pupil.1.3d") and msg['diameter_3d'] != 0:
                    eye_id = np.array(float(topic[6]))
                    print(msg)
                    payload[0]=eye_id 
                    payload[1:4]=self.read_pupil_data(msg)
                    
                # gaze can be either gaze.3d.1 or gaze.3d.0 doesn't make a difference           
                elif topic == "gaze.3d.01." or topic == "gaze.3d.0.":
                    # -1 encodes gaze position
                    payload[0] = -1
                    payload[1:]=self.read_gaze_data(msg)
                    
                else:
                    continue
            
                print("\n Data to matlab {}, type {} \n".format(payload, payload.dtype))

                self.send_to_matlab(payload)
                              
            except KeyboardInterrupt:
                break
        
            
    def read_pupil_data(self, msg):
        pupil_diameter = np.array(msg["diameter_3d"]).astype('float64')
        confidence = np.array(msg["model_confidence"]).astype('float64')
        timestamp = np.array(msg["timestamp"]).astype('float64')
        return np.hstack((timestamp, confidence, pupil_diameter))
    
    
    def read_gaze_data(self, msg):
        timestamp = np.array(msg["timestamp"]).astype('float64')
        confidence = np.array(msg["confidence"]).astype('float64')
        gaze_point = np.array(msg["gaze_point_3d"]).astype('float64')
        return  np.hstack((timestamp, confidence, gaze_point))
    
    def send_to_matlab(self, msg):
        matlab_socket = self.connect_matlab()
        bytes_send = matlab_socket.send(msg)
    
def main():
    parser = ArgumentParser()
    parser.add_argument('pupil_ip', help='ip to pupil capture software', type=str)
    parser.add_argument('pupil_port', help='port to communicate with pupil capture software', type=int)
    parser.add_argument('simulink_port', help= 'port to communicate with simulink', type=int)
    args = parser.parse_args()
    remote = PupilRemote(args.pupil_ip, args.pupil_port, args.simulink_port)
    socket = remote.open_socket()
    sub_port = remote.subscribe_to_topic(socket)
    remote.receive_send_data(sub_port)
   
        
if __name__ == "__main__":
    main()