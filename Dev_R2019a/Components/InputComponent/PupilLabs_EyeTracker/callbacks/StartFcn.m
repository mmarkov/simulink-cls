% cmd_line = ['" start python ' PATH.get(gcb) '/res/NetworkAccess.py"' ' &&exit' ' &']; % The & at the end is important, change just the file name 
fullfilePath = [evalin('base','PATH.get(gcb)') '/res/EyetrackerSimulinkBridge.py'];
currentBlock = gcb;

pupil_ip = get_param(currentBlock,'udpHost'); % the IP where the pupil capture software is running
simulink_port = get_param(currentBlock,'udpLocal'); %the port specified in Simulink
pupil_port = get_param(currentBlock,'udpBridge'); % he Port to connect to the pupil capture software 

cmd_line = ['start python ' fullfilePath ' ' pupil_ip ' ' pupil_port ' ' simulink_port ' &']; 
[status,~] = system(cmd_line);
if status ~= 0
    fprintf("Something went wrong when initializing the EyeTracker");
    msgbox('Something went wrong when starting the EyeTracker UPD App! Do you have Python installed?');
    set_param( bdroot, 'SimulationCommand', 'Stop' );	
end
clear status cmd_line fullfilePath
