cmd_line = ['"' PATH.get(gcb) '/res/QualisysUDP.exe"' ' &&exit' ' &']; % The & at the end is important, change just the file name 
status = system(cmd_line);
if status ~= 0
    msgbox('Something went wrong when starting the Qualisys UPD App!');
    set_param( bdroot, 'SimulationCommand', 'Stop' );	
end
clear status cmd_line
