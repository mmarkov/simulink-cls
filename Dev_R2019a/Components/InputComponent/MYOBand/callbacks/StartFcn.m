currentBlock = gcb;
udpHost = get_param(currentBlock,'udpHost');
udpPort = get_param(currentBlock,'udpLocal');
% kill any potentially running MyoUDP.exe
try
    system('TASKKILL -f -im "MyoUDP.exe" -im "cmd.exe"');
catch
    
end
% start new MyoUDP.exe
fullfilePath = [PATH.get(currentBlock) '/res/MyoUDP/MyoUDP.exe'];

cmd_line = [ fullfilePath ' ' udpHost ' ' udpPort ' &&exit' ' &']; % The & at the end is important, change just the file name 
status = system(cmd_line);
if status ~= 0
    msgbox('Something went wrong when starting the MyoUPD App!');
    set_param( bdroot, 'SimulationCommand', 'Stop' );	
end
clear fullfilePath currentBlock error_id udpHost udpPort