function fullExperimentTrials = generateFULLexperimentTrials(ncontrolModes,nObjects,nGraspingSides,nStartingRots)
% Generation of the total number of trials

totalTrials = ncontrolModes * 3*nObjects * nGraspingSides;

fullExperimentTrials = -1*ones(ncontrolModes,totalTrials);
trialsPerControl = totalTrials / ncontrolModes;

controlOrder = mod(randperm(ncontrolModes),ncontrolModes); %remember in output I increase by 1 to be 1-based!

for i = 0:3
    start = 1                   + trialsPerControl*i;
    ends = trialsPerControl     + trialsPerControl*i;
    
    fullExperimentTrials(1,start:ends) = mod(randperm(trialsPerControl),nStartingRots); %initialOrientation;
    fullExperimentTrials(2,start:ends) = mod(randperm(trialsPerControl),nGraspingSides); %grasp Mode;
    fullExperimentTrials(3,start:ends) = mod(randperm(trialsPerControl),nObjects); %target;
    fullExperimentTrials(4,start:ends) = controlOrder(i+1).*ones(1,trialsPerControl); %control Mode;
end

fullExperimentTrials = fullExperimentTrials+1; %all my indexes go from 1, not from 0. 