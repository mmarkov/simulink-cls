
folderName = 'D:\Experiments\Julia\';

Answers = [];
Tasks = [];
% Finding the Answers
AnswerSubj = load([folderName 'Answers1.mat']);
Answers = ConcatenData(Answers, AnswerSubj.simoutAnswers);
TaskSubj = load([folderName 'Task1.mat']);
Tasks = ConcatenData(Tasks, TaskSubj.simoutTask);

results = GuessingGameAnalysis(TaskSubj.simoutTask.signals.values, AnswerSubj.simoutAnswers.signals.values);
figure(1); 
plotConfMat(80, results,{'1' '2' '3' '4' '5' '6' '7' '8' sprintf('Correct / Incorrect')});

AnswerSubj = load([folderName 'Answers1New.mat']);
Answers = ConcatenData(Answers, AnswerSubj.simoutAnswers);
TaskSubj = load([folderName 'Task1New.mat']);
Tasks = ConcatenData(Tasks, TaskSubj.simoutTask);

results = GuessingGameAnalysis(TaskSubj.simoutTask.signals.values, AnswerSubj.simoutAnswers.signals.values);
figure(2); 
plotConfMat(80, results,{'1' '2' '3' '4' '5' '6' '7' '8' sprintf('Correct / Incorrect')});

% AnswerSubj = load([folderName 'Answers2.mat']);
% Answers = ConcatenData(Answers, AnswerSubj.simoutAnswers);
% TaskSubj = load([folderName 'Task2.mat']);
% Tasks = ConcatenData(Tasks, TaskSubj.simoutTask);
% 
% results = GuessingGameAnalysis(TaskSubj.simoutTask.signals.values, AnswerSubj.simoutAnswers.signals.values);
% figure(3); 
% plotConfMat(80, results,{'1' '2' '3' '4' '5' '6' '7' '8' sprintf('Correct / Incorrect')});

% AnswerSubj = load([folderName 'AnswerJeremy.mat']);
% Answers = ConcatenData(Answers, AnswerSubj.simoutAnswers);
% TaskSubj = load([folderName 'TaskJeremy.mat']);
% Tasks = ConcatenData(Tasks, TaskSubj.simoutTask);
% 
% results = GuessingGameAnalysis(TaskSubj.simoutTask.signals.values, AnswerSubj.simoutAnswers.signals.values);
% figure(4); 
% plotConfMat(80, results,{'1' '2' '3' '4' '5' '6' '7' '8' sprintf('Correct / Incorrect')});
% 
% 
% 
% results = GuessingGameAnalysis(Tasks, Answers);
% figure(5); 
% plotConfMat(80 * 3, results,{'1' '2' '3' '4' '5' '6' '7' '8' sprintf('Correct / Incorrect')});

