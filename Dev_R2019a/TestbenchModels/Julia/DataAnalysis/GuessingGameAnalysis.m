function [results] = GuessingGameAnalysis (Tasks, Answers)


% Finding the Answers
ValuePosition = find(Answers ~= 0); %.simoutAnswers.signals.values
SubjectsAnswer = Answers(ValuePosition);

TaskValues = Tasks(ValuePosition - 1,2); %.simoutTask.signals.values

%MatrixTogether = [TaskValues SubjectsAnswer];

% Build arguments for the Plotconfusion function
TargetMatrix = zeros(6, length(TaskValues));
AnswerMatrix = zeros(6, length(SubjectsAnswer));

for i = 1:length(SubjectsAnswer)
    TargetMatrix(TaskValues(i),i) = 1;
end

for i = 1:length(TaskValues)
    AnswerMatrix(SubjectsAnswer(i),i) = 1;
end

GoodVector = TaskValues==SubjectsAnswer;
figure;
plot(GoodVector);

%plotconfusion(TargetMatrix,AnswerMatrix)

results = confusionmat(SubjectsAnswer, TaskValues);
STD = std(diag(results))
%plotconfusion(C)


%C = plotconfusion(MatrixTogether)
%C = plotconfusion(Task,SubjectsAnswer)


% Success = MatrixTogether(:,1) == MatrixTogether(:,2);
% SuccessRate = sum(Success)/length(Success)

