function plotConfMat(num_of_repet, varargin)
%PLOTCONFMAT plots the confusion matrix with colorscale, absolute numbers
%   and precision normalized percentages

% number of arguments
switch (nargin)
    case 0
       confmat = 1;
       labels = {'1'};
    case 1
       confmat = varargin{1};
       labels = 1:size(confmat, 1);
    otherwise
       confmat = varargin{1};
       labels = varargin{2};
end

confmat(isnan(confmat))=0; % in case there are NaN elements
numlabels = size(confmat, 1)+1; % number of labels

% calculate the percentage accuracies
% confpercent = 100*confmat./repmat(sum(confmat, 1),numlabels,1);
confpercent = 100*confmat./num_of_repet; %80 is the number of repetition (10) times number of electrods (8)
for i = 1:length(confmat)
    rightcolC(i) = 100*confmat(i,i)/sum(confmat(i,:));
    bottomrowC(i) = 100*confmat(i,i)/sum(confmat(:,i));
end

rightcolI=100-rightcolC;
bottomrowI=100-bottomrowC;

lastC=100*trace(confmat)/sum(confmat(:));
lastI=100-lastC;

totalgrig = [confpercent ,ones(8,1)];
totalgrig = [totalgrig; ones(1,9)];

% plotting the colors
imagesc(totalgrig);
title(sprintf('Accuracy: %.2f%%', 100*trace(confmat)/sum(confmat(:))));
ylabel('Output Class'); xlabel('Target Class');

% set the colormap
colormap(flipud(gray));

% Create strings from the matrix values and remove spaces
textStrings = num2str([ confmat(:), confpercent(:)], '%d\n%.1f%%\n');
textStrings = strtrim(cellstr(textStrings));

for  i = length(rightcolC):-1:1
    temp = num2str([ bottomrowC(i).', bottomrowI(i)], '%.1f%%\n%.1f%%\n');
    temp = strtrim(cellstr(temp));
    if(i==length(rightcolC))
        textStrings = [textStrings(1:i*8); temp];
    else
        textStrings = [textStrings(1:i*8); temp; textStrings(i*8+1:end)];
    end
end

temp = num2str([ rightcolC(:), rightcolI(:)], '%.1f%%\n%.1f%%\n');
temp = strtrim(cellstr(temp));
textStrings =[textStrings; temp];

temp = num2str([ lastC(i), lastI(i)], '%.1f%%\n%.1f%%\n');
temp = strtrim(cellstr(temp));
textStrings =[textStrings; temp];


% Create x and y coordinates for the strings and plot them
[x,y] = meshgrid(1:numlabels);
hStrings = text(x(:),y(:),textStrings(:), ...
    'HorizontalAlignment','center');

% Get the middle value of the color range
midValue = mean(get(gca,'CLim'));

% Choose white or black for the text color of the strings so
% they can be easily seen over the background color
textColors = repmat(confpercent(:) > midValue,1,3);

for  i = length(rightcolC):-1:1
    if(i==length(rightcolC))
        textColors = [textColors(1:i*8,:); [0 0 0]];
    else
        textColors = [textColors(1:i*8,:); [0 0 0]; textColors(i*8+1:end,:)];
    end
end
textColors = [textColors ; zeros(9,3)];
set(hStrings,{'Color'},num2cell(textColors,2));

% Setting the axis labels
set(gca,'XTick',1:numlabels,...
    'XTickLabel',labels,...
    'YTick',1:numlabels,...
    'YTickLabel',labels,...
    'TickLength',[0 0]);
end