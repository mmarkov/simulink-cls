% folderName = 'D:\Experiments\Julia\';
% AnswerSubj = load([folderName 'Answers1.mat']);
% Answers = ConcatenData(Answers, AnswerSubj.simoutAnswers);
% TaskSubj = load([folderName 'Task1.mat']);
% Tasks = ConcatenData(Tasks, TaskSubj.simoutTask);

results = GuessingGameAnalysis(simoutTask.signals.values, simoutAnswer.signals.values);
figure(1); 
plotConfMat(40, results,{'1' '2' '3' '4' '5' '6' '7' '8' sprintf('Correct / Incorrect')});