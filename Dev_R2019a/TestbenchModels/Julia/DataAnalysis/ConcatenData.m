function NewFile = ConcatenData (BigFile, NewData)

NewFile = [BigFile; NewData.signals.values];
