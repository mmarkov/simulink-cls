% add the current model's folder to the path
function addModelToPath(modelH)
fullFilePath = get_param(modelH,'filename');
fSep = filesep;
if ~isempty(fullFilePath)
    idx = strfind(fullFilePath,fSep);
    addpath(fullFilePath(1:idx(end)-1));
end
end