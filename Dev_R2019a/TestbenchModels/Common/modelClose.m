% Reset the build folder to default build folder.
% Note: Somewhere along the way the build folder has been changed.
% Note on note: The build folder is the folder where the executable to be
% run is built.
Simulink.fileGenControl('set', 'CacheFolder', '','CodeGenFolder', '', 'keepPreviousPath',true, 'createDir', true);
vrclear('-force') 