addpath('F:\SVNProjects\CLS\Dev_R2016a\Testbench models\Jeremy\FOM_Extended_Analisys');
addpath('F:\SVNProjects\CLS\Dev_R2016a\Testbench models\Jeremy\FOM_Extended_Analisys\aboxplot');
addpath('F:\SVNProjects\CLS\Dev_R2016a\Testbench models\Jeremy\FOM_Extended_Analisys\MYO');

FinalResults = true;

%% Variables
direc = 'E:\MYOPACE\';
folder = 'Subject';

LDA = 'LDATrial';
Auto = 'AutoTrial';
states = {'Reaching', 'Preshaping', 'Grasping', 'Transpo', 'Manip', 'Releasing'};
%states = {'Reaching', 'Preshaping', 'Grasping', 'Pressing', 'Transpo', 'Manip', 'Releasing'};

NoEMGPhases = [1,4];
EMGPhases = 1:length(states);
EMGPhases = EMGPhases(~ismember(EMGPhases,NoEMGPhases));
NoiseLvls = [0, 0.1];
numberOfNoiseLvl = length(NoiseLvls);
numberOfMea = 7;
color = colorgrad(5,'blue_up');
color = color([2 4], :);

%% Old Data logging
path = 'E:\MYOPACE/';
Subjects = ['Subject001'; 'Subject002'; 'Subject003'; 'Subject004'; 'Subject005'; 'Subject006'; 'Subject007'; 'Subject008'; 'Subject009'; 'Subject010'];
LegendText = {'Subject1', 'Subject2', 'Subject3', 'Subject4', 'Subject5', 'Subject6', 'Subject7', 'Subject8', 'Amputee1', 'Amputee2'};
for i = 1:size(Subjects,1)
    array = load([path Subjects(i,:) '/SummaryLDA.mat']);
    if i == 6
        array.SummaryLDA(:,11) = [];
    end
    scores{1,i} = array.SummaryLDA(1,:);
    array = load([path Subjects(i,:) '/SummaryAuto.mat']);
    scores{2,i} = array.SummaryAuto(1,:);
end

%% data Loading
folders = dir([direc '*' folder '*']);
iter = 0;
for fold = 1:length(folders)-3
    clear data
    iter = iter + 1;
    folder = folders(fold).name;
    
    %% LDA
    clear dataExtLDA
    listOfLDA = dir([direc folder '\*' LDA '*.mat']);
    record = length(listOfLDA) - 5:length(listOfLDA);
    scoresArrayLDA = scores{1,fold};
    scoresArrayAUTO = scores{2,fold};
    for i = 1:length(record)
        load([direc folder '\' listOfLDA(record(i)).name]);
        
            
            if fold == 1 || fold == 2 || true
                [dataExtLDA(i).proportion, ~, ~, ~]  = JExperimentAnalysisMYO(data);
                dataExtLDA(i).maxTime = scoresArrayLDA(record(i));
                dataExtLDA(i).proportion = dataExtLDA(i).proportion * dataExtLDA(i).maxTime;
            else
                [dataExtLDA(iter).proportion, ~, dataExtLDA(iter).ComputedResults, ~]  = JExperimentAnalysisAcc(IDs == idx, simoutCtrlSigWithNoise, simoutProsthesisFeedback, simoutAccWorld);
            end
    end
    AllSubjects{iter,1} = dataExtLDA;
    
    %% LDA
    clear dataExtAuto
    listOfAuto = dir([direc folder '\*' Auto '*.mat']);
    record = length(listOfAuto) - 5:length(listOfAuto);
    for i = 1:length(record)
        load([direc folder '\' listOfAuto(record(i)).name]);
        
            
            if fold == 1 || fold == 2 || true
                [dataExtAuto(i).proportion, dataExtAuto(i).maxTime, ~, ~]  = JExperimentAnalysisMYO(data);
                dataExtAuto(i).maxTime = scoresArrayAUTO(record(i));
                dataExtAuto(i).proportion = dataExtAuto(i).proportion * dataExtAuto(i).maxTime;
            else
                [dataExtAuto(iter).proportion, ~, dataExtAuto(iter).ComputedResults, ~]  = JExperimentAnalysisAcc(IDs == idx, simoutCtrlSigWithNoise, simoutProsthesisFeedback, simoutAccWorld);
            end
    end
    AllSubjects{iter,2} = dataExtAuto;
    
    %% Data Processing
%     prop = zeros(6, length(states));
%     maxTime = zeros(1,6);
%     for i = 1:length(AllSubjects{iter,1})
%         prop(i,:) = AllSubjects{iter,1}.proportion;
%         maxTime(i) = AllSubjects{iter,1}.maxTime;
%     end
%     summary.proportionsLDA = prop;
%     summary.MeanPropLDA = mean(prop);
%     summary.StdPropLDA = std(prop);
%     summary.MeanTimeLDA = mean(maxTime);

    prop = zeros(6, length(states));
    maxTime = zeros(1,6);
    for i = 1:length(dataExt)
        prop(i,:) = dataExtLDA(i).proportion;
        maxTime(i) = dataExtLDA(i).maxTime;
    end
    summary.proportionsLDA = prop;
    summary.MeanPropLDA = mean(prop);
    summary.StdPropLDA = std(prop);
    summary.MeanTimeLDA = mean(maxTime);
    summary.StdTimeLDA = std(maxTime);
    
    prop = zeros(6, length(states));
    maxTime = zeros(1,6);
%     for i = 1:length(AllSubjects{iter,2})
%         prop(i,:) = AllSubjects{iter,2}.proportion;
%         maxTime(i) = AllSubjects{iter,2}.maxTime;
%     end
    
    for i = 1:length(dataExt)
        prop(i,:) = dataExtAuto(i).proportion;
        maxTime(i) = dataExtAuto(i).maxTime;
    end
    summary.proportionsAUTO = prop;
    summary.MeanPropAUTO = mean(prop);
    summary.StdPropAUTO = std(prop);
    summary.MeanTimeAUTO = mean(maxTime);
    summary.StdTimeAUTO = std(maxTime);
    
    AllSubjects{iter,3} = summary;
    
    
end

totalPrportions = zeros(2,length(AllSubjects), 6);
totalStdPrportions = zeros(2,length(AllSubjects), 6);
totalTime = zeros(2,length(AllSubjects));
totalStdTime = zeros(2,length(AllSubjects));
for i = 1:length(AllSubjects)
    totalPrportions(1,i,:) = AllSubjects{i,3}.MeanPropLDA;
    totalPrportions(2,i,:) = AllSubjects{i,3}.MeanPropAUTO;
    totalStdPrportions(1,i,:) = AllSubjects{i,3}.StdPropLDA;
    totalStdPrportions(2,i,:) = AllSubjects{i,3}.StdPropAUTO;
    totalTime(1,i) = AllSubjects{i,3}.MeanTimeLDA;
    totalTime(2,i) = AllSubjects{i,3}.MeanTimeAUTO;
    totalStdTime(1,i) = AllSubjects{i,3}.StdTimeLDA;
    totalStdTime(2,i) = AllSubjects{i,3}.StdTimeAUTO;
end


%% Data Analysis
%{
if FinalResults
measurements = zeros(size(AllSubjects,1) * numberOfMea, length(NoiseLvls));

for fold = 1:size(AllSubjects,1)
    mea = zeros(numberOfMea, length(NoiseLvls));
    if fold == 1
        for noise = 1:length(NoiseLvls) - 1
                Idx = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise)) < 0.01;
                res = AllSubjects{fold, 2}.results(Idx);
                measurements((fold-1) * numberOfMea + 1 : fold * numberOfMea,noise) = res(1:numberOfMea);
                mea(:,noise) = res(1:numberOfMea);
        end
        AllSubjects{fold, 2}.OrganizedResults = mea;
    else
        for noise = 1:length(NoiseLvls)
            Idx = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise)) < 0.01;
            res = AllSubjects{fold, 2}.results(Idx);
            measurements((fold-1) * numberOfMea + 1 : fold * numberOfMea,noise) = res(1:numberOfMea);
            mea(:,noise) = res(1:numberOfMea);
        end
        AllSubjects{fold, 2}.OrganizedResults = mea;
    end
end

[p,tbl,stats] = friedman(measurements,numberOfMea, 'off');
multcompare(stats)
end
%% Wilcoxon
if FinalResults
for fold = 1:size(AllSubjects,1)
    matrixSignificancy = zeros(length(NoiseLvls), length(NoiseLvls));
    for noise = 1:length(NoiseLvls)
        Idx = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise)) < 0.01;
        res = AllSubjects{fold, 2}.results(Idx);
        for noise2 = noise+1:length(NoiseLvls)
            Idx2 = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise2)) < 0.01;
            res2 = AllSubjects{fold, 2}.results(Idx2);
            [~, matrixSignificancy(noise, noise2)] = ttest2(res, res2); %ranksum(res, res2);
        end
    end
    AllSubjects{fold, 2}.WilcoxonMatrix = matrixSignificancy;
    
    P_Values = sort(matrixSignificancy(matrixSignificancy ~= 0));
    signi = 0.05;
    SignificancySubj = true;
    for idx = 1: length(P_Values)
        SignificancySubj = P_Values(idx) < signi/(length(P_Values) - idx + 1) & SignificancySubj;
    end
    AllSubjects{fold, 2}.Significancy = SignificancySubj;
end
end
%}
%% Plot
if FinalResults
    % Final Results
    %fig = figure(fold);
    %close(fig);
    fig = figure(1);
    noise0 = cat(2, totalTime(1,:)');
    noise10 = cat(2, totalTime(2,:)');
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    aboxplot(h,'labels',{'Total Time'}, 'colormap', color, 'significancypairing', true);
    leg = legend('Pattern Rec. Ctr.', 'Semi-Autonomous system', 'Location','northeast')
    leg.FontSize = 13;
    %xlabel('System');
    ylabel('Time in  sec');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    title('Total Time');
    set(gca,'YGrid','on');
  
    % Decomposition
    %fig = figure(fold);
    %close(fig);
    noise0 = cat(2, squeeze(totalPrportions(1,:,:)));
    noise10 = cat(2, squeeze(totalPrportions(2,:,:)));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    fig = figure(2);
    aboxplot(h,'labels',states, 'colormap', color, 'significancypairing', true);
    leg = legend('PatternRec. Ctr', 'Semi-Autonomous system', 'Location','northeast')
    leg.FontSize = 13;
    xlabel('Phases');
    ylabel('Time in  seconds');
    title('Movement Phases Decomposition');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    set(gca,'YGrid','on');
    
    % Decomposition
    %fig = figure(fold);
    %close(fig);
    noise0 = cat(2, squeeze(totalStdPrportions(1,:,:)));
    noise10 = cat(2, squeeze(totalStdPrportions(2,:,:)));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    fig = figure(22);
    aboxplot(h,'labels',states, 'colormap', color, 'significancypairing', true);
    leg = legend('PatternRec. Ctr', 'Semi-Autonomous system', 'Location','northeast')
    leg.FontSize = 13;
    xlabel('Phases');
    ylabel('Time in  seconds');
    title('Movement Phases Decomposition');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    set(gca,'YGrid','on');

    % Object Decomposition
    %fig = figure(fold);
    %close(fig);
    fig = figure(3);
    noise0 = cat(2, sum(squeeze(totalPrportions(1,:,1:2)),2), sum(squeeze(totalPrportions(1,:,3:6)),2));
    noise10 = cat(2, sum(squeeze(totalPrportions(2,:,1:2)),2), sum(squeeze(totalPrportions(2,:,3:6)),2));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    fig = figure(3);
    aboxplot(h,'labels',{'system is active', 'system is NOT active'}, 'colormap', color, 'significancypairing', true);
    leg = legend('Pattern Rec. Ctr.', 'Semi-Autonomous system', 'Location','northeast')
    leg.FontSize = 13;
    xlabel('Phases');
    ylabel('Time in  sec');
    title('Movement Phases Decomposition');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)+10]);
    set(gca,'YGrid','on');

    % EMG Decomposition
    %fig = figure(fold);
    %close(fig);
    fig = figure(4);
    lbls = {'EMG', 'No EMG', 'Total'};
    noise0 = cat(2, sum(squeeze(totalPrportions(1,:,EMGPhases)),2), sum(squeeze(totalPrportions(1,:,NoEMGPhases)),2));
    noise10 = cat(2, sum(squeeze(totalPrportions(2,:,EMGPhases)),2), sum(squeeze(totalPrportions(2,:,NoEMGPhases)),2));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    aboxplot(h,'labels',lbls, 'colormap', color, 'significancypairing', true);
    leg = legend('Pattern Rec. Ctr.', 'Semi-Autonomous system', 'Location','northeast')
    leg.FontSize = 13;
    xlabel('Phases');
    ylabel('Time in  sec');
    title('Comparison EMG Usage');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    set(gca,'YGrid','on');
    
    % EMG Decomposition
    %fig = figure(fold);
    %close(fig);
    fig = figure(5);
    lbls = {'EMG', 'No EMG', 'Total'};
    noise0 = cat(2, squeeze(totalPrportions(1,:,2))', sum(squeeze(totalPrportions(1,:,[1 3:6])),2));
    noise10 = cat(2, totalPrportions(2,:,2)', sum(squeeze(totalPrportions(2,:,[1 3:6])),2));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    aboxplot(h,'labels',lbls, 'colormap', color, 'significancypairing', true);
    leg = legend('Pattern Rec. Ctr.', 'Semi-Autonomous system', 'Location','northeast')
    leg.FontSize = 13;
    xlabel('Phases');
    ylabel('Time in  sec');
    title('Comparison EMG Usage');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    set(gca,'YGrid','on');
    
for fold = 1:size(AllSubjects,1)
    fig = figure(fold);
    close(fig);
   
    compareIt = [1, 3];
    %noise0 = cat(2, squeeze(AllSubjects{fold, 2}.proportions(:,:)));
    noise0 = cat(2, squeeze(totalPrportions(1,:,:)));
    noise10 = cat(2, squeeze(totalPrportions(2,:,:)));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    
    fig = figure(fold);
    %subplot(1,3,1:2)
    %if fold == 1;
    aboxplot(h,'labels',states);
    legend('LDA', 'AUTO','Location','northeast')
    xlabel('Phases');
    ylabel('Time in  sec');
    title('Comparison Phases for 1 block');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    set(gca,'YGrid','on');

    
    
    
    fig = figure(fold);
    %subplot(3,3,[6 9])
    lbls = {'EMG', 'No EMG', 'Total'};
    noise0 = cat(2, sum(squeeze(totalPrportions(1,:,EMGPhases)),2), sum(squeeze(totalPrportions(1,:,NoEMGPhases)),2));
    noise10 = cat(2, sum(squeeze(totalPrportions(2,:,EMGPhases)),2), sum(squeeze(totalPrportions(2,:,NoEMGPhases)),2));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    
    aboxplot(h,'labels',lbls, 'colormap', color);
    legend('LDA', 'AUTO','Location','northwest')
    xlabel('Phases');
    ylabel('Time in  sec');
    title('Comparison EMG Usage for 1 Block');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    set(gca,'YGrid','on');

    fig = figure(fold);
    %subplot(3,3,3)
    noise0 = cat(2, totalTime(1,:)');
    noise10 = cat(2, totalTime(2,:)');
    
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise10,[1 size(noise10)]));
    
    aboxplot(h,'labels',{'Number of Block'}, 'colormap', color, 'significancypairing', true);
    legend('LDA', 'AUTO', 'Location','southwest')
    xlabel('States');
    ylabel('Time in  sec');
    
    YLIM = get(gca,'YLim');
    %set(gca,'YLim', [0 YLIM(2)]);
    title('Nomber of Blocks');
    set(gca,'YGrid','on');
    
end
end
%% Proportions
if ~FinalResults
proportions = zeros(size(AllSubjects,1), length(states));
for fold = 1:size(AllSubjects,1)
    fig = figure(fold);
    subplot(9,1,1)
    title(['Subject ' num2str(fold)]);
    subplot(9,1,2:3)
    plot(AllSubjects{fold, 2}.results);
    title('number of blocks in 60sec');
    subplot(9,1,4:5)
    plot([sum(AllSubjects{fold, 2}.proportions(:,EMGPhases),2),sum(AllSubjects{fold, 2}.proportions(:,NoEMGPhases),2)]);
    title('phases decomposition');
    legend('EMG', 'NoEMG');
    subplot(9,1,6:9)
    plot(AllSubjects{fold, 2}.proportions);
    title('phases decomposition');
    legend(states);
    
    saveas(fig, ['rampSubj_' num2str(fold) '.jpg']);
end
end