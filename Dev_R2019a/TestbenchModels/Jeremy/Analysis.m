function [proportions, diffs, maxtime, nbOfCycle] = analysis(file)
%%Introduction
%BBT 2 Phasen
%1) Greifen
%1.1 mit Steuerung
%1.2 ohne Steuerung
%2) Transfer mit Block
%2.1 mit Steuerung
%2.2 ohne Steuerung
% clear all
% clc
load (file)
run Ergebnisauswertung_InOut_BBT

toPlot = false;

%states = {'Grasping', 'Transportation', 'Manipulation', 'Releasing', 'Return', 'Repositionning'};
vektorStates = zeros (1,length(InOut(:,1)));

ctrlSig = abs(sum(InOut(:,2:3),2))'; %Absolute Werte der Steuerung

windowSize = 20;
%ctrlSig_Fil = ctrlSig; %lowFilter(ctrlSig, windowSize);


ctrlSigApp = InOut(:,2)';
ctrlSigRot = InOut(:,3)';

errorSigApp = InOut(:,12)';
errorSigRot = InOut(:,13)';

ctrlSigApp_Fil = medfilt1(ctrlSigApp + errorSigApp, windowSize); %lowFilter(ctrlSigApp, windowSize);
ctrlSigRot_Fil = medfilt1(ctrlSigRot + errorSigRot, windowSize); %lowFilter(ctrlSigRot, windowSize);

%handOpen = InOut(:,5)'; %Handoeffnungsstellung der Prothese
gripForce = InOut(:,7)'; %Griffkraft
touchOnset = InOut(:, 8)'; %Beruehrungszeitpunkt
timeData = InOut(:, 1)'; %Zeit
touchEvents = [1 find(touchOnset) length(touchOnset)];


acc = InOut(:,9:11)';
acc_Fil = acc - repmat(mean(acc(:,touchEvents(2):end),2),1,length(acc));
acc_Fil = lowFilter(acc_Fil, windowSize);

%% State Vector

% ToDo: filter too short transport phase
forceThresh = 0.018;
ctrlThresh = 0.04;
%appThresh = 0.07;

stateFroce = gripForce >= forceThresh;
ForceChange = [zeros(1,10) newDiff(stateFroce,20) zeros(1,10)];

preciseCtrl = zeros(size(ctrlSigRot_Fil));
preciseCtrl(abs(ctrlSigRot_Fil) > ctrlThresh) = 1;
preciseCtrl(ctrlSigApp_Fil < -ctrlThresh) = 2;
preciseCtrl(ctrlSigApp_Fil > ctrlThresh) = 3;
eventsPrecCtrl = [1 find([0 diff(preciseCtrl(1:end-1))~=0]) length(ctrlSigRot)];

statebuild = zeros(size(ctrlSigRot));
stateVector = zeros(1,length(eventsPrecCtrl)-1);

again = true;
while again
    again = false;
    for i = 1:length(eventsPrecCtrl)-1
        state = 0;
        interval = eventsPrecCtrl(i):eventsPrecCtrl(i+1)-1;
        somme = sum(ForceChange(interval));
        if abs(mean(preciseCtrl(interval))) <= 0.1 %NO EMG
            if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                state = 5;
            else
                state = 2;
            end
        else      %EMG
            if somme==0 %NO FORCE CHANGE
                if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                    state = 6;
                else
                    state = 3;
                end
            else
                if somme > 0.1 % NOW FORCE
                    state = 1;
                end
                if somme < -0.1; % NOW NO FORCE ANYMORE
                    state = 4;
                end
            end
        end
        statebuild(interval) = state;
        stateVector(i) = state;
    end
end
 
statebuild = medfilt1(statebuild,5);
proportions = [sum(statebuild==1) sum(statebuild==2) sum(statebuild==3) sum(statebuild==4) sum(statebuild==5) sum(statebuild==6)]/length(statebuild); 

%% Counting cycles

timesOfGrasps = timeData(eventsPrecCtrl(stateVector == 1));
nbOfCycle = length(find(diff(timesOfGrasps) > 1.5) + 1);

%% Speed Work

speedProfil = cumsum(acc_Fil, 2)/100;
normSP = sqrt(sum(speedProfil.^2,1));
normSP = normSP - lowFilter(normSP, 1000);

[~,upPeaks] = findpeaks(normSP, 1:length(normSP), 'MinPeakDistance', 200, 'MinPeakProminence', 0.05);
[~, downPeaks] = findpeaks(-normSP, 1:length(normSP), 'MinPeakDistance', 200, 'MinPeakProminence', 0.05);
peaks = sort([upPeaks downPeaks]);
diffs = mean(abs(diff(normSP(peaks))));

%% Plot

if toPlot
    figure(1)
    subplot(3,1,1)
    plot(timeData,[errorSigApp+ctrlSigApp; errorSigRot+ctrlSigRot])
    legend({'Error Aperture', 'Error Rotation'})
    title('noisy signal');

    subplot(3,1,2)
    plot(timeData,[errorSigApp; errorSigRot])
    legend({'Dif Aperture', 'Dif Rotation'})
    title('noise');

    subplot(3,1,3)
    plot(timeData,[ctrlSigApp; ctrlSigRot])
    legend({'Aperture', 'Rotation'})
    title('original signal');

    figure(2);
    subplot(5,1,1)
    plot(timeData,[ctrlSigApp; ctrlSigRot])
    legend({'Aperture', 'Rotation'})

    subplot(5,1,2)
    plot(timeData,[ctrlSigApp; ctrlSigApp_Fil])
    legend({'ctrlSig', 'ctrlSig Fil'})

    subplot(5,1,3)
    plot(timeData, [handOpen; gripForce; touchOnset])
    legend({'Aperture', 'Force', 'Touch'})

    subplot(5,1,4)
    plot(timeData, [preciseCtrl; stateFroce; ForceChange]);
    legend({'preciseCtrl', 'phase', 'force change'});

    subplot(5,1,5)
    plot(timeData, statebuild);
    legend({'state'});

    figure(3)
    subplot(4,1,1)
    plot(timeData,acc_Fil);
    legend({'acc_x','acc_y','acc_z'});

    subplot(4,1,2)
    plot(timeData,speedProfil);
    legend({'acc_x','acc_y','acc_z'});

    subplot(4,1,3)
    plot(timeData,normSP);
    legend({'speed_x','speed_y','speed_z'});

    subplot(4,1,4)
    plot(timeData,normSP);
    legend({'speed_x','speed_y','speed_z'});
end

maxtime = timeData(length(timeData));