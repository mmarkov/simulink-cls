function [EndNoisePourcentage, gatherPeriodNoise] = framePerFrame(Signals, prozentNoise, sizeWindow, meanNoiseWindow, calmTime, regulationFaktor)
%#codegen
oldNoiseActive = false;
lengthSigActive = 1;
currentProNoise = 0;
frameRate = 0.01;
threshSig = 0.01;
currentWindow = 0;

gatherPeriodNoise = [];

%% Rest
for iterSig = 1:length(Signals);
    ctrlSig = Signals(:,iterSig);

    SignalNoise = false;
    probaPulse = frameRate/(meanNoiseWindow * (1-prozentNoise) / prozentNoise - calmTime);

    if abs(sum(ctrlSig)) > threshSig
        if oldNoiseActive 

            currentWindow = currentWindow + frameRate;
            if currentWindow < noiseWindow + calmTime
                oldNoiseActive = true;
                if currentWindow < noiseWindow
                    SignalNoise = true;
                else
                    SignalNoise = false;
                end
            else
                currentWindow = 0;
                oldNoiseActive = false;
                SignalNoise = false;
            end

        else
            %r = RandUni(2);
            r = rand;
            if (r < probaPulse && lengthSigActive <= 10/frameRate) || (r < probaPulse * (1 + (prozentNoise - currentProNoise) * regulationFaktor) && lengthSigActive > 10/frameRate)
                oldNoiseActive = true;
                SignalNoise = true;
                
                gatherPeriodNoise = [gatherPeriodNoise, lengthSigActive];

                while true
                    r = randn(1,4);
                    noiseWindow = sum(r.^2)/4 * (sizeWindow(2) - sizeWindow(1)) + sizeWindow(1);
                    if noiseWindow >= sizeWindow(1) && noiseWindow <= sizeWindow(3)
                        break;
                    end
                end 

                %{
                while true
                    r = RandNorm(randi(100,1,4));
                    noiseWindow = sum(r.^2)/4 * (sizeWindow(2) - sizeWindow(1)) + sizeWindow(1);
                    if noiseWindow >= sizeWindow(1) && noiseWindow <= sizeWindow(3)
                        break;
                    end
                end 
                %}
            else
                oldNoiseActive = false;
                SignalNoise = false;
            end
        end
%{
        if SignalNoise
            noisedCtrl = abs(sum(ctrlSig))* noise;
        end
%}
        currentProNoise = (currentProNoise * lengthSigActive + SignalNoise)/(lengthSigActive + 1);
        lengthSigActive = lengthSigActive + 1;
    end
end
EndNoisePourcentage = currentProNoise;
gatherPeriodNoise = diff(gatherPeriodNoise);