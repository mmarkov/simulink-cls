function [proportions, maxtime, nbOfCycle, Freq] = JExperimentAnalysisAcc(idx, simoutCtrlSigWithNoise, simoutProsthesisFeedback, simoutAccWorld)
%%Introduction
%BBT 2 Phasen
%1) Greifen
%1.1 mit Steuerung
%1.2 ohne Steuerung
%2) Transfer mit Block
%2.1 mit Steuerung
%2.2 ohne Steuerung
% clear all
% clc

toPlot = true;
EMG = true;
InOut = JExperiementReader(idx, simoutCtrlSigWithNoise, simoutProsthesisFeedback);

%states = {'Grasping', 'Transportation', 'Manipulation', 'Releasing', 'Return', 'Repositionning'};
%vektorStates = zeros (1,length(InOut(:,1)));

%ctrlSig = abs(sum(InOut(:,2:3),2))'; %Absolute Werte der Steuerung

windowSize = 20;
%ctrlSig_Fil = ctrlSig; %lowFilter(ctrlSig, windowSize);

if EMG
    ctrlSigApp = InOut(:,2)';
    ctrlSigRot = InOut(:,3)';
    ctrlSigFlex = InOut(:,4)';
else
    ctrlSigApp = InOut(:,6)';
    ctrlSigRot = InOut(:,7)';
    ctrlSigFlex = InOut(:,8)';
end

ctrlSigApp_Fil = medfilt1(ctrlSigApp, windowSize); %lowFilter(ctrlSigApp, windowSize);
ctrlSigManip_Fil = medfilt1(ctrlSigRot + ctrlSigFlex, windowSize); %lowFilter(ctrlSigApp, windowSize);

%handOpen = InOut(:,5)'; %Handoeffnungsstellung der Prothese
gripForce = InOut(:,5)'; %Griffkraft
timeData = InOut(:, 1)'; %Zeit

%% State Vector

% ToDo: filter too short transport phase
forceThresh = 0.018;
if EMG
    ctrlThresh = 0.04;
else
    ctrlThresh = 0;
end
%appThresh = 0.07;

stateFroce = gripForce >= forceThresh;
ForceChange = [zeros(1,10) newDiff(stateFroce,20) zeros(1,10)];

preciseCtrl = zeros(size(ctrlSigManip_Fil));
preciseCtrl(abs(ctrlSigManip_Fil) > ctrlThresh) = 1;
preciseCtrl(ctrlSigApp_Fil < -ctrlThresh) = 2;
preciseCtrl(ctrlSigApp_Fil > ctrlThresh) = 3;
eventsPrecCtrl = [1 find([0 diff(preciseCtrl(1:end-1))~=0]) length(ctrlSigRot)];

statebuild = zeros(size(ctrlSigRot));
stateVector = zeros(1,length(eventsPrecCtrl)-1);

again = true;
while again
    again = false;
    for i = 1:length(eventsPrecCtrl)-1
        state = 0;
        interval = eventsPrecCtrl(i):eventsPrecCtrl(i+1)-1;
        somme = sum(ForceChange(interval));
        if abs(mean(preciseCtrl(interval))) <= 0.1 %NO EMG
            if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                state = 5;
            else
                state = 2;
            end
        else      %EMG
            if somme==0 %NO FORCE CHANGE
                if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                    state = 6;
                else
                    state = 3;
                end
            else
                if somme > 0.1 % NOW FORCE
                    state = 1;
                end
                if somme < -0.1; % NOW NO FORCE ANYMORE
                    state = 4;
                end
            end
        end
        statebuild(interval) = state;
        stateVector(i) = state;
    end
end
 
Freq = mean(diff(timeData));
statebuild = medfilt1(statebuild,5);
proportions = [sum(statebuild==1) sum(statebuild==2) sum(statebuild==3) sum(statebuild==4) sum(statebuild==5) sum(statebuild==6)]/length(statebuild); 
%% Counting cycles

phases = [1, 2, 4, 5];
iterPhase = 1;
computedRes = 0;
last = 0;
for iter = 1:length(statebuild)
    if (statebuild(iter) == phases(iterPhase))&& (mod(iterPhase,2) == 0 || iter - last > 150)
        iterPhase = iterPhase + 1;
        last = iter;
    end
    if iterPhase == 5;
        iterPhase = 1;
        computedRes = computedRes + 1;
    end
end
timesOfGrasps = timeData(eventsPrecCtrl(stateVector == 1));
nbOfCycle = length(find(diff(timesOfGrasps) > 1.5) + 1);
nbOfCycle = computedRes;

%% Vit
acc = squeeze(simoutAccWorld.signals.values(2,:,idx))';
acc = acc - repmat(mean(acc, 1), size(acc,1),1);
vit = cumsum(acc,1);
vit = normAcc(vit);


%% Plot

if toPlot
    %{
    figure(1)
    subplot(3,1,1)
    plot(timeData,[errorSigApp+ctrlSigApp; errorSigRot+ctrlSigRot])
    legend({'Error Aperture', 'Error Rotation'})
    title('noisy signal');

    subplot(3,1,2)
    plot(timeData,[errorSigApp; errorSigRot])
    legend({'Dif Aperture', 'Dif Rotation'})
    title('noise');

    subplot(3,1,3)
    plot(timeData,[ctrlSigApp; ctrlSigRot])
    legend({'Aperture', 'Rotation'})
    title('original signal');
    %}
%{
    figure(2);
    subplot(5,1,1)
    plot(timeData,[ctrlSigApp_Fil; ctrlSigManip_Fil])
    legend({'Aperture', 'Rotation'})

    subplot(5,1,2)
    plot(timeData,[ctrlSigApp; ctrlSigApp_Fil])
    legend({'ctrlSig', 'ctrlSig Fil'})

    subplot(5,1,3)
    plot(timeData, [gripForce])
    legend({'Force'})

    subplot(5,1,4)
    plot(timeData, [preciseCtrl; stateFroce; ForceChange]);
    legend({'preciseCtrl', 'phase', 'force change'});

    subplot(5,1,5)
    plot(timeData, statebuild);
    legend({'state'});
    
    subplot(5,1,2)
    plot(timeData, vit);
    legend({'state'});
    %}
    
    figure(2);
    subplot(2,1,1)
    plot(timeData, vit);
    legend({'state'});
    subplot(2,1,2)
    plot(timeData, statebuild);
    legend({'state'});
    %{
    figure(3)
    subplot(4,1,1)
    plot(timeData,acc_Fil);
    legend({'acc_x','acc_y','acc_z'});

    subplot(4,1,2)
    plot(timeData,speedProfil);
    legend({'acc_x','acc_y','acc_z'});

    subplot(4,1,3)
    plot(timeData,normSP);
    legend({'speed_x','speed_y','speed_z'});

    subplot(4,1,4)
    plot(timeData,normSP);
    legend({'speed_x','speed_y','speed_z'});
    %}
end

maxtime = timeData(length(timeData));
%nbOfCycle = 6;