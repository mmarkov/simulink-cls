
meanProzent = [];
Freq = 100;
lawDisturb = [0.1 0.3 0.7];

for prozentNoise = 0.1:0.1:0.3

    sizeWindow = [0.3 0.5 1.2];
    probaPulse = prozentNoise/(Freq * sizeWindow(2) * (1-prozentNoise));
    
    for fileID = 1:3
        file = ['CRT_n_' num2str(fileID) '.mat'];
        load(file);
        Signals = squeeze(simoutCtrlSig.signals.values);

        
        noiseActive = false;
        currentWindow = 0;
        clck = 0;
        startWindow = 0;
    
        noisedSignals =  zeros(1,size(Signals,1));

        GatheredNoisedSig = zeros(size(Signals));
        
        isNoiseActive = zeros(1,size(Signals,2));
        isSigActive = zeros(1,size(Signals,2));

        for i = 1:length(Signals)

            clck = i/Freq;
            ctrlSignals = Signals(:,i);


            ctrlSignalsNoise = ctrlSignals;
            strengthSig = abs(sum(ctrlSignals));
            
            sigActive = strengthSig < 0.01;

            if noiseActive && clck < startWindow + currentWindow;
                noiseActive = true;
                ctrlSignalsNoise = noisedSignals;
            else
                if rand < probaPulse
                    startWindow = clck;
                    noiseActive = true;
                    if ctrlSignals(1) ~= 0
                        noisedDoF = 3;
                    else
                        noisedDoF = 1;
                    end
                    noisedSignals = zeros(size(ctrlSignals));
                    if rand > 0.5
                        noisedSignals(noisedDoF) = strengthSig;
                    else
                        noisedSignals(noisedDoF) = -strengthSig;
                    end

                    while true
                        currentWindow = (randn^2 + randn^2 + randn^2 + randn^2)/4 * (sizeWindow(2) - sizeWindow(1)) + sizeWindow(1);
                        if currentWindow < sizeWindow(3)
                            break;
                        end
                    end
                    ctrlSignalsNoise = noisedSignals;
                else
                    noiseActive = false;
                    ctrlSignalsNoise = ctrlSignals;
                end
            end
            
            isNoiseActive(i) = noiseActive;
            isSigActive(i) = sigActive;
            GatheredNoisedSig(:,i) = ctrlSignalsNoise;
        end
        sigActive = abs(sum(Signals)) > 0.01;
        wrongSig = (Signals(1,sigActive) ~= GatheredNoisedSig(1,sigActive)) | (Signals(3,sigActive) ~= GatheredNoisedSig(3,sigActive));
        prozent = mean(wrongSig)*100;

        meanProzent = [meanProzent prozent];
    end
end