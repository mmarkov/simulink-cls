regulationFaktorList = [0:0.1:3 4:10 15:5:30 40:10:100];
meanProzent = zeros(500,9,length(regulationFaktorList));
meanMEcarts = zeros(500,9,length(regulationFaktorList));
meanSEcarts = zeros(500,9,length(regulationFaktorList));
Freq = 100;
lawDisturb = [0.1 0.3 0.7];

sizeWindow = [0.3 0.5 0.7];
calmTime = 0.1;
regulationFaktor = 20;
iterR = 0;
for regulationFaktor = regulationFaktorList
    iterR = iterR +1;
for j = 1:500
    iterM = 0;
for prozentNoise = 0.1:0.1:0.3

    probaPulse = 1/(Freq * ((sizeWindow(2)) * (1-prozentNoise) / prozentNoise - calmTime));
    
    for fileID = 1:3
        iterM = iterM + 1;
        file = ['CRT_n_' num2str(fileID) '.mat'];
        load(file);
        Signals = squeeze(simoutCtrlSig.signals.values);
        noisedSignals =  zeros(1,size(Signals,1));

        GatheredNoise = zeros(size(Signals));
        FinalSig = zeros(size(Signals));
        
        isNoiseActive = zeros(1,size(Signals,2));
        isSigActive = zeros(1,size(Signals,2));
        sigActive = abs(sum(Signals)) > 0.01;
        
        
        while true
        iter = 0;
        noiseActive = false;
        currentWindow = 0;
        clck = 0;
        startWindow = 0;
        SignalNoise  = zeros(1, 5000);
        for i = 1:length(SignalNoise)
            clck = i / Freq;
            if noiseActive && clck < startWindow + currentWindow + calmTime;
                noiseActive = true;
                if clck < startWindow + currentWindow
                    SignalNoise(i) = 1;
                else
                    SignalNoise(i) = 0;
                end
            else
                %if rand < probaPulse
                if rand < probaPulse * (1 + (prozentNoise - sum(SignalNoise)/i) * regulationFaktor)
                %if rand < probaPulse * (prozentNoise / (sum(SignalNoise)/i))^regulationFaktor;
                    noiseActive = true;
                    SignalNoise(i) = 1;
                    startWindow = clck;
                    while true
                        currentWindow = randn * (sizeWindow(3) - sizeWindow(1)) + sizeWindow(2);
                        if currentWindow >= sizeWindow(1) && currentWindow <= sizeWindow(3)
                            break;
                        end
                    end
                else
                    noiseActive = false;
                    SignalNoise(i) = 0;
                end
            end
        end
        
        if abs(mean(SignalNoise) - prozentNoise) < 0.007
            break;
        end
        end
        
        oldNoise = 0;
        for i = 1:length(Signals)
            clck = i / Freq;
            ctrlSignals = Signals(:,i);
            strengthSig = abs(sum(ctrlSignals));
            if strengthSig > 0.01
                if SignalNoise(mod(iter,length(SignalNoise))+1)
                    if oldNoise ~= SignalNoise(mod(iter,length(SignalNoise))+1)
                        if ctrlSignals(1) ~= 0
                            noisedDoF = 3;
                        else
                            noisedDoF = 1;
                        end
                        noisedSignals = zeros(size(ctrlSignals));
                        if rand > 0.5
                            noisedSignals(noisedDoF) = strengthSig;
                        else
                            noisedSignals(noisedDoF) = -strengthSig;
                        end
                    end
                    FinalSig(:,i) = noisedSignals;
                else
                    FinalSig(:,i) = ctrlSignals;
                end
                oldNoise = SignalNoise(mod(iter,length(SignalNoise))+1);
                iter = iter + 1;
            end
        end
        wrongSig = (Signals(1,sigActive) ~= FinalSig(1,sigActive)) | (Signals(3,sigActive) ~= FinalSig(3,sigActive));
        prozent = mean(wrongSig)*100;
        ecarts = diff(find(diff(wrongSig)==1));
        stdEcarts = std(ecarts);
        meanEcarts = mean(ecarts);
        meanMEcarts(j,iterM, iterR) = meanEcarts;
        meanSEcarts(j,iterM, iterR) = stdEcarts;
        meanProzent(j,iterM, iterR) = prozent;
    end
end
end
end

%%
%{
rep = 2;
meanProzentSave = meanProzent;
test10 = reshape(meanProzentSave(1:100,1:3),1,300);
mean10 = mean(test10)
std10 = std(test10)
test20 = reshape(meanProzentSave(1:100,4:6),1,300);
mean20 = mean(test20)
std20 = std(test20)
test30 = reshape(meanProzentSave(1:100,7:9),1,300);
mean30 = mean(test30)
std30 = std(test30)
%}

%%
meanNoise = zeros(3,length(regulationFaktorList));
stdNoise = zeros(3,length(regulationFaktorList));
meanEcarts = zeros(3,length(regulationFaktorList));
stdEcarts = zeros(3,length(regulationFaktorList));
dim = 2;
for iterFaktor = 1:length(regulationFaktorList)
    meanPro10 = reshape(meanProzent(:, 1:3, iterFaktor),1,3*size(meanProzent,1));
    meanPro20 = reshape(meanProzent(:, 4:6, iterFaktor),1,3*size(meanProzent,1));
    meanPro30 = reshape(meanProzent(:, 7:9, iterFaktor),1,3*size(meanProzent,1));
    
    meanNoise(:,iterFaktor) = [mean(meanPro10,dim); mean(meanPro20,dim); mean(meanPro30,dim)];
    stdNoise(:,iterFaktor) = [std(meanPro10,0,dim); std(meanPro20,0,dim); std(meanPro30,0,dim)];
    
    meanPro10 = reshape(meanMEcarts(:, 1:3, iterFaktor),1,3*size(meanProzent,1));
    meanPro20 = reshape(meanMEcarts(:, 4:6, iterFaktor),1,3*size(meanProzent,1));
    meanPro30 = reshape(meanMEcarts(:, 7:9, iterFaktor),1,3*size(meanProzent,1));
    
    meanEcarts(:,iterFaktor) = [mean(meanPro10,dim); mean(meanPro20,dim); mean(meanPro30,dim)];
    
    meanPro10 = reshape(meanSEcarts(:, 1:3, iterFaktor),1,3*size(meanProzent,1));
    meanPro20 = reshape(meanSEcarts(:, 4:6, iterFaktor),1,3*size(meanProzent,1));
    meanPro30 = reshape(meanSEcarts(:, 7:9, iterFaktor),1,3*size(meanProzent,1));
    
    stdEcarts(:,iterFaktor) = [mean(meanPro10,dim); mean(meanPro20,dim); mean(meanPro30,dim)];
end

fig(1) = figure;
plot(regulationFaktorList, meanNoise')
title('noise porcentage')
legend('10', '20', '30')

fig(2) = figure;
plot(regulationFaktorList, stdNoise')
title('noise std')
legend('10', '20', '30')

fig(3) = figure;
plot(regulationFaktorList, meanEcarts')
title('Ecart Average')
legend('10', '20', '30')

fig(4) = figure;
plot(regulationFaktorList, stdEcarts')
title('Ecart deviation')
legend('10', '20', '30')
savefig(fig,'Plots.fig')
save('results_sim_Complete.mat')