addpath('F:\SVNProjects\CLS\Dev_R2016a\Testbench models\Jeremy\FOM_Extended_Analisys');
addpath('F:\SVNProjects\CLS\Dev_R2016a\Testbench models\Jeremy\FOM_Extended_Analisys\aboxplot');

FinalResults = true;

%% Variables
direc = 'F:\Experiments\Jeremy\Pilot00\';
folder = 'Subject';
if FinalResults
    part = '\Part2';
    clear AllSubjects;
else
    part = '\Part1';
end
BBT = 'PilotTestBB';
states = {'Reaching', 'Preshaping', 'Grasping', 'Transportation', 'Manipulation', 'Releasing'};
%states = {'Reaching', 'Preshaping', 'Grasping', 'Pressing', 'Transpo', 'Manip', 'Releasing'};

NoEMGPhases = [1,4];
EMGPhases = 1:length(states);
EMGPhases = EMGPhases(~ismember(EMGPhases,NoEMGPhases));
NoiseLvls = [0, 0.05, 0.1, 0.15, 0.2];
numberOfNoiseLvl = length(NoiseLvls);
numberOfMea = 5;
color = colorgrad(numberOfNoiseLvl,'blue_up');

%% data Loading

folders = dir([direc '*' folder '*']);
toDel = [];
for fold = 1:length(folders)
    folder = folders(fold).name;
    if length(dir([direc folder part])) == 0
        toDel = [toDel fold];
    end
end
folders(toDel)=[];

for fold = 1:length(folders)
    clear data
    folder = folders(fold).name;
    if ~isdir([direc folder part])
        continue
    end
    listOfBBT = dir([direc folder part '\*' BBT '*.mat']);
    load([direc folder part '\Results.mat']);

    iter = 0;
    for record = 1:length(listOfBBT)
        load([direc folder part '\' listOfBBT(record).name]);
        IDs = simoutTrialId.signals.values;
        %plot(IDs);
        unique_ID = unique(IDs);
        %simoutProzentNoise.signals.values(mod(1:length(simoutProzentNoise.signals.values),10) ~= 1) = [];
        for idx = unique_ID(2:end)';
            iter = iter + 1;
            if size(results,2) == 2
                data(iter).noiseLvl = results(iter,1);
                data(iter).result = results(iter,2);
            else
                data(iter).noiseLvl = round(mean(simoutProzentNoise.signals.values(IDs == idx)/0.05))*0.05;
                data(iter).result = results(iter);
            end
            sig = squeeze(simoutCtrlSig.signals.values);
            data(iter).sig = sig(:,IDs == idx);
            sig = squeeze(simoutCtrlSigWithNoise.signals.values);
            data(iter).noise = sig(:,IDs == idx);
            sig = squeeze(simoutProsthesisFeedback.signals.values);
            data(iter).feedback = sig(:,IDs == idx);
            if fold == 1 || fold == 2 || true
                [data(iter).proportion, ~, data(iter).ComputedResults, ~]  = JExperimentAnalysis(IDs == idx, simoutCtrlSig, simoutProsthesisFeedback);
            else
                [data(iter).proportion, ~, data(iter).ComputedResults, ~]  = JExperimentAnalysisAcc(IDs == idx, simoutCtrlSigWithNoise, simoutProsthesisFeedback, simoutAccWorld);
            end
            data(iter).proportion = data(iter).proportion * 60 / data(iter).result;
        end
        
        
    end
    
    AllSubjects{fold,1} = data;
    
    %% Data Processing
    res = zeros(length(data), 1);
    mea = zeros(length(data), 1);
    idx = zeros(length(data), 1);
    noi = zeros(length(data), 1);
    prop = zeros(length(data), length(states));
    for i = 1:length(data)
        SigActive = sum(data(i).sig,1) ~= 0;
        data(i).measured_noise = mean(sum(abs(data(i).sig(:,SigActive) - data(i).noise(:,SigActive))) ~= 0);
        res(i) = data(i).result;
        noi(i) = data(i).noiseLvl;
        idx(i) = i;
        mea(i) = data(i).measured_noise;
        prop(i,:) = data(i).proportion;
    end
    summary.results = res;
    summary.measuredNoise = mea;
    summary.noiseLvl = noi;
    summary.proportions = prop;
    
    AllSubjects{fold,2} = summary;
    
    %% Data Processing Noise Analysis
    unique_Noise = unique(AllSubjects{fold, 2}.noiseLvl);
    NoiseAnalysis = zeros(length(unique_Noise),3);
    for noise = 1:length(unique_Noise)
        NoiseAnalysis(noise,1) = unique_Noise(noise);
        Idx = AllSubjects{fold, 2}.noiseLvl == unique_Noise(noise);
        NoiseAnalysis(noise,2) = mean(AllSubjects{fold, 2}.measuredNoise(Idx));
        NoiseAnalysis(noise,3) = std(AllSubjects{fold, 2}.measuredNoise(Idx));
    end
    AllSubjects{fold,2}.NoiseAnalysis = NoiseAnalysis;
    
    %% Data Processing Proportions
    
    ProportionsAnalysis = zeros(length(NoiseLvls),numberOfMea , length(states));
    for noise = 1:length(NoiseLvls)
        Idx = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise)) < 0.01;
        tempo_propo = AllSubjects{fold, 2}.proportions(Idx,:);
        MaxMea = min(size(tempo_propo,1), numberOfMea);
        ProportionsAnalysis(noise,1:MaxMea,:) = tempo_propo(1:MaxMea, :);
        
    end
    AllSubjects{fold,2}.ProportionsAnalysis = ProportionsAnalysis;
    
    
end


%% Data Analysis
if FinalResults
measurements = zeros(size(AllSubjects,1) * numberOfMea, length(NoiseLvls));

for fold = 1:size(AllSubjects,1)
    mea = zeros(numberOfMea, length(NoiseLvls));
    if fold == 1
        for noise = 1:length(NoiseLvls) - 1
                Idx = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise)) < 0.01;
                res = AllSubjects{fold, 2}.results(Idx);
                measurements((fold-1) * numberOfMea + 1 : fold * numberOfMea,noise) = res(1:numberOfMea);
                mea(:,noise) = res(1:numberOfMea);
        end
        AllSubjects{fold, 2}.OrganizedResults = mea;
    else
        for noise = 1:length(NoiseLvls)
            Idx = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise)) < 0.01;
            res = AllSubjects{fold, 2}.results(Idx);
            measurements((fold-1) * numberOfMea + 1 : fold * numberOfMea,noise) = res(1:numberOfMea);
            mea(:,noise) = res(1:numberOfMea);
        end
        AllSubjects{fold, 2}.OrganizedResults = mea;
    end
end

[p,tbl,stats] = friedman(measurements,numberOfMea, 'off');
multcompare(stats)
end
%% Wilcoxon
if FinalResults
for fold = 1:size(AllSubjects,1)
    matrixSignificancy = zeros(length(NoiseLvls), length(NoiseLvls));
    for noise = 1:length(NoiseLvls)
        Idx = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise)) < 0.01;
        res = AllSubjects{fold, 2}.results(Idx);
        for noise2 = noise+1:length(NoiseLvls)
            Idx2 = abs(AllSubjects{fold, 2}.noiseLvl - NoiseLvls(noise2)) < 0.01;
            res2 = AllSubjects{fold, 2}.results(Idx2);
            [~, matrixSignificancy(noise, noise2)] = ttest2(res, res2); %ranksum(res, res2);
        end
    end
    AllSubjects{fold, 2}.WilcoxonMatrix = matrixSignificancy;
    
    P_Values = sort(matrixSignificancy(matrixSignificancy ~= 0));
    signi = 0.05;
    SignificancySubj = true;
    for idx = 1: length(P_Values)
        SignificancySubj = P_Values(idx) < signi/(length(P_Values) - idx + 1) & SignificancySubj;
    end
    AllSubjects{fold, 2}.Significancy = SignificancySubj;
end
end
%% Plot
if FinalResults
for fold = 1:size(AllSubjects,1)
    fig = figure(fold);
    close(fig);
    
    compareIt = [1, 3];
    %{
    signi = zeros(1,6);
    for i = 1:6
        signi(i) = SignificancyCheck(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(1),:,i)), squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(2),:,i)), false);
        labelBoxPlot{i} = [states{i} ' p = ' num2str(signi(i),3)];
    end
%}
    noise0 = cat(2, squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(1,:,:)));
    noise05 = cat(2, squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(2,:,:)));
    noise10 = cat(2, squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(3,:,:)));
    noise15 = cat(2, squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(4,:,:)));
    
    noise20 = cat(2, squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(5,:,:)));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise05,[1 size(noise05)]), reshape(noise10,[1 size(noise10)]), reshape(noise15,[1 size(noise15)]), reshape(noise20,[1 size(noise20)]));
    
    fig = figure(fold);
    subplot(1,3,1:2)
    %if fold == 1;
    aboxplot(h,'labels',states, 'colormap', color);
    legend('noise 0', 'noise 05', 'noise 10', 'noise 15', 'noise 20','Location','northeast')
    xlabel('States');
    ylabel('Time in  sec');
    title('Comparison Phases for 1 block');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);

    
    
    
    fig = figure(fold);
    subplot(3,3,[6 9])
    lbls = {'EMG', 'No EMG', 'Total'};
    %{
    signi = zeros(1,3);
    signi(1) = SignificancyCheck(sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(1),:,[2, 3, 6, 7])),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(2),:,[1,3,4,6])),2), false);
    signi(2) = SignificancyCheck(sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(1),:,[1, 4, 5, 8])),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(2),:,[2,5])),2), false);
    signi(3) = SignificancyCheck(sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(1),:,:)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(compareIt(2),:,:)),2), false);
    
    for i = 1:3
        labelBoxPlot{i} = [lbls{i} ' p = ' num2str(signi(i),3)];
    end
    %}
    noise0 = cat(2, sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(1,:,EMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(1,:,NoEMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(1,:,:)),2));
    noise05 = cat(2, sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(2,:,EMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(2,:,NoEMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(2,:,:)),2));
    noise10 = cat(2, sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(3,:,EMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(3,:,NoEMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(3,:,:)),2));
    noise15 = cat(2, sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(4,:,EMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(4,:,NoEMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(4,:,:)),2));
    noise20 = cat(2, sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(5,:,EMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(5,:,NoEMGPhases)),2), sum(squeeze(AllSubjects{fold, 2}.ProportionsAnalysis(5,:,:)),2));
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise05,[1 size(noise05)]), reshape(noise10,[1 size(noise10)]), reshape(noise15,[1 size(noise15)]), reshape(noise20,[1 size(noise20)]));
    
    aboxplot(h,'labels',lbls, 'colormap', color);
    legend('noise 0', 'noise 05', 'noise 10', 'noise 15', 'noise 20','Location','northwest')
    xlabel('States');
    ylabel('Time in  sec');
    title('Comparison EMG Usage for 1 Block');
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);

    %{
    fig = figure(fold+length(folders));
    close(fig)
    fig = figure(fold+length(folders));
    %}
    fig = figure(fold);
    subplot(3,3,3)
    noise0 = cat(2, AllSubjects{fold, 2}.OrganizedResults(:,1));
    noise05 = cat(2, AllSubjects{fold, 2}.OrganizedResults(:,2));
    noise10 = cat(2, AllSubjects{fold, 2}.OrganizedResults(:,3));
    noise15 = cat(2, AllSubjects{fold, 2}.OrganizedResults(:,4));
    noise20 = cat(2, AllSubjects{fold, 2}.OrganizedResults(:,5));
    
    h = cat(1, reshape(noise0,[1 size(noise0)]), reshape(noise05,[1 size(noise05)]), reshape(noise10,[1 size(noise10)]), reshape(noise15,[1 size(noise15)]), reshape(noise20,[1 size(noise20)]));
    
    aboxplot(h,'labels',{'Number of Block'}, 'colormap', color);
    legend('noise 0', 'noise 05', 'noise 10', 'noise 15', 'noise 20','Location','southwest')
    xlabel('States');
    ylabel('Time in  sec');
    
    YLIM = get(gca,'YLim');
    set(gca,'YLim', [0 YLIM(2)]);
    title('Nomber of Blocks');
    
end
end
%% Proportions
if ~FinalResults
proportions = zeros(size(AllSubjects,1), length(states));
for fold = 1:size(AllSubjects,1)
    fig = figure(fold);
    subplot(9,1,1)
    title(['Subject ' num2str(fold)]);
    subplot(9,1,2:3)
    plot(AllSubjects{fold, 2}.results);
    title('number of blocks in 60sec');
    subplot(9,1,4:5)
    plot([sum(AllSubjects{fold, 2}.proportions(:,EMGPhases),2),sum(AllSubjects{fold, 2}.proportions(:,NoEMGPhases),2)]);
    title('phases decomposition');
    legend('EMG', 'NoEMG');
    subplot(9,1,6:9)
    plot(AllSubjects{fold, 2}.proportions);
    title('phases decomposition');
    legend(states);
    
    saveas(fig, ['rampSubj_' num2str(fold) '.jpg']);
end
end