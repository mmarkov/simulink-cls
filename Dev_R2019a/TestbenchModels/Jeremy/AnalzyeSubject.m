%% Parameters
Freq = 100;
windowSize = 20;
    forceThresh = 0.018;
    ctrlThresh = 0.04;
    %appThresh = 0.07;

%% Files
TrialsBB = 'BoxBlocks_trial_v1_';
TrialsCP = 'BoxBlocks_trial_v2_';
folderData = 'F:\SVNProjects\CLS\Dev_R2016a\Testbench models\Jeremy\Subject00\';

orderTestBB = [2, 3, 0, 3, 3, 1, 2, 0, 1, 3, 3, 1, 2, 0, 2, 1, 1, 0, 2, 0, 4];
orderTestBB = orderTestBB + 1;

listOfBBT = dir([folderData '*' TrialsBB '*.mat']);

for trial = 1:numel(listOfBBT)
    gatherFileNames{trial} = [folderData listOfBBT(trial).name];
end

%% Output Declaration
output = zeros(4, 5, 7);
iterNoiseLvl = [1,1,1,1];


%% Analysis
for fileID = 1:numel(gatherFileNames)-1
    %% Loading
    file = gatherFileNames{fileID};
    load(file);
    
    trialRunning = simoutTrialRunning.signals.values(1:10:end-10);
    trialRunning(cumsum(trialRunning) > (60 * Freq)) = 0;
    trialRunning = trialRunning == 1;
    
    ctrlSig = squeeze(simoutCtrlSig.signals.values);
    ctrlSig = ctrlSig(:,trialRunning);
    noisedSig = squeeze(simoutCtrlSigWithNoise.signals.values);
    noisedSig = noisedSig(:,trialRunning);
    feedback = squeeze(simoutProsthesisFeedback.signals.values);
    feedback = feedback(:,trialRunning); 
    
    gripForce = feedback(5,:);
    ctrlSigApp = ctrlSig(1,:);
    ctrlSigRot = ctrlSig(3,:);

    ctrlSigApp_Fil = medfilt1(ctrlSigApp, windowSize); %lowFilter(ctrlSigApp, windowSize);
    ctrlSigRot_Fil = medfilt1(ctrlSigRot, windowSize); %lowFilter(ctrlSigRot, windowSize);
    
    noise = false(1,length(ctrlSig));
    for i = 1:length(noise)
        noise(i) = ctrlSig(1,i) ~= noisedSig(1,i) | ctrlSig(3,i) ~= noisedSig(3,i);
    end
    noise = noise(sum(ctrlSig,1)>ctrlThresh);
    prozentNoise = mean(noise);
    
    %% Phases
        % ToDo: filter too short transport phase

    stateFroce = gripForce >= forceThresh;
    ForceChange = [zeros(1,10) newDiff(stateFroce,20) zeros(1,10)];

    preciseCtrl = zeros(size(ctrlSigRot_Fil));
    preciseCtrl(abs(ctrlSigRot_Fil) > ctrlThresh) = 1;
    preciseCtrl(ctrlSigApp_Fil < -ctrlThresh) = 2;
    preciseCtrl(ctrlSigApp_Fil > ctrlThresh) = 3;
    eventsPrecCtrl = [1 find([0 diff(preciseCtrl(1:end-1))~=0]) length(ctrlSigRot)];

    statebuild = zeros(size(ctrlSigRot));
    stateVector = zeros(1,length(eventsPrecCtrl)-1);

    again = true;
    while again
        again = false;
        for i = 1:length(eventsPrecCtrl)-1
            state = 0;
            interval = eventsPrecCtrl(i):eventsPrecCtrl(i+1)-1;
            somme = sum(ForceChange(interval));
            if abs(mean(preciseCtrl(interval))) <= 0.1 %NO EMG
                if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                    state = 5;
                else
                    state = 2;
                end
            else      %EMG
                if somme==0 %NO FORCE CHANGE
                    if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                        state = 6;
                    else
                        state = 3;
                    end
                else
                    if somme > 0.1 % NOW FORCE
                        state = 1;
                    end
                    if somme < -0.1; % NOW NO FORCE ANYMORE
                        state = 4;
                    end
                end
            end
            statebuild(interval) = state;
            stateVector(i) = state;
        end
    end

    statebuild = medfilt1(statebuild,5);
    proportions = [sum(statebuild==1) sum(statebuild==2) sum(statebuild==3) sum(statebuild==4) sum(statebuild==5) sum(statebuild==6)]/length(statebuild); 
    
    output(orderTestBB(fileID), iterNoiseLvl(orderTestBB(fileID)),:) = [prozentNoise proportions];
    iterNoiseLvl(orderTestBB(fileID)) = iterNoiseLvl(orderTestBB(fileID)) + 1;
end