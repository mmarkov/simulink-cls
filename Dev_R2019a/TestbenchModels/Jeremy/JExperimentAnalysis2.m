function [proportions, maxtime, nbOfCycle, Freq] = JExperimentAnalysis2(idx, simoutCtrlSigWithNoise, simoutProsthesisFeedback)
%%Introduction
%BBT 2 Phasen
%1) Greifen
%1.1 mit Steuerung
%1.2 ohne Steuerung
%2) Transfer mit Block
%2.1 mit Steuerung
%2.2 ohne Steuerung
% clear all
% clc

toPlot = false;
EMG = true;
InOut = JExperiementReader(idx, simoutCtrlSigWithNoise, simoutProsthesisFeedback);

%states = {'Grasping', 'Transportation', 'Manipulation', 'Releasing', 'Return', 'Repositionning'};
%vektorStates = zeros (1,length(InOut(:,1)));

%ctrlSig = abs(sum(InOut(:,2:3),2))'; %Absolute Werte der Steuerung

windowSize = 20;
%ctrlSig_Fil = ctrlSig; %lowFilter(ctrlSig, windowSize);

%if EMG
    ctrlSigApp = InOut(:,2)';
    ctrlSigRot = InOut(:,3)';
    ctrlSigFlex = InOut(:,4)';
%else
    ProsthApp = InOut(:,6)';
    ProsthRot = InOut(:,7)';
    ProsthFlex = InOut(:,8)';
%end

ctrlSigApp_Fil = medfilt1(ctrlSigApp, windowSize); %lowFilter(ctrlSigApp, windowSize);
ctrlSigManip_Fil = medfilt1(ctrlSigRot + ctrlSigFlex, windowSize); %lowFilter(ctrlSigApp, windowSize);

%handOpen = InOut(:,5)'; %Handoeffnungsstellung der Prothese
gripForce = InOut(:,5)'; %Griffkraft
timeData = InOut(:, 1)'; %Zeit

%% State Vector

% ToDo: filter too short transport phase
forceThresh = 0.018;
if EMG
    ctrlThresh = 0.04;
else
    ctrlThresh = 0;
end
%appThresh = 0.07;

stateFroce = gripForce >= forceThresh;
ForceChange = [zeros(1,10) newDiff(stateFroce,20) zeros(1,10)];

preciseCtrl = zeros(size(ctrlSigManip_Fil));
preciseCtrl(abs(ctrlSigManip_Fil) > ctrlThresh) = 1;
preciseCtrl(ctrlSigApp_Fil < -ctrlThresh) = 2;
preciseCtrl(ctrlSigApp_Fil > ctrlThresh) = 3;
eventsPrecCtrl = [1 find([0 diff(preciseCtrl(1:end-1))~=0]) length(ctrlSigRot)];

statebuild = zeros(size(ctrlSigRot));
stateVector = zeros(1,length(eventsPrecCtrl)-1);


again = true;
while again
    again = false;
    for i = 1:length(eventsPrecCtrl)-1
        state = 0;
        interval = eventsPrecCtrl(i):eventsPrecCtrl(i+1)-1;
        somme = sum(ForceChange(interval));
        if abs(mean(preciseCtrl(interval))) <= 0.1 %NO EMG
            if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                state = 1;
            else
                state = 5;
            end
        else      %EMG
            if somme==0 %NO FORCE CHANGE
                if abs(mean(stateFroce(interval))) <= 0.1 %NO FORCE
                    if mean(preciseCtrl(interval)) < 1.1
                        state = 2;
                    else
                        state = 8;
                    end
                else
                    if mean(preciseCtrl(interval)) < 1.1
                        state = 6;
                    else
                        state = 4;
                    end
                end
            else
                if somme > 0.1 % NOW FORCE
                    state = 3;
                end
                if somme < -0.1; % NOW NO FORCE ANYMORE
                    state = 7;
                end
            end
        end
        if state == 3
            idx = find(ForceChange ~=0);
            idx(idx<interval(1)|idx>interval(end)) = [];
            idx = cast(median(idx), 'uint32');
            statebuild(interval(1):idx) = state;
            statebuild(idx + 1:interval(end)) = state + 1;
        else
            statebuild(interval) = state;
            stateVector(i) = state;
        end
    end
end
 
Freq = mean(diff(timeData));
statebuild = medfilt1(statebuild,5);
statebuild(statebuild==8) = 2;
proportions = [sum(statebuild==1) sum(statebuild==2) sum(statebuild==3) sum(statebuild==4) sum(statebuild==5) sum(statebuild==6) sum(statebuild==7)]/length(statebuild); 
%% Counting cycles

phases = [3,7];
iterPhase = 1;
computedRes = 0;
last = 0;
for iter = 1:length(statebuild)
    if statebuild(iter) == phases(iterPhase)
        iterPhase = iterPhase + 1;
        last = iter;
    end
    if iterPhase == length(phases) + 1;
        iterPhase = 1;
        computedRes = computedRes + 1;
    end
end
timesOfGrasps = timeData(eventsPrecCtrl(stateVector == 1));
nbOfCycle = length(find(diff(timesOfGrasps) > 1.5) + 1);
nbOfCycle = computedRes;

%% Plot

if toPlot
    %{
    figure(1)
    subplot(3,1,1)
    plot(timeData,[errorSigApp+ctrlSigApp; errorSigRot+ctrlSigRot])
    legend({'Error Aperture', 'Error Rotation'})
    title('noisy signal');

    subplot(3,1,2)
    plot(timeData,[errorSigApp; errorSigRot])
    legend({'Dif Aperture', 'Dif Rotation'})
    title('noise');

    subplot(3,1,3)
    plot(timeData,[ctrlSigApp; ctrlSigRot])
    legend({'Aperture', 'Rotation'})
    title('original signal');
    %}

    figure(2);
    subplot(5,1,1)
    plot(timeData,[ctrlSigApp_Fil; ctrlSigManip_Fil])
    legend({'Aperture', 'Rotation'})

    subplot(5,1,2)
    plot(timeData,[ctrlSigApp; ctrlSigApp_Fil])
    legend({'ctrlSig', 'ctrlSig Fil'})

    subplot(5,1,3)
    plot(timeData, [gripForce])
    legend({'Force'})

    subplot(5,1,4)
    plot(timeData, [preciseCtrl; stateFroce; ForceChange]);
    legend({'preciseCtrl', 'phase', 'force change'});

    subplot(5,1,5)
    plot(timeData, statebuild);
    gca.YGrid = 'on';
    legend({'state'});

    %{
    figure(3)
    subplot(4,1,1)
    plot(timeData,acc_Fil);
    legend({'acc_x','acc_y','acc_z'});

    subplot(4,1,2)
    plot(timeData,speedProfil);
    legend({'acc_x','acc_y','acc_z'});

    subplot(4,1,3)
    plot(timeData,normSP);
    legend({'speed_x','speed_y','speed_z'});

    subplot(4,1,4)
    plot(timeData,normSP);
    legend({'speed_x','speed_y','speed_z'});
    %}
end

maxtime = timeData(length(timeData));
%nbOfCycle = 6;