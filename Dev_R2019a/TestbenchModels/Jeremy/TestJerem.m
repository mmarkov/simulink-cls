
Freq = 100;

Means = zeros(1,10000);
noiseWindows = [];

sizeWindow = [0.3 0.5 1.2];
probaNoise = 0.2;

probaPulse = probaNoise/(Freq * sizeWindow(2) * (1-probaNoise));

for iter = 1:length(Means)
    noiseActive = false;
    currentWindow = 0;
    clck = 0;
    startWindow = 0;
    Signal  = zeros(1, 6000);
    for i = 1:length(Signal)
        clck = i / Freq;
        if noiseActive && clck < startWindow + currentWindow;
           noiseActive = true;
           Signal(i) = 1;
        else
            if rand < probaPulse
                noiseActive = true;
                Signal(i) = 1;
                startWindow = clck;
                while true
                    currentWindow = (randn^2 + randn^2 + randn^2 + randn^2)/4 * (sizeWindow(2) - sizeWindow(1)) + sizeWindow(1);
                    if currentWindow < sizeWindow(3)
                        break;
                    end
                end
                noiseWindows = [noiseWindows currentWindow];
            else
                noiseActive = false;
                Signal(i) = 0;
            end
        end
    end
    Means(iter) = mean(Signal);
end

mean(Means)
std(Means)

histogram(noiseWindows);