%% Parameters
calmTime = 0;
regulationFaktor = 0;
Freq = 100;
numberOfIteration = 5;

sizeWindow = [0.1, 0.3, 0.6];
possibleSizeWindow = [0.1, 0.3, 0.5; 0.1, 0.3, 0.7; 0.1, 0.3, 0.8; 0.2, 0.4, 0.8];
possibleRegFaktor = [0:0.1:1.9, 2:0.5:5, 6:10, 15:5:50, 60:10:200];

%% Files
subjects = 'Proband_';
folderData = 'F:\SVNProjects\CLS\Dev_R2016a\Testbench models\Jeremy\Franyi Data\';

DATA = {'BBT_n_','BBT_vers(10)','BBT_vers(20)','CRT_n_','CRT_vers(10)','CRT_vers(20)'};

listOfSubjects = dir([folderData '*' subjects '*']);
selection = 1:7;

gatherFileNames = {};
iterFile = 0;
for sub = selection
    for i = 1:numel(DATA)
        listOfBBT = dir([folderData listOfSubjects(sub).name '\*' DATA{i} '*.mat']);
        for test = 1:length(listOfBBT)
            iterFile = iterFile+1;
            filename = [folderData listOfSubjects(sub).name '\' listOfBBT(test).name];
            gatherFileNames{iterFile} = filename;
        end
    end
end

gatherVektor = zeros(length(possibleRegFaktor), numberOfIteration, 4, numel(gatherFileNames), 3);

vek = zeros(1,100000000);
for i = 1:length(vek)
   while true
        vek(i) = (randn^2 + randn^2 + randn^2 + randn^2)/4 * (sizeWindow(2) - sizeWindow(1)) + sizeWindow(1);
        if vek(i) >= sizeWindow(1) && vek(i) <= sizeWindow(3)
            break;
        end
    end 
end
meanNoiseWindow = mean(vek)

%% Loops
for iterRegFaktor = 1:length(possibleRegFaktor)
    %sizeWindow = possibleSizeWindow(windoNoise,:);
    regulationFaktor = possibleRegFaktor(iterRegFaktor);
    
    iterRegFak = regulationFaktor
    %% Analysis LawNoise


for j = 1:numberOfIteration
    iterProzent = 0;
for prozentNoise = 0.05:0.05:0.2
    iterProzent = iterProzent + 1;
    
for fileID = 1:numel(gatherFileNames)
    file = gatherFileNames{fileID};
    load(file);
    Signals = squeeze(simoutCtrlSig.signals.values);

%% Process

[EndNoisePourcentage, periodNoise] = framePerFrame(Signals, prozentNoise, sizeWindow, meanNoiseWindow, calmTime, regulationFaktor);

gatherVektor(iterRegFaktor, j, iterProzent, fileID, :) = [EndNoisePourcentage, mean(periodNoise), std(periodNoise)];

end
end
end
end

%% Analysis Prozent

meanNoise = zeros(4,length(possibleRegFaktor),3);
stdNoise = zeros(4,length(possibleRegFaktor),3);
minNoise = zeros(4,length(possibleRegFaktor),3);
maxNoise = zeros(4,length(possibleRegFaktor),3);
dim = 2;

for iterSizeWin = 1: length(possibleRegFaktor)
    PoI = 1;
meanPro05 = reshape(gatherVektor(iterSizeWin, :, 1, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro10 = reshape(gatherVektor(iterSizeWin, :, 2, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro15 = reshape(gatherVektor(iterSizeWin, :, 3, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro20 = reshape(gatherVektor(iterSizeWin, :, 4, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));

meanPro05(isnan(meanPro05)) = [];
meanPro10(isnan(meanPro10)) = [];
meanPro15(isnan(meanPro15)) = [];
meanPro20(isnan(meanPro20)) = [];

minNoise(:,iterSizeWin,PoI) = [min(meanPro05,[],dim); min(meanPro10,[],dim); min(meanPro15,[],dim); min(meanPro20,[],dim)];
maxNoise(:,iterSizeWin,PoI) = [max(meanPro05,[],dim); max(meanPro10,[],dim); max(meanPro15,[],dim); max(meanPro20,[],dim)];
meanNoise(:,iterSizeWin,PoI) = [mean(meanPro05,dim); mean(meanPro10,dim); mean(meanPro15,dim); mean(meanPro20,dim)];
stdNoise(:,iterSizeWin,PoI) = [std(meanPro05,0,dim); std(meanPro10,0,dim); std(meanPro15,0,dim); std(meanPro20,0,dim)];


    PoI = 2;
meanPro05 = reshape(gatherVektor(iterSizeWin, :, 1, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro10 = reshape(gatherVektor(iterSizeWin, :, 2, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro15 = reshape(gatherVektor(iterSizeWin, :, 3, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro20 = reshape(gatherVektor(iterSizeWin, :, 4, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));

meanPro05(isnan(meanPro05)) = [];
meanPro10(isnan(meanPro10)) = [];
meanPro15(isnan(meanPro15)) = [];
meanPro20(isnan(meanPro20)) = [];

minNoise(:,iterSizeWin,PoI) = [min(meanPro05,[],dim); min(meanPro10,[],dim); min(meanPro15,[],dim); min(meanPro20,[],dim)];
maxNoise(:,iterSizeWin,PoI) = [max(meanPro05,[],dim); max(meanPro10,[],dim); max(meanPro15,[],dim); max(meanPro20,[],dim)];
meanNoise(:,iterSizeWin,PoI) = [mean(meanPro05,dim); mean(meanPro10,dim); mean(meanPro15,dim); mean(meanPro20,dim)];
stdNoise(:,iterSizeWin,PoI) = [std(meanPro05,0,dim); std(meanPro10,0,dim); std(meanPro15,0,dim); std(meanPro20,0,dim)];

    PoI = 3;
meanPro05 = reshape(gatherVektor(iterSizeWin, :, 1, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro10 = reshape(gatherVektor(iterSizeWin, :, 2, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro15 = reshape(gatherVektor(iterSizeWin, :, 3, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));
meanPro20 = reshape(gatherVektor(iterSizeWin, :, 4, :,PoI),1,numel(gatherVektor(iterSizeWin,:,1,:,PoI)));

meanPro05(isnan(meanPro05)) = [];
meanPro10(isnan(meanPro10)) = [];
meanPro15(isnan(meanPro15)) = [];
meanPro20(isnan(meanPro20)) = [];

%minNoise(:,iterSizeWin) = [min(meanPro05); min(meanPro10); min(meanPro15); min(meanPro20)];

minNoise(:,iterSizeWin,PoI) = [min(meanPro05,[],dim); min(meanPro10,[],dim); min(meanPro15,[],dim); min(meanPro20,[],dim)];
maxNoise(:,iterSizeWin,PoI) = [max(meanPro05,[],dim); max(meanPro10,[],dim); max(meanPro15,[],dim); max(meanPro20,[],dim)];
meanNoise(:,iterSizeWin,PoI) = [mean(meanPro05,dim); mean(meanPro10,dim); mean(meanPro15,dim); mean(meanPro20,dim)];
stdNoise(:,iterSizeWin,PoI) = [std(meanPro05,0,dim); std(meanPro10,0,dim); std(meanPro15,0,dim); std(meanPro20,0,dim)];
end

%% Plots
figure

subplot(2,3,1)
plot(possibleRegFaktor, meanNoise(:,:,1)');
title('Mean Noise');
subplot(2,3,2)
plot(possibleRegFaktor, meanNoise(:,:,2)');
title('Mean mean period');
subplot(2,3,3)
plot(possibleRegFaktor, meanNoise(:,:,3)');
title('mean Std period');
subplot(2,3,4)
plot(possibleRegFaktor, stdNoise(:,:,1)');
title('std noise');
subplot(2,3,5)
plot(possibleRegFaktor, stdNoise(:,:,2)');
title('std mean period');
subplot(2,3,6)
plot(possibleRegFaktor, stdNoise(:,:,3)');
title('std Std period');

save('results_Very_Long_Run.mat');