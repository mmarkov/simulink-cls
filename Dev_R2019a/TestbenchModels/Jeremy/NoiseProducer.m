sizeWindow = [0.3 0.5 0.7];
calmTime = 0.3;
Freq = 100;

prozentNoise = 0.1;

probaPulse = 1/(Freq * ((sizeWindow(2)) * (1-prozentNoise) / prozentNoise - calmTime));
while true
    iter = 0;
    noiseActive = false;
    currentWindow = 0;
    clck = 0;
    startWindow = 0;
    Noise  = false(1, Freq * 50);
    for i = 1:length(Noise)
        clck = i / Freq;
        if noiseActive && clck < startWindow + currentWindow + calmTime;
            noiseActive = true;
            if clck < startWindow + currentWindow
                Noise(i) = true;
            else
                Noise(i) = false;
            end
        else
            if rand < probaPulse
                %if rand < probaPulse * (1 + (prozentNoise - sum(SignalNoise)/i) * regulationFaktor)
                %if rand < probaPulse * (prozentNoise / (sum(SignalNoise)/i))^regulationFaktor;
                noiseActive = true;
                Noise(i) = true;
                startWindow = clck;
                while true
                    currentWindow = randn * (sizeWindow(3) - sizeWindow(1)) + sizeWindow(2);
                    if currentWindow >= sizeWindow(1) && currentWindow <= sizeWindow(3)
                        break;
                    end
                end
            else
                noiseActive = false;
                Noise(i) = false;
            end
        end
    end
    
    if abs(mean(Noise) - prozentNoise) < 0.007
        break;
    end
end