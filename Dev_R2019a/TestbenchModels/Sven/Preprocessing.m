% Preprocessing Script to create usable structure for further evaluation
% Creates Matrices for different Sensors with time data as well as
% Sensor Data

% Clean all local variables
clc
clear

% Read in the raw Data, memorize name and path for later saving
[filename,path] = uigetfile('*.mat','Bitte Datei auswaehlen'); 
Data = load(fullfile(path,filename)); 

% Reshape matrices
M_AccGyroW = reshape(Data.simoutAccGyroW.signals.values,[],size(Data.simoutAccGyroW.signals.values,3),1)';
M_AccGyroR = reshape(Data.simoutAccGyroR.signals.values,[],size(Data.simoutAccGyroR.signals.values,3),1)';
M_Quat = reshape(Data.simoutQuat.signals.values,[],size(Data.simoutQuat.signals.values,3),1)';
M_TaskID = Data.simoutTaskID.signals.values;
M_SensID = Data.simoutSensIDs.signals.values;
M_Running = Data.simoutTrialRunning.signals.values;
M_Error = Data.simoutTrialError.signals.values(:);
M_Active = Data.simoutActiveUsage.signals.values;


% Save the processed data
f_output = strrep(filename,'.mat','_python.mat');
save([path,f_output],'-v7','M_AccGyroW','M_Quat',...
    'M_AccGyroR','M_TaskID','M_SensID','M_Running','M_Error','M_Active')

fprintf('Saved file under %s',[path,f_output])