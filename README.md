# Simulink-CLS

Real-time Simulink(MATLAB) toolbox for closed loop control. 
Developed by Marko Markovic, ART-Lab, UMG, Goettingen.